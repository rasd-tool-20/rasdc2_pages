create or replace package         RASDC2_PAGES is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASDC2_PAGES generated on 28.02.24 by user RASDDEV.
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/
function version return varchar2;
function this_form return varchar2;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
function metadata return clob;
procedure metadata;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rlog(v_clob clob);
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
  function changes(p_log out varchar2) return varchar2;

procedure openLOV(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
);
end;

/
create or replace package body         RASDC2_PAGES is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASDC2_PAGES generated on 28.02.24 by user RASDDEV.
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/
  type rtab is table of rowid          index by binary_integer;
  type ntab is table of number         index by binary_integer;
  type dtab is table of date           index by binary_integer;
  type ttab is table of timestamp      index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of clob index by binary_integer;
  type itab is table of pls_integer    index by binary_integer;
  type set_type is record
  (
    visible boolean default true,
    readonly boolean default false,
    disabled boolean default false,
    required boolean default false,
    error varchar2(4000) ,
    info varchar2(4000) ,
    custom   varchar2(2000)
  );
  type stab is table of set_type index by binary_integer;
  log__ clob := '';
  set_session_block__ clob := '';
  RESTREQUEST clob := '';
  TYPE LOVrec__ IS RECORD (label varchar2(4000),id varchar2(4000) );
  TYPE LOVtab__ IS TABLE OF LOVrec__ INDEX BY BINARY_INTEGER;
  LOV__ LOVtab__;
  RESTRESTYPE varchar2(4000);
  RECNUMB10                     varchar2(4000);  RECNUMP                       number := 1
;
  ACTION                        varchar2(4000);  GBUTTONCLR                    varchar2(4000) := 'GBUTTONCLR'
;  PAGE                          number := 0
;
  LANG                          varchar2(4000);
  PFORMID                       varchar2(4000);
  PFORM                         varchar2(4000);  GBUTTONSRC                    varchar2(4000) := RASDI_TRNSLT.text('Search',LANG)
;  GBUTTONSAVE                   varchar2(4000) := RASDI_TRNSLT.text('Save',LANG)
;
  GBUTTONSAVE#SET                set_type;  GBUTTONCOMPILE                varchar2(4000) := RASDI_TRNSLT.text('Compile',LANG)
;
  GBUTTONCOMPILE#SET             set_type;  GBUTTONRES                    varchar2(4000) := RASDI_TRNSLT.text('Reset',LANG)
;
  GBUTTONRES#SET                 set_type;  GBUTTONPREV                   varchar2(4000) := RASDI_TRNSLT.text('Preview',LANG)
;
  GBUTTONPREV#SET                set_type;
  ERROR                         varchar2(4000);
  MESSAGE                       varchar2(4000);
  WARNING                       varchar2(4000);
  VUSER                         varchar2(4000);
  VLOB                          varchar2(4000);
  COMPID                        number;
  HINTCONTENT                   varchar2(4000);
  UNLINK                        varchar2(4000);
  XRFORM                        varchar2(4000);
  PFILTER                       ctab;
  PPF                           ctab;
  PPF#SET                        stab;
  PGBTNSRC                      ctab;
  PSESSSTORAGEENABLED           ctab;
  B10RS                         ctab;
  B10FORMID                     ctab;
  B10BLOCKID                    ctab;
  B10BLOK                       ctab;
  B10FIELDID                    ctab;
  B10PAGE0                      ctab;
  B10PAGE0#SET                   stab;
  B10PAGE1                      ctab;
  B10PAGE1#SET                   stab;
  B10PAGE2                      ctab;
  B10PAGE2#SET                   stab;
  B10PAGE3                      ctab;
  B10PAGE3#SET                   stab;
  B10PAGE4                      ctab;
  B10PAGE4#SET                   stab;
  B10PAGE5                      ctab;
  B10PAGE5#SET                   stab;
  B10PAGE6                      ctab;
  B10PAGE6#SET                   stab;
  B10PAGE7                      ctab;
  B10PAGE7#SET                   stab;
  B10PAGE8                      ctab;
  B10PAGE8#SET                   stab;
  B10PAGE9                      ctab;
  B10PAGE9#SET                   stab;
  B10PAGE10                     ctab;
  B10PAGE10#SET                  stab;
  B10PAGE11                     ctab;
  B10PAGE11#SET                  stab;
  B10PAGE12                     ctab;
  B10PAGE12#SET                  stab;
  B10PAGE13                     ctab;
  B10PAGE13#SET                  stab;
  B10PAGE14                     ctab;
  B10PAGE14#SET                  stab;
  B10PAGE15                     ctab;
  B10PAGE15#SET                  stab;
  B10PAGE16                     ctab;
  B10PAGE16#SET                  stab;
  B10PAGE17                     ctab;
  B10PAGE17#SET                  stab;
  B10PAGE18                     ctab;
  B10PAGE18#SET                  stab;
  B10PAGE19                     ctab;
  B10PAGE19#SET                  stab;
  B10PAGE99                     ctab;
  B10PAGE99#SET                  stab;
  B10RF1                        ctab;
  B10RF2                        ctab;
  B10RF3                        ctab;
  B10RF4                        ctab;
  B10RF5                        ctab;
  B10RF6                        ctab;
  B10RF7                        ctab;
  B10RF8                        ctab;
  B10RF9                        ctab;
  B10RF10                       ctab;
  B10RF11                       ctab;
  B10RF12                       ctab;
  B10RF13                       ctab;
  B10RF14                       ctab;
  B10RF15                       ctab;
  B10RF16                       ctab;
  B10RF17                       ctab;
  B10RF18                       ctab;
  B10RF19                       ctab;
  B10RF99                       ctab;
  B30TEXT                       ctab;
  function changes(p_log out varchar2) return varchar2 is

  begin



    p_log := '/* Change LOG:

20230301 - Created new 2 version

20170105 - Modified filter on Pages page

20161207 - Added filter on Pages page

20161124 - Added possibility to check blocks and form fields for setting SESSION variables. Default setting is - not checked. Be careful with programs using SESSION variables in past.

20160627 - Included reference form future.

20160607 - Added 10 more pages to select.

20160406 - Problem when field type was null, it was not shown on pages. Solved.

20151202 - Included session variables in filters

20150814 - Added superuser

20141027 - Added footer on all pages

*/';

    return version;

 end;
function showLabel(plabel varchar2, pcolor varchar2 default 'U', pshowdialog number default 0)

return varchar2 is

   v__ varchar2(32000);

begin



v__ := RASDI_TRNSLT.text(plabel, lang);



if pshowdialog = 1 then

v__ := v__ || rasdc_hints.linkDialog(replace(replace(plabel,' ',''),'.',''),lang,replace(this_form,'2','')||'_DIALOG');

end if;



if pcolor is null then



return v__;



else



return '<font color="'||pcolor||'">'||v__||'</font>';



end if;





end;
procedure post_submit_template is

begin

RASDC_LIBRARY.checkprivileges(PFORMID);

begin

      select upper(form)

        into pform

        from RASD_FORMS

       where formid = PFORMID;

exception when others then null;

end;



if ACTION = GBUTTONSAVE or ACTION = GBUTTONCOMPILE then

  if rasdc_library.allowEditing(pformid) then

     null;

  else

     ACTION := GBUTTONSRC;

	 message := message || RASDI_TRNSLT.text('User has no privileges to save data!', lang);

  end if;

end if;





if rasdc_library.allowEditing(pformid) then

   GBUTTONSAVE#SET.visible := true;

   GBUTTONCOMPILE#SET.visible := true;

else

   GBUTTONSAVE#SET.visible := false;

   GBUTTONCOMPILE#SET.visible := false;

end if;





VUSER := rasdi_client.secGetUsername;

VLOB := rasdi_client.secGetLOB;

if lang is null then lang := rasdi_client.c_defaultLanguage; end if;



end;
procedure compile(pformid number, pform varchar2, lang varchar2,  sporocilo in out varchar2, refform varchar2 default ''  , pcompid in out number) is

      v_server RASD_ENGINES.server%type;

      cid      pls_integer;

      n        pls_integer;

      vup      varchar2(30) := rasdi_client.secGetUsername;

begin

        rasdc_library.log(this_form,pformid, 'COMPILE_S', pcompid);



        begin

          if instr( upper(rasd_client.secGet_PATH_INFO), upper(pform)) > 0

			 then

            sporocilo := RASDI_TRNSLT.text('From is not generated.', lang);

          else



            select server

              into v_server

              from RASD_FORMS_COMPILED fg, RASD_ENGINES g

             where fg.engineid = g.engineid

               and fg.formid = PFORMID

               and fg.editor = vup

               and (fg.lobid = rasdi_client.secGetLOB or

                   fg.lobid is null and rasdi_client.secGetLOB is null);



            cid     := dbms_sql.open_cursor;



            dbms_sql.parse(cid,

                           'begin ' || v_server || '.c_debug := false;'|| v_server || '.form(' || PFORMID ||

                           ',''' || lang || ''');end;',

                           dbms_sql.native);



            n       := dbms_sql.execute(cid);



            dbms_sql.close_cursor(cid);



            sporocilo := RASDI_TRNSLT.text('From is generated.', lang) || rasdc_library.checknumberofsubfields(PFORMID);



        if refform is not null then

           sporocilo :=  sporocilo || '<br/> - '||  RASDI_TRNSLT.text('To unlink referenced code check:', lang)||'<input type="checkbox" name="UNLINK" value="Y"/>.';

        end if;



          end if;

        exception

          when others then

            if sqlcode = -24344 then



            sporocilo := RASDI_TRNSLT.text('Form is generated with compilation error. Check your code.', lang)||'('||sqlerrm||')';



            else

            sporocilo := RASDI_TRNSLT.text('Form is NOT generated - internal RASD error.', lang) || '('||sqlerrm||')<br>'||

                         RASDI_TRNSLT.text('To debug run: ', lang) || 'begin ' || v_server || '.form(' || PFORMID ||

                         ',''' || lang || ''');end;' ;

            end if;

        end;

        rasdc_library.log(this_form,pformid, 'COMPILE_E', pcompid);

end;
  function poutputrest return clob;
     procedure htpClob(v_clob clob) is
        i number := 0;
        v clob := v_clob;
       begin
       while length(v) > 0 and i < 100000 loop
        htp.prn(substr(v,1,10000));
        i := i + 1;
        v := substr(v,10001);
       end loop;
       end;
     procedure rlog(v_clob clob) is
       begin
        log__ := log__ ||systimestamp||':'||v_clob||'<br/>';
        rasd_client.callLog('RASDC2_PAGES',v_clob, systimestamp, '' );
       end;
procedure pLog is begin htpClob('<div class="debug">'||log__||'</div>'); end;
     function FORM_UIHEAD return clob is
       begin
        return  '

';
       end;
     function form_js return clob is
          v_out clob;
       begin
        v_out :=  '
$(function() {



//  addSpinner();

//   initRowStatus();

//   transformVerticalTable("B15_TABLE", 4 );

   setShowHideDiv("B30_DIV", true);

//   CheckFieldValue(pid , pname)

//   CheckFieldMandatory(pid , pname)





HighLightCell(''referenceBlock'', ''#aaccf7'');





$(".rasdTxB20BLOCKID INPUT").attr("maxlength", 30);

$(".rasdTxB20SQLTABLE INPUT").attr("maxlength", 30);

$(".rasdTxB20LABEL INPUT").attr("maxlength", 100);

$(".rasdTxB20NUMROWS INPUT").attr("maxlength", 5);

$(".rasdTxB20EMPTYROWS INPUT").attr("maxlength", 5);





ShowLOVIcon();





 });
$(function() {



  addSpinner();



});



$(function() {



  $(".rasdFormMenu").html("'|| RASDC_LIBRARY.showMeni(THIS_FORM, PFORMID, null, lang) ||'");





  $(document).ready(function () {

   $(".dialog").dialog({ autoOpen: false });

  });





});
        ';
        return v_out;
       end;
     function form_css return clob is
          v_out clob;
       begin
        v_out :=  '
#B10LABEL_1_RASD {

   width: 300px;

}



#rasdTxB10VERSION_1 {

   text-align: center;

}





.rasdTxB20VRSTNIRED {

font-size: x-small;

}



.rasdTxB20LABEL INPUT {

width: 150px;

}



.rasdTxB20SQLTABLE {

width: 150px;

}
        ';
        return v_out;
       end;
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is begin htpClob(form_js); end;
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is begin htpClob(form_css); end;

 procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr);
 procedure on_session;
 procedure formgen_js;

function openLOV(
  p_lov varchar2,
  p_value varchar2
) return lovtab__ is
  name_array   owa.vc_arr;
  value_array  owa.vc_arr;
begin
  name_array(1) := 'PLOV';
  value_array(1) := p_lov;
  name_array(2) := 'PID';
  value_array(2) := p_value;
  name_array(3) := 'CALL';
  value_array(3) := 'PLSQL';
  openLOV(name_array, value_array);
  return lov__;
end;
procedure openLOV(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
  num_entries number := name_array.count;
cursor c_lov$sqlobjects(p_id varchar2) is
--<lovsql formid="91" linkid="lov$sqlobjects">
select id, label from (                   select '' id, '' label, 1 x

                    from dual

                  union

                   select b.sqltable, sqltable || ' ... F' , 3

                   from rasd_blocks b where b.formid = PFORMID

                   and b.sqltable is not null

                   union

                  select /*+ RULE*/ OBJECT_NAME id,

                          OBJECT_NAME || ' ... ' || substr(object_type, 1, 1) label,

                          2 x

                    from all_objects

                   where object_type in ('TABLE', 'VIEW')

                     and (owner = rasdc_library.currentDADUser)

                  union

                  select /*+ RULE*/ distinct SYNONYM_NAME id,

                                   SYNONYM_NAME || ' ... S' label,

                                   2 x

                    from all_synonyms s, all_tab_columns tc

                   where s.table_name = tc.table_name

                     and s.table_owner = tc.owner

                     and (s.owner = rasdc_library.currentDADUser)

                   union

                   select distinct owner||'.'||table_name id,

                          owner||'.'||table_name  /*|| ' ... ' || substr(type, 1, 1) */ label, 2 x

                   from dba_tab_privs x

                   where --type in ('TABLE', 'VIEW') and

                    grantee = rasdc_library.currentDADUser

                   order by 3, 1            ) where upper(id) like '%'||upper(p_id)||'%' or upper(label) like '%'||upper(p_id)||'%'
--</lovsql>
;
cursor c_lov$pages(p_id varchar2) is
--<lovsql formid="91" linkid="lov$pages">
select id, label from (                  select '' id, '' label, -1000 vr

                    from dual

                  union

                  select distinct to_char(page) id, decode(page,99,RASDI_TRNSLT.text('SESSION', lang),RASDI_TRNSLT.text('Page', lang)||' '||to_char(page)) label, page vr

                  from rasd_pages where formid = PFORMID

                   order by 3, 1 ) where upper(id) like '%'||upper(p_id)||'%' or upper(label) like '%'||upper(p_id)||'%'
--</lovsql>
;
TYPE pLOVType IS RECORD (
output varchar2(500),
p1 varchar2(200)
);
  TYPE tab_pLOVType IS TABLE OF pLOVType INDEX BY BINARY_INTEGER;
  v_lov tab_pLOVType;
  v_lovf tab_pLOVType;
  v_counter number := 1;
  v_description varchar2(100);
  p_lov varchar2(100);
  p_nameid varchar2(100);
  p_id varchar2(100);
  v_output boolean;
  v_call varchar2(10);
  v_hidden_fields varchar2(32000) := '';
  v_opener_tekst  varchar2(32000) := '';
  RESTRESTYPE varchar2(10);
begin
  on_submit(name_array, value_array);
  for i in 1..num_entries loop
    if name_array(i) = 'PLOV' then p_lov := value_array(i);
    elsif name_array(i) = 'FIN' then p_nameid := value_array(i);
    elsif name_array(i) = 'PID' then p_id := value_array(i);
    elsif upper(name_array(i)) = 'CALL' then v_call := value_array(i);
    elsif upper(name_array(i)) = upper('RESTRESTYPE') then RESTRESTYPE := value_array(i);
    else
      if name_array(i) not in ('LOVlist') then
        v_hidden_fields := v_hidden_fields||'<input type="hidden" name="'||name_array(i)||'" value="'||value_array(i)||'" />';
      end if;
    end if;
  end loop;
    if v_call not in ('PLSQL','REST') then
      on_session;
    end if;
  if lower(p_lov) = lower('lov$sqlobjects') then
    v_description := 'OBJECTS (TABLE, VIEW, ...)';
    for r in c_lov$sqlobjects(p_id) loop
        v_lov(v_counter).p1 := r.id;
        v_lov(v_counter).output := r.label;
        v_counter := v_counter + 1;
    end loop;
    v_counter := v_counter - 1;
        if 1=2 then null;
        end if;
  elsif lower(p_lov) = lower('lov$pages') then
    v_description := 'PAGES';
    for r in c_lov$pages(p_id) loop
        v_lov(v_counter).p1 := r.id;
        v_lov(v_counter).output := r.label;
        v_counter := v_counter + 1;
    end loop;
    v_counter := v_counter - 1;
        if 1=2 then null;
        end if;
  elsif lower(p_lov) = lower('lov$showmesslist') then
    v_description := 'SHOWMESSLIST';
        v_lov(1).output := '';
        v_lov(1).p1 := '';
        v_lov(2).output := '^';
        v_lov(2).p1 := 'T';
        v_lov(3).output := 'ˇ';
        v_lov(3).p1 := 'B';
        v_counter := 3;
      if p_id is not null then
      for i in 1..v_lov.count loop
       if instr( v_lov(i).output , p_id)+ instr( v_lov(i).p1 , p_id) > 0 then
        v_lovf( nvl(v_lovf.count+1,1) ).output := v_lov(i).output;
        v_lovf( v_lovf.count ).p1 := v_lov(i).p1;
       end if;
      end loop;
      v_lov := v_lovf;
      v_counter := v_lov.count;
      v_lovf.delete;
    end if;
        if 1=2 then null;
        end if;
  elsif lower(p_lov) = lower('link$CHKBXD') then
    v_description := 'CHKBXD';
        v_lov(1).output := 'N';
        v_lov(1).p1 := 'N';
        v_lov(2).output := 'Y';
        v_lov(2).p1 := 'Y';
        v_counter := 2;
        if 1=2 then null;
        end if;
  else
   return;
  end if;
if v_call = 'PLSQL' then
  lov__.delete;
  for i in 1..v_lov.count loop
   lov__(i).id := v_lov(i).p1;
   lov__(i).label := v_lov(i).output;
  end loop;
elsif v_call = 'REST' then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE);
    OWA_UTIL.http_header_close;
 htp.p('<?xml version="1.0" encoding="UTF-8"?>
<openLOV LOV="'||p_lov||'" filter="'||p_id||'">');
 htp.p('<result>');
 for i in 1..v_counter loop
 htp.p('<element><code>'||v_lov(i).p1||'</code><description>'||v_lov(i).output||'</description></element>');
 end loop;
 htp.p('</result></openLOV>');
else
    OWA_UTIL.mime_header('application/json', FALSE);
    OWA_UTIL.http_header_close;
 htp.p('{"openLOV":{"@LOV":"'||p_lov||'","@filter":"'||p_id||'",' );
 htp.p('"result":[');
 for i in 1..v_counter loop
  if i = 1 then
 htp.p('{"code":"'||v_lov(i).p1||'","description":"'||v_lov(i).output||'"}');
  else
 htp.p(',{"code":"'||v_lov(i).p1||'","description":"'||v_lov(i).output||'"}');
  end if;
 end loop;
 htp.p(']}}');
end if;
else
 htp.p('
<html>');
    htp.prn('<head>
<head>');
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||''));
htp.p('');
htp.p('<script type="text/javascript">');
formgen_js;
htp.p('</script>');
htpClob(FORM_UIHEAD);
htp.p('<style type="text/css">');
htpClob(FORM_CSS);
htp.p('</style><script type="text/javascript">');  htp.p('</script>');

htp.prn('</head></head>
    ');
 htp.bodyOpen('','');
htp.p('
<script language="JavaScript">
        $(function() {
        document.getElementById("PID").select();
        });
   function closeLOV() {
     this.close();
   }
   function selectLOV() {
     var value = window.document.'||p_lov||'.LOVlist.options[window.document.'||p_lov||'.LOVlist.selectedIndex].value;
     var tekst = window.document.'||p_lov||'.LOVlist.options[window.document.'||p_lov||'.LOVlist.selectedIndex].text;
     window.opener.'||p_nameid||'.value = value;
     '||v_opener_tekst||'
     event = new Event(''change'');
     window.opener.'||p_nameid||'.dispatchEvent(event);
     ');
htp.p('this.close ();
   }
  with (document) {
  if (screen.availWidth < 900){
    moveTo(-4,-4)}
  }
</script>');
 htp.p('<div class="rasdLovName">'||v_description||'</div>');
 htp.formOpen(curl=>'!RASDC2_PAGES.openLOV',
                 cattributes=>'name="'||p_lov||'"');
 htp.p('<input type="hidden" name="PLOV" value="'||p_lov||'">');
 htp.p('<input type="hidden" name="FIN" value="'||p_nameid||'">');
 htp.p(v_hidden_fields);
 htp.p('<div class="rasdLov" align="center"><center>');
 htp.p('Filter:<input type="text" id="PID" autofocus="autofocus" name="PID" value="'||p_id||'" ></BR><input type="submit" class="rasdButton" value="Search"><input class="rasdButton" type="button" value="Clear" onclick="document.'||p_lov||'.PID.value=''''; document.'||p_lov||'.submit();"></BR>');
 htp.formselectOpen('LOVlist',cattributes=>'size=15 width="100%"');
 for i in 1..v_counter loop
  if i = 1 then -- fokus na prvem
    htp.formSelectOption(cvalue=>v_lov(i).output,cselected=>1,Cattributes => 'value="'||v_lov(i).p1||'"');
  else
    htp.formSelectOption(cvalue=>v_lov(i).output,Cattributes => 'value="'||v_lov(i).p1||'"');
  end if;
 end loop;
 htp.formselectClose;
 htp.p('');
 htp.line;
 htp.p('<input type="button" class="rasdButton" value="Select and Confirm" onClick="selectLOV();">');
 htp.p('<input type="button" class="rasdButton" value="Close" onClick="closeLOV();">');
 htp.p('</center></div>');
 htp.p('</form>');
 htp.p('</body>');
 htp.p('</html>');
end if;
end;
  function version return varchar2 is
  begin
   return 'v.1.1.20240228090904';
  end;
  function this_form return varchar2 is
  begin
   return 'RASDC2_PAGES';
  end;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  begin
   htp.p( version );
  end;
  procedure on_session is
    i__ pls_integer := 1;
  begin
  if ACTION is not null then
set_session_block__ := set_session_block__ || 'begin ';
set_session_block__ := set_session_block__ || 'rasd_client.sessionStart;';
   if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 99 then null;
  if ACTION = GBUTTONCLR then
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PFILTER'', '''' ); ';
  else
    if length( PFILTER(i__)) < 512 then
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PFILTER'', '''||replace(PFILTER(i__),'''','''''')||''' ); ';
     else
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PFILTER'', '''' ); ';
     end if;
 end if;
  if ACTION = GBUTTONCLR then
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''SESSSTORAGEENABLED'', '''' ); ';
  else
    if length( PSESSSTORAGEENABLED(i__)) < 512 then
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''SESSSTORAGEENABLED'', '''||replace(PSESSSTORAGEENABLED(i__),'''','''''')||''' ); ';
     else
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''SESSSTORAGEENABLED'', '''' ); ';
     end if;
 end if;
    end if;
set_session_block__ := set_session_block__ || ' rasd_client.sessionClose;';
set_session_block__ := set_session_block__ || 'exception when others then null; rasd_client.sessionClose; end;';
  else
declare vc varchar2(2000); begin
null;
if PFILTER(i__) is null then vc := rasd_client.sessionGetValue('PFILTER'); PFILTER(i__)  := vc;  end if;
if PSESSSTORAGEENABLED(i__) is null then vc := rasd_client.sessionGetValue('SESSSTORAGEENABLED'); PSESSSTORAGEENABLED(i__)  := vc;  end if;
exception when others then  null; end;
  end if;
  end;
  procedure on_readrest is
    i__ pls_integer := 1;
  begin
for r__  in (select * from json_table( RESTREQUEST , '$.form.formfields' COLUMNS(
   x__ varchar2(1) PATH '$.X__'
  ,RECNUMB10 varchar2(4000) PATH '$.recnumb10'
  ,RECNUMP number PATH '$.recnump'
  ,ACTION varchar2(4000) PATH '$.action'
  ,PAGE number PATH '$.page'
  ,LANG varchar2(4000) PATH '$.lang'
  ,PFORMID varchar2(4000) PATH '$.pformid'
  ,GBUTTONSAVE varchar2(4000) PATH '$.gbuttonsave'
  ,GBUTTONCOMPILE varchar2(4000) PATH '$.gbuttoncompile'
  ,GBUTTONRES varchar2(4000) PATH '$.gbuttonres'
  ,GBUTTONPREV varchar2(4000) PATH '$.gbuttonprev'
  ,HINTCONTENT varchar2(4000) PATH '$.hintcontent'
)) jt ) loop
 if instr(RESTREQUEST,'recnumb10') > 0 then RECNUMB10 := r__.RECNUMB10; end if;
 if instr(RESTREQUEST,'recnump') > 0 then RECNUMP := r__.RECNUMP; end if;
 if instr(RESTREQUEST,'action') > 0 then ACTION := r__.ACTION; end if;
 if instr(RESTREQUEST,'page') > 0 then PAGE := r__.PAGE; end if;
 if instr(RESTREQUEST,'lang') > 0 then LANG := r__.LANG; end if;
 if instr(RESTREQUEST,'pformid') > 0 then PFORMID := r__.PFORMID; end if;
 if instr(RESTREQUEST,'gbuttonsave') > 0 then GBUTTONSAVE := r__.GBUTTONSAVE; end if;
 if instr(RESTREQUEST,'gbuttoncompile') > 0 then GBUTTONCOMPILE := r__.GBUTTONCOMPILE; end if;
 if instr(RESTREQUEST,'gbuttonres') > 0 then GBUTTONRES := r__.GBUTTONRES; end if;
 if instr(RESTREQUEST,'gbuttonprev') > 0 then GBUTTONPREV := r__.GBUTTONPREV; end if;
 if instr(RESTREQUEST,'hintcontent') > 0 then HINTCONTENT := r__.HINTCONTENT; end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.p' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,PFILTER varchar2(4000) PATH '$.pfilter'
  ,PGBTNSRC varchar2(4000) PATH '$.pgbtnsrc'
  ,PSESSSTORAGEENABLED varchar2(4000) PATH '$.sessstorageenabled'
)) jt ) loop
 if instr(RESTREQUEST,'pfilter') > 0 then PFILTER(i__) := r__.PFILTER; end if;
 if instr(RESTREQUEST,'pgbtnsrc') > 0 then PGBTNSRC(i__) := r__.PGBTNSRC; end if;
 if instr(RESTREQUEST,'sessstorageenabled') > 0 then PSESSSTORAGEENABLED(i__) := r__.PSESSSTORAGEENABLED; end if;
i__ := i__ + 1;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b10[*]' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,B10RS varchar2(4000) PATH '$.b10rs'
  ,B10FORMID varchar2(4000) PATH '$.b10formid'
  ,B10BLOCKID varchar2(4000) PATH '$.b10blockid'
  ,B10FIELDID varchar2(4000) PATH '$.b10fieldid'
  ,B10PAGE0 varchar2(4000) PATH '$.b10page0'
  ,B10PAGE1 varchar2(4000) PATH '$.b10page1'
  ,B10PAGE2 varchar2(4000) PATH '$.b10page2'
  ,B10PAGE3 varchar2(4000) PATH '$.b10page3'
  ,B10PAGE4 varchar2(4000) PATH '$.b10page4'
  ,B10PAGE5 varchar2(4000) PATH '$.b10page5'
  ,B10PAGE6 varchar2(4000) PATH '$.b10page6'
  ,B10PAGE7 varchar2(4000) PATH '$.b10page7'
  ,B10PAGE8 varchar2(4000) PATH '$.b10page8'
  ,B10PAGE9 varchar2(4000) PATH '$.b10page9'
  ,B10PAGE10 varchar2(4000) PATH '$.b10page10'
  ,B10PAGE11 varchar2(4000) PATH '$.b10page11'
  ,B10PAGE12 varchar2(4000) PATH '$.b10page12'
  ,B10PAGE13 varchar2(4000) PATH '$.b10page13'
  ,B10PAGE14 varchar2(4000) PATH '$.b10page14'
  ,B10PAGE15 varchar2(4000) PATH '$.b10page15'
  ,B10PAGE16 varchar2(4000) PATH '$.b10page16'
  ,B10PAGE17 varchar2(4000) PATH '$.b10page17'
  ,B10PAGE18 varchar2(4000) PATH '$.b10page18'
  ,B10PAGE19 varchar2(4000) PATH '$.b10page19'
  ,B10PAGE99 varchar2(4000) PATH '$.b10page99'
  ,B10RF1 varchar2(4000) PATH '$.b10rf1'
  ,B10RF2 varchar2(4000) PATH '$.b10rf2'
  ,B10RF3 varchar2(4000) PATH '$.b10rf3'
  ,B10RF4 varchar2(4000) PATH '$.b10rf4'
  ,B10RF5 varchar2(4000) PATH '$.b10rf5'
  ,B10RF6 varchar2(4000) PATH '$.b10rf6'
  ,B10RF7 varchar2(4000) PATH '$.b10rf7'
  ,B10RF8 varchar2(4000) PATH '$.b10rf8'
  ,B10RF9 varchar2(4000) PATH '$.b10rf9'
  ,B10RF10 varchar2(4000) PATH '$.b10rf10'
  ,B10RF11 varchar2(4000) PATH '$.b10rf11'
  ,B10RF12 varchar2(4000) PATH '$.b10rf12'
  ,B10RF13 varchar2(4000) PATH '$.b10rf13'
  ,B10RF14 varchar2(4000) PATH '$.b10rf14'
  ,B10RF15 varchar2(4000) PATH '$.b10rf15'
  ,B10RF16 varchar2(4000) PATH '$.b10rf16'
  ,B10RF17 varchar2(4000) PATH '$.b10rf17'
  ,B10RF18 varchar2(4000) PATH '$.b10rf18'
  ,B10RF19 varchar2(4000) PATH '$.b10rf19'
  ,B10RF99 varchar2(4000) PATH '$.b10rf99'
)) jt ) loop
if r__.x__ is not null then
 if instr(RESTREQUEST,'b10rs') > 0 then B10RS(i__) := r__.B10RS; end if;
 if instr(RESTREQUEST,'b10formid') > 0 then B10FORMID(i__) := r__.B10FORMID; end if;
 if instr(RESTREQUEST,'b10blockid') > 0 then B10BLOCKID(i__) := r__.B10BLOCKID; end if;
 if instr(RESTREQUEST,'b10fieldid') > 0 then B10FIELDID(i__) := r__.B10FIELDID; end if;
 if instr(RESTREQUEST,'b10page0') > 0 then B10PAGE0(i__) := r__.B10PAGE0; end if;
 if instr(RESTREQUEST,'b10page1') > 0 then B10PAGE1(i__) := r__.B10PAGE1; end if;
 if instr(RESTREQUEST,'b10page2') > 0 then B10PAGE2(i__) := r__.B10PAGE2; end if;
 if instr(RESTREQUEST,'b10page3') > 0 then B10PAGE3(i__) := r__.B10PAGE3; end if;
 if instr(RESTREQUEST,'b10page4') > 0 then B10PAGE4(i__) := r__.B10PAGE4; end if;
 if instr(RESTREQUEST,'b10page5') > 0 then B10PAGE5(i__) := r__.B10PAGE5; end if;
 if instr(RESTREQUEST,'b10page6') > 0 then B10PAGE6(i__) := r__.B10PAGE6; end if;
 if instr(RESTREQUEST,'b10page7') > 0 then B10PAGE7(i__) := r__.B10PAGE7; end if;
 if instr(RESTREQUEST,'b10page8') > 0 then B10PAGE8(i__) := r__.B10PAGE8; end if;
 if instr(RESTREQUEST,'b10page9') > 0 then B10PAGE9(i__) := r__.B10PAGE9; end if;
 if instr(RESTREQUEST,'b10page10') > 0 then B10PAGE10(i__) := r__.B10PAGE10; end if;
 if instr(RESTREQUEST,'b10page11') > 0 then B10PAGE11(i__) := r__.B10PAGE11; end if;
 if instr(RESTREQUEST,'b10page12') > 0 then B10PAGE12(i__) := r__.B10PAGE12; end if;
 if instr(RESTREQUEST,'b10page13') > 0 then B10PAGE13(i__) := r__.B10PAGE13; end if;
 if instr(RESTREQUEST,'b10page14') > 0 then B10PAGE14(i__) := r__.B10PAGE14; end if;
 if instr(RESTREQUEST,'b10page15') > 0 then B10PAGE15(i__) := r__.B10PAGE15; end if;
 if instr(RESTREQUEST,'b10page16') > 0 then B10PAGE16(i__) := r__.B10PAGE16; end if;
 if instr(RESTREQUEST,'b10page17') > 0 then B10PAGE17(i__) := r__.B10PAGE17; end if;
 if instr(RESTREQUEST,'b10page18') > 0 then B10PAGE18(i__) := r__.B10PAGE18; end if;
 if instr(RESTREQUEST,'b10page19') > 0 then B10PAGE19(i__) := r__.B10PAGE19; end if;
 if instr(RESTREQUEST,'b10page99') > 0 then B10PAGE99(i__) := r__.B10PAGE99; end if;
 if instr(RESTREQUEST,'b10rf1') > 0 then B10RF1(i__) := r__.B10RF1; end if;
 if instr(RESTREQUEST,'b10rf2') > 0 then B10RF2(i__) := r__.B10RF2; end if;
 if instr(RESTREQUEST,'b10rf3') > 0 then B10RF3(i__) := r__.B10RF3; end if;
 if instr(RESTREQUEST,'b10rf4') > 0 then B10RF4(i__) := r__.B10RF4; end if;
 if instr(RESTREQUEST,'b10rf5') > 0 then B10RF5(i__) := r__.B10RF5; end if;
 if instr(RESTREQUEST,'b10rf6') > 0 then B10RF6(i__) := r__.B10RF6; end if;
 if instr(RESTREQUEST,'b10rf7') > 0 then B10RF7(i__) := r__.B10RF7; end if;
 if instr(RESTREQUEST,'b10rf8') > 0 then B10RF8(i__) := r__.B10RF8; end if;
 if instr(RESTREQUEST,'b10rf9') > 0 then B10RF9(i__) := r__.B10RF9; end if;
 if instr(RESTREQUEST,'b10rf10') > 0 then B10RF10(i__) := r__.B10RF10; end if;
 if instr(RESTREQUEST,'b10rf11') > 0 then B10RF11(i__) := r__.B10RF11; end if;
 if instr(RESTREQUEST,'b10rf12') > 0 then B10RF12(i__) := r__.B10RF12; end if;
 if instr(RESTREQUEST,'b10rf13') > 0 then B10RF13(i__) := r__.B10RF13; end if;
 if instr(RESTREQUEST,'b10rf14') > 0 then B10RF14(i__) := r__.B10RF14; end if;
 if instr(RESTREQUEST,'b10rf15') > 0 then B10RF15(i__) := r__.B10RF15; end if;
 if instr(RESTREQUEST,'b10rf16') > 0 then B10RF16(i__) := r__.B10RF16; end if;
 if instr(RESTREQUEST,'b10rf17') > 0 then B10RF17(i__) := r__.B10RF17; end if;
 if instr(RESTREQUEST,'b10rf18') > 0 then B10RF18(i__) := r__.B10RF18; end if;
 if instr(RESTREQUEST,'b10rf19') > 0 then B10RF19(i__) := r__.B10RF19; end if;
 if instr(RESTREQUEST,'b10rf99') > 0 then B10RF99(i__) := r__.B10RF99; end if;
i__ := i__ + 1;
end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b30[*]' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
)) jt ) loop
if r__.x__ is not null then
i__ := i__ + 1;
end if;
end loop;
  end;
  function validate_submit(v_text varchar2) return varchar2 is
    v_outt varchar2(32000) := v_text;
  begin
    if instr(v_outt,'"') > 0 then v_outt := replace(v_outt,'"','&quot;');
    elsif instr(v_outt,'%22') > 0 then v_outt := replace(v_outt,'%22','&quot;');
    elsif instr(lower(v_outt),'<script') > 0 then v_outt := replace(v_outt,'<script','&lt;script');
    end if;
    return v_outt;
  end;
  procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
    num_entries number := name_array.count;
    v_max  pls_integer := 0;
  begin
-- submit fields
    for i__ in 1..nvl(num_entries,0) loop
      if 1 = 2 then null;
      elsif  upper(name_array(i__)) = 'RESTRESTYPE' then RESTRESTYPE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = 'RESTREQUEST' then RESTREQUEST := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMB10') then RECNUMB10 := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMP') then RECNUMP := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ACTION') then ACTION := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONCLR') then GBUTTONCLR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PAGE') then PAGE := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('LANG') then LANG := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFORMID') then PFORMID := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFORM') then PFORM := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONSRC') then GBUTTONSRC := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONSAVE') then GBUTTONSAVE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONCOMPILE') then GBUTTONCOMPILE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONRES') then GBUTTONRES := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONPREV') then GBUTTONPREV := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ERROR') then ERROR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('MESSAGE') then MESSAGE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('WARNING') then WARNING := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('VUSER') then VUSER := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('VLOB') then VLOB := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('COMPID') then COMPID := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('HINTCONTENT') then HINTCONTENT := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('UNLINK') then UNLINK := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('XRFORM') then XRFORM := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFILTER_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PFILTER(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PF_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PPF(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGBTNSRC_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PGBTNSRC(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('SESSSTORAGEENABLED_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PSESSSTORAGEENABLED(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RS_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RS(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10FORMID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10FORMID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10BLOCKID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10BLOCKID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10BLOK_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10BLOK(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10FIELDID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10FIELDID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE0_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE0(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE1_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE1(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE2_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE2(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE3_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE3(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE4_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE4(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE5_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE5(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE6_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE6(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE7_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE7(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE8_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE8(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE9_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE9(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE10_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE10(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE11_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE11(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE12_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE12(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE13_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE13(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE14_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE14(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE15_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE15(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE16_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE16(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE17_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE17(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE18_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE18(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE19_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE19(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE99_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PAGE99(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF1_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF1(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF2_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF2(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF3_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF3(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF4_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF4(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF5_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF5(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF6_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF6(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF7_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF7(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF8_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF8(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF9_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF9(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF10_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF10(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF11_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF11(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF12_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF12(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF13_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF13(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF14_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF14(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF15_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF15(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF16_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF16(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF17_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF17(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF18_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF18(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF19_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF19(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF99_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RF99(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B30TEXT_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B30TEXT(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFILTER') and PFILTER.count = 0 and value_array(i__) is not null then
        PFILTER(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PF') and PPF.count = 0 and value_array(i__) is not null then
        PPF(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGBTNSRC') and PGBTNSRC.count = 0 and value_array(i__) is not null then
        PGBTNSRC(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('SESSSTORAGEENABLED') and PSESSSTORAGEENABLED.count = 0 and value_array(i__) is not null then
        PSESSSTORAGEENABLED(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RS') and B10RS.count = 0 and value_array(i__) is not null then
        B10RS(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10FORMID') and B10FORMID.count = 0 and value_array(i__) is not null then
        B10FORMID(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10BLOCKID') and B10BLOCKID.count = 0 and value_array(i__) is not null then
        B10BLOCKID(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10BLOK') and B10BLOK.count = 0 and value_array(i__) is not null then
        B10BLOK(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10FIELDID') and B10FIELDID.count = 0 and value_array(i__) is not null then
        B10FIELDID(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE0') and B10PAGE0.count = 0 and value_array(i__) is not null then
        B10PAGE0(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE1') and B10PAGE1.count = 0 and value_array(i__) is not null then
        B10PAGE1(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE2') and B10PAGE2.count = 0 and value_array(i__) is not null then
        B10PAGE2(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE3') and B10PAGE3.count = 0 and value_array(i__) is not null then
        B10PAGE3(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE4') and B10PAGE4.count = 0 and value_array(i__) is not null then
        B10PAGE4(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE5') and B10PAGE5.count = 0 and value_array(i__) is not null then
        B10PAGE5(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE6') and B10PAGE6.count = 0 and value_array(i__) is not null then
        B10PAGE6(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE7') and B10PAGE7.count = 0 and value_array(i__) is not null then
        B10PAGE7(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE8') and B10PAGE8.count = 0 and value_array(i__) is not null then
        B10PAGE8(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE9') and B10PAGE9.count = 0 and value_array(i__) is not null then
        B10PAGE9(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE10') and B10PAGE10.count = 0 and value_array(i__) is not null then
        B10PAGE10(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE11') and B10PAGE11.count = 0 and value_array(i__) is not null then
        B10PAGE11(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE12') and B10PAGE12.count = 0 and value_array(i__) is not null then
        B10PAGE12(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE13') and B10PAGE13.count = 0 and value_array(i__) is not null then
        B10PAGE13(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE14') and B10PAGE14.count = 0 and value_array(i__) is not null then
        B10PAGE14(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE15') and B10PAGE15.count = 0 and value_array(i__) is not null then
        B10PAGE15(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE16') and B10PAGE16.count = 0 and value_array(i__) is not null then
        B10PAGE16(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE17') and B10PAGE17.count = 0 and value_array(i__) is not null then
        B10PAGE17(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE18') and B10PAGE18.count = 0 and value_array(i__) is not null then
        B10PAGE18(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE19') and B10PAGE19.count = 0 and value_array(i__) is not null then
        B10PAGE19(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PAGE99') and B10PAGE99.count = 0 and value_array(i__) is not null then
        B10PAGE99(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF1') and B10RF1.count = 0 and value_array(i__) is not null then
        B10RF1(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF2') and B10RF2.count = 0 and value_array(i__) is not null then
        B10RF2(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF3') and B10RF3.count = 0 and value_array(i__) is not null then
        B10RF3(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF4') and B10RF4.count = 0 and value_array(i__) is not null then
        B10RF4(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF5') and B10RF5.count = 0 and value_array(i__) is not null then
        B10RF5(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF6') and B10RF6.count = 0 and value_array(i__) is not null then
        B10RF6(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF7') and B10RF7.count = 0 and value_array(i__) is not null then
        B10RF7(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF8') and B10RF8.count = 0 and value_array(i__) is not null then
        B10RF8(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF9') and B10RF9.count = 0 and value_array(i__) is not null then
        B10RF9(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF10') and B10RF10.count = 0 and value_array(i__) is not null then
        B10RF10(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF11') and B10RF11.count = 0 and value_array(i__) is not null then
        B10RF11(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF12') and B10RF12.count = 0 and value_array(i__) is not null then
        B10RF12(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF13') and B10RF13.count = 0 and value_array(i__) is not null then
        B10RF13(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF14') and B10RF14.count = 0 and value_array(i__) is not null then
        B10RF14(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF15') and B10RF15.count = 0 and value_array(i__) is not null then
        B10RF15(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF16') and B10RF16.count = 0 and value_array(i__) is not null then
        B10RF16(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF17') and B10RF17.count = 0 and value_array(i__) is not null then
        B10RF17(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF18') and B10RF18.count = 0 and value_array(i__) is not null then
        B10RF18(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF19') and B10RF19.count = 0 and value_array(i__) is not null then
        B10RF19(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RF99') and B10RF99.count = 0 and value_array(i__) is not null then
        B10RF99(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B30TEXT') and B30TEXT.count = 0 and value_array(i__) is not null then
        B30TEXT(1) := validate_submit(value_array(i__));
      end if;
    end loop;
-- organize records
declare
v_last number :=
B10FORMID
.last;
v_curr number :=
B10FORMID
.first;
i__ number;
begin
 if v_last <>
B10FORMID
.count then
   v_curr :=
B10FORMID
.FIRST;
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if B10RS.exists(v_curr) then B10RS(i__) := B10RS(v_curr); end if;
      if B10FORMID.exists(v_curr) then B10FORMID(i__) := B10FORMID(v_curr); end if;
      if B10BLOCKID.exists(v_curr) then B10BLOCKID(i__) := B10BLOCKID(v_curr); end if;
      if B10BLOK.exists(v_curr) then B10BLOK(i__) := B10BLOK(v_curr); end if;
      if B10FIELDID.exists(v_curr) then B10FIELDID(i__) := B10FIELDID(v_curr); end if;
      if B10PAGE0.exists(v_curr) then B10PAGE0(i__) := B10PAGE0(v_curr); end if;
      if B10PAGE1.exists(v_curr) then B10PAGE1(i__) := B10PAGE1(v_curr); end if;
      if B10PAGE2.exists(v_curr) then B10PAGE2(i__) := B10PAGE2(v_curr); end if;
      if B10PAGE3.exists(v_curr) then B10PAGE3(i__) := B10PAGE3(v_curr); end if;
      if B10PAGE4.exists(v_curr) then B10PAGE4(i__) := B10PAGE4(v_curr); end if;
      if B10PAGE5.exists(v_curr) then B10PAGE5(i__) := B10PAGE5(v_curr); end if;
      if B10PAGE6.exists(v_curr) then B10PAGE6(i__) := B10PAGE6(v_curr); end if;
      if B10PAGE7.exists(v_curr) then B10PAGE7(i__) := B10PAGE7(v_curr); end if;
      if B10PAGE8.exists(v_curr) then B10PAGE8(i__) := B10PAGE8(v_curr); end if;
      if B10PAGE9.exists(v_curr) then B10PAGE9(i__) := B10PAGE9(v_curr); end if;
      if B10PAGE10.exists(v_curr) then B10PAGE10(i__) := B10PAGE10(v_curr); end if;
      if B10PAGE11.exists(v_curr) then B10PAGE11(i__) := B10PAGE11(v_curr); end if;
      if B10PAGE12.exists(v_curr) then B10PAGE12(i__) := B10PAGE12(v_curr); end if;
      if B10PAGE13.exists(v_curr) then B10PAGE13(i__) := B10PAGE13(v_curr); end if;
      if B10PAGE14.exists(v_curr) then B10PAGE14(i__) := B10PAGE14(v_curr); end if;
      if B10PAGE15.exists(v_curr) then B10PAGE15(i__) := B10PAGE15(v_curr); end if;
      if B10PAGE16.exists(v_curr) then B10PAGE16(i__) := B10PAGE16(v_curr); end if;
      if B10PAGE17.exists(v_curr) then B10PAGE17(i__) := B10PAGE17(v_curr); end if;
      if B10PAGE18.exists(v_curr) then B10PAGE18(i__) := B10PAGE18(v_curr); end if;
      if B10PAGE19.exists(v_curr) then B10PAGE19(i__) := B10PAGE19(v_curr); end if;
      if B10PAGE99.exists(v_curr) then B10PAGE99(i__) := B10PAGE99(v_curr); end if;
      if B10RF1.exists(v_curr) then B10RF1(i__) := B10RF1(v_curr); end if;
      if B10RF2.exists(v_curr) then B10RF2(i__) := B10RF2(v_curr); end if;
      if B10RF3.exists(v_curr) then B10RF3(i__) := B10RF3(v_curr); end if;
      if B10RF4.exists(v_curr) then B10RF4(i__) := B10RF4(v_curr); end if;
      if B10RF5.exists(v_curr) then B10RF5(i__) := B10RF5(v_curr); end if;
      if B10RF6.exists(v_curr) then B10RF6(i__) := B10RF6(v_curr); end if;
      if B10RF7.exists(v_curr) then B10RF7(i__) := B10RF7(v_curr); end if;
      if B10RF8.exists(v_curr) then B10RF8(i__) := B10RF8(v_curr); end if;
      if B10RF9.exists(v_curr) then B10RF9(i__) := B10RF9(v_curr); end if;
      if B10RF10.exists(v_curr) then B10RF10(i__) := B10RF10(v_curr); end if;
      if B10RF11.exists(v_curr) then B10RF11(i__) := B10RF11(v_curr); end if;
      if B10RF12.exists(v_curr) then B10RF12(i__) := B10RF12(v_curr); end if;
      if B10RF13.exists(v_curr) then B10RF13(i__) := B10RF13(v_curr); end if;
      if B10RF14.exists(v_curr) then B10RF14(i__) := B10RF14(v_curr); end if;
      if B10RF15.exists(v_curr) then B10RF15(i__) := B10RF15(v_curr); end if;
      if B10RF16.exists(v_curr) then B10RF16(i__) := B10RF16(v_curr); end if;
      if B10RF17.exists(v_curr) then B10RF17(i__) := B10RF17(v_curr); end if;
      if B10RF18.exists(v_curr) then B10RF18(i__) := B10RF18(v_curr); end if;
      if B10RF19.exists(v_curr) then B10RF19(i__) := B10RF19(v_curr); end if;
      if B10RF99.exists(v_curr) then B10RF99(i__) := B10RF99(v_curr); end if;
      i__ := i__ + 1;
      v_curr :=
B10FORMID
.NEXT(v_curr);
   END LOOP;
      B10RS.DELETE(i__ , v_last);
      B10FORMID.DELETE(i__ , v_last);
      B10BLOCKID.DELETE(i__ , v_last);
      B10BLOK.DELETE(i__ , v_last);
      B10FIELDID.DELETE(i__ , v_last);
      B10PAGE0.DELETE(i__ , v_last);
      B10PAGE1.DELETE(i__ , v_last);
      B10PAGE2.DELETE(i__ , v_last);
      B10PAGE3.DELETE(i__ , v_last);
      B10PAGE4.DELETE(i__ , v_last);
      B10PAGE5.DELETE(i__ , v_last);
      B10PAGE6.DELETE(i__ , v_last);
      B10PAGE7.DELETE(i__ , v_last);
      B10PAGE8.DELETE(i__ , v_last);
      B10PAGE9.DELETE(i__ , v_last);
      B10PAGE10.DELETE(i__ , v_last);
      B10PAGE11.DELETE(i__ , v_last);
      B10PAGE12.DELETE(i__ , v_last);
      B10PAGE13.DELETE(i__ , v_last);
      B10PAGE14.DELETE(i__ , v_last);
      B10PAGE15.DELETE(i__ , v_last);
      B10PAGE16.DELETE(i__ , v_last);
      B10PAGE17.DELETE(i__ , v_last);
      B10PAGE18.DELETE(i__ , v_last);
      B10PAGE19.DELETE(i__ , v_last);
      B10PAGE99.DELETE(i__ , v_last);
      B10RF1.DELETE(i__ , v_last);
      B10RF2.DELETE(i__ , v_last);
      B10RF3.DELETE(i__ , v_last);
      B10RF4.DELETE(i__ , v_last);
      B10RF5.DELETE(i__ , v_last);
      B10RF6.DELETE(i__ , v_last);
      B10RF7.DELETE(i__ , v_last);
      B10RF8.DELETE(i__ , v_last);
      B10RF9.DELETE(i__ , v_last);
      B10RF10.DELETE(i__ , v_last);
      B10RF11.DELETE(i__ , v_last);
      B10RF12.DELETE(i__ , v_last);
      B10RF13.DELETE(i__ , v_last);
      B10RF14.DELETE(i__ , v_last);
      B10RF15.DELETE(i__ , v_last);
      B10RF16.DELETE(i__ , v_last);
      B10RF17.DELETE(i__ , v_last);
      B10RF18.DELETE(i__ , v_last);
      B10RF19.DELETE(i__ , v_last);
      B10RF99.DELETE(i__ , v_last);
end if;
end;
declare
v_last number :=
B30TEXT
.last;
v_curr number :=
B30TEXT
.first;
i__ number;
begin
 if v_last <>
B30TEXT
.count then
   v_curr :=
B30TEXT
.FIRST;
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if B30TEXT.exists(v_curr) then B30TEXT(i__) := B30TEXT(v_curr); end if;
      i__ := i__ + 1;
      v_curr :=
B30TEXT
.NEXT(v_curr);
   END LOOP;
      B30TEXT.DELETE(i__ , v_last);
end if;
end;
-- init fields
    v_max := 0;
    if B10RS.count > v_max then v_max := B10RS.count; end if;
    if B10FORMID.count > v_max then v_max := B10FORMID.count; end if;
    if B10BLOCKID.count > v_max then v_max := B10BLOCKID.count; end if;
    if B10BLOK.count > v_max then v_max := B10BLOK.count; end if;
    if B10FIELDID.count > v_max then v_max := B10FIELDID.count; end if;
    if B10PAGE0.count > v_max then v_max := B10PAGE0.count; end if;
    if B10PAGE1.count > v_max then v_max := B10PAGE1.count; end if;
    if B10PAGE2.count > v_max then v_max := B10PAGE2.count; end if;
    if B10PAGE3.count > v_max then v_max := B10PAGE3.count; end if;
    if B10PAGE4.count > v_max then v_max := B10PAGE4.count; end if;
    if B10PAGE5.count > v_max then v_max := B10PAGE5.count; end if;
    if B10PAGE6.count > v_max then v_max := B10PAGE6.count; end if;
    if B10PAGE7.count > v_max then v_max := B10PAGE7.count; end if;
    if B10PAGE8.count > v_max then v_max := B10PAGE8.count; end if;
    if B10PAGE9.count > v_max then v_max := B10PAGE9.count; end if;
    if B10PAGE10.count > v_max then v_max := B10PAGE10.count; end if;
    if B10PAGE11.count > v_max then v_max := B10PAGE11.count; end if;
    if B10PAGE12.count > v_max then v_max := B10PAGE12.count; end if;
    if B10PAGE13.count > v_max then v_max := B10PAGE13.count; end if;
    if B10PAGE14.count > v_max then v_max := B10PAGE14.count; end if;
    if B10PAGE15.count > v_max then v_max := B10PAGE15.count; end if;
    if B10PAGE16.count > v_max then v_max := B10PAGE16.count; end if;
    if B10PAGE17.count > v_max then v_max := B10PAGE17.count; end if;
    if B10PAGE18.count > v_max then v_max := B10PAGE18.count; end if;
    if B10PAGE19.count > v_max then v_max := B10PAGE19.count; end if;
    if B10PAGE99.count > v_max then v_max := B10PAGE99.count; end if;
    if B10RF1.count > v_max then v_max := B10RF1.count; end if;
    if B10RF2.count > v_max then v_max := B10RF2.count; end if;
    if B10RF3.count > v_max then v_max := B10RF3.count; end if;
    if B10RF4.count > v_max then v_max := B10RF4.count; end if;
    if B10RF5.count > v_max then v_max := B10RF5.count; end if;
    if B10RF6.count > v_max then v_max := B10RF6.count; end if;
    if B10RF7.count > v_max then v_max := B10RF7.count; end if;
    if B10RF8.count > v_max then v_max := B10RF8.count; end if;
    if B10RF9.count > v_max then v_max := B10RF9.count; end if;
    if B10RF10.count > v_max then v_max := B10RF10.count; end if;
    if B10RF11.count > v_max then v_max := B10RF11.count; end if;
    if B10RF12.count > v_max then v_max := B10RF12.count; end if;
    if B10RF13.count > v_max then v_max := B10RF13.count; end if;
    if B10RF14.count > v_max then v_max := B10RF14.count; end if;
    if B10RF15.count > v_max then v_max := B10RF15.count; end if;
    if B10RF16.count > v_max then v_max := B10RF16.count; end if;
    if B10RF17.count > v_max then v_max := B10RF17.count; end if;
    if B10RF18.count > v_max then v_max := B10RF18.count; end if;
    if B10RF19.count > v_max then v_max := B10RF19.count; end if;
    if B10RF99.count > v_max then v_max := B10RF99.count; end if;
    if v_max = 0 then v_max := 0; end if;
    for i__ in 1..v_max loop
      if not B10RS.exists(i__) then
        B10RS(i__) := null;
      end if;
      if not B10FORMID.exists(i__) then
        B10FORMID(i__) := null;
      end if;
      if not B10BLOCKID.exists(i__) then
        B10BLOCKID(i__) := null;
      end if;
      if not B10BLOK.exists(i__) then
        B10BLOK(i__) := null;
      end if;
      if not B10FIELDID.exists(i__) then
        B10FIELDID(i__) := null;
      end if;
      if not B10PAGE0.exists(i__) or B10PAGE0(i__) is null then
        B10PAGE0(i__) := 'N';
      end if;
      if not B10PAGE0#SET.exists(i__) then
        B10PAGE0#SET(i__).visible := true;
      end if;
      if not B10PAGE1.exists(i__) or B10PAGE1(i__) is null then
        B10PAGE1(i__) := 'N';
      end if;
      if not B10PAGE1#SET.exists(i__) then
        B10PAGE1#SET(i__).visible := true;
      end if;
      if not B10PAGE2.exists(i__) or B10PAGE2(i__) is null then
        B10PAGE2(i__) := 'N';
      end if;
      if not B10PAGE2#SET.exists(i__) then
        B10PAGE2#SET(i__).visible := true;
      end if;
      if not B10PAGE3.exists(i__) or B10PAGE3(i__) is null then
        B10PAGE3(i__) := 'N';
      end if;
      if not B10PAGE3#SET.exists(i__) then
        B10PAGE3#SET(i__).visible := true;
      end if;
      if not B10PAGE4.exists(i__) or B10PAGE4(i__) is null then
        B10PAGE4(i__) := 'N';
      end if;
      if not B10PAGE4#SET.exists(i__) then
        B10PAGE4#SET(i__).visible := true;
      end if;
      if not B10PAGE5.exists(i__) or B10PAGE5(i__) is null then
        B10PAGE5(i__) := 'N';
      end if;
      if not B10PAGE5#SET.exists(i__) then
        B10PAGE5#SET(i__).visible := true;
      end if;
      if not B10PAGE6.exists(i__) or B10PAGE6(i__) is null then
        B10PAGE6(i__) := 'N';
      end if;
      if not B10PAGE6#SET.exists(i__) then
        B10PAGE6#SET(i__).visible := true;
      end if;
      if not B10PAGE7.exists(i__) or B10PAGE7(i__) is null then
        B10PAGE7(i__) := 'N';
      end if;
      if not B10PAGE7#SET.exists(i__) then
        B10PAGE7#SET(i__).visible := true;
      end if;
      if not B10PAGE8.exists(i__) or B10PAGE8(i__) is null then
        B10PAGE8(i__) := 'N';
      end if;
      if not B10PAGE8#SET.exists(i__) then
        B10PAGE8#SET(i__).visible := true;
      end if;
      if not B10PAGE9.exists(i__) or B10PAGE9(i__) is null then
        B10PAGE9(i__) := 'N';
      end if;
      if not B10PAGE9#SET.exists(i__) then
        B10PAGE9#SET(i__).visible := true;
      end if;
      if not B10PAGE10.exists(i__) or B10PAGE10(i__) is null then
        B10PAGE10(i__) := 'N';
      end if;
      if not B10PAGE10#SET.exists(i__) then
        B10PAGE10#SET(i__).visible := true;
      end if;
      if not B10PAGE11.exists(i__) or B10PAGE11(i__) is null then
        B10PAGE11(i__) := 'N';
      end if;
      if not B10PAGE11#SET.exists(i__) then
        B10PAGE11#SET(i__).visible := true;
      end if;
      if not B10PAGE12.exists(i__) or B10PAGE12(i__) is null then
        B10PAGE12(i__) := 'N';
      end if;
      if not B10PAGE12#SET.exists(i__) then
        B10PAGE12#SET(i__).visible := true;
      end if;
      if not B10PAGE13.exists(i__) or B10PAGE13(i__) is null then
        B10PAGE13(i__) := 'N';
      end if;
      if not B10PAGE13#SET.exists(i__) then
        B10PAGE13#SET(i__).visible := true;
      end if;
      if not B10PAGE14.exists(i__) or B10PAGE14(i__) is null then
        B10PAGE14(i__) := 'N';
      end if;
      if not B10PAGE14#SET.exists(i__) then
        B10PAGE14#SET(i__).visible := true;
      end if;
      if not B10PAGE15.exists(i__) or B10PAGE15(i__) is null then
        B10PAGE15(i__) := 'N';
      end if;
      if not B10PAGE15#SET.exists(i__) then
        B10PAGE15#SET(i__).visible := true;
      end if;
      if not B10PAGE16.exists(i__) or B10PAGE16(i__) is null then
        B10PAGE16(i__) := 'N';
      end if;
      if not B10PAGE16#SET.exists(i__) then
        B10PAGE16#SET(i__).visible := true;
      end if;
      if not B10PAGE17.exists(i__) or B10PAGE17(i__) is null then
        B10PAGE17(i__) := 'N';
      end if;
      if not B10PAGE17#SET.exists(i__) then
        B10PAGE17#SET(i__).visible := true;
      end if;
      if not B10PAGE18.exists(i__) or B10PAGE18(i__) is null then
        B10PAGE18(i__) := 'N';
      end if;
      if not B10PAGE18#SET.exists(i__) then
        B10PAGE18#SET(i__).visible := true;
      end if;
      if not B10PAGE19.exists(i__) or B10PAGE19(i__) is null then
        B10PAGE19(i__) := 'N';
      end if;
      if not B10PAGE19#SET.exists(i__) then
        B10PAGE19#SET(i__).visible := true;
      end if;
      if not B10PAGE99.exists(i__) or B10PAGE99(i__) is null then
        B10PAGE99(i__) := 'N';
      end if;
      if not B10PAGE99#SET.exists(i__) then
        B10PAGE99#SET(i__).visible := true;
      end if;
      if not B10RF1.exists(i__) then
        B10RF1(i__) := null;
      end if;
      if not B10RF2.exists(i__) then
        B10RF2(i__) := null;
      end if;
      if not B10RF3.exists(i__) then
        B10RF3(i__) := null;
      end if;
      if not B10RF4.exists(i__) then
        B10RF4(i__) := null;
      end if;
      if not B10RF5.exists(i__) then
        B10RF5(i__) := null;
      end if;
      if not B10RF6.exists(i__) then
        B10RF6(i__) := null;
      end if;
      if not B10RF7.exists(i__) then
        B10RF7(i__) := null;
      end if;
      if not B10RF8.exists(i__) then
        B10RF8(i__) := null;
      end if;
      if not B10RF9.exists(i__) then
        B10RF9(i__) := null;
      end if;
      if not B10RF10.exists(i__) then
        B10RF10(i__) := null;
      end if;
      if not B10RF11.exists(i__) then
        B10RF11(i__) := null;
      end if;
      if not B10RF12.exists(i__) then
        B10RF12(i__) := null;
      end if;
      if not B10RF13.exists(i__) then
        B10RF13(i__) := null;
      end if;
      if not B10RF14.exists(i__) then
        B10RF14(i__) := null;
      end if;
      if not B10RF15.exists(i__) then
        B10RF15(i__) := null;
      end if;
      if not B10RF16.exists(i__) then
        B10RF16(i__) := null;
      end if;
      if not B10RF17.exists(i__) then
        B10RF17(i__) := null;
      end if;
      if not B10RF18.exists(i__) then
        B10RF18(i__) := null;
      end if;
      if not B10RF19.exists(i__) then
        B10RF19(i__) := null;
      end if;
      if not B10RF99.exists(i__) then
        B10RF99(i__) := null;
      end if;
    null; end loop;
    v_max := 0;
    if B30TEXT.count > v_max then v_max := B30TEXT.count; end if;
    if v_max = 0 then v_max := 0; end if;
    for i__ in 1..v_max loop
      if not B30TEXT.exists(i__) then
        B30TEXT(i__) := null;
      end if;
    null; end loop;
    v_max := 0;
    if PFILTER.count > v_max then v_max := PFILTER.count; end if;
    if PPF.count > v_max then v_max := PPF.count; end if;
    if PGBTNSRC.count > v_max then v_max := PGBTNSRC.count; end if;
    if PSESSSTORAGEENABLED.count > v_max then v_max := PSESSSTORAGEENABLED.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not PFILTER.exists(i__) then
        PFILTER(i__) := null;
      end if;
      if not PPF.exists(i__) then
        PPF(i__) := null;
      end if;
      if not PPF#SET.exists(i__) then
        PPF#SET(i__).visible := true;
      end if;
      if not PGBTNSRC.exists(i__) then
        PGBTNSRC(i__) := gbuttonsrc;
      end if;
      if not PSESSSTORAGEENABLED.exists(i__) then
        PSESSSTORAGEENABLED(i__) := null;
      end if;
    null; end loop;
  end;
  procedure post_submit is
  begin
--<POST_SUBMIT formid="91" blockid="">
page := 1;

----put procedure in the begining of trigger;

post_submit_template;



--if action is null then action := RASDI_TRNSLT.text(GBUTTONSRC, lang); end if;
--</POST_SUBMIT>
    null;
  end;
  procedure psubmit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_session;
    post_submit;
  end;
  procedure psubmitrest(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_readrest;
    post_submit;
  end;
  procedure pclear_P(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        PFILTER(i__) := null;
        PPF(i__) := null;
        PGBTNSRC(i__) := gbuttonsrc;
        PSESSSTORAGEENABLED(i__) := null;
        PPF#SET(i__).visible := true;

      end loop;
  end;
  procedure pclear_B10(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 0;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else
       if i__ > 0 then  k__ := i__ + 0;
       else k__ := 0 + 0;
       end if;
      end if;
      j__ := i__;
if pstart = 0 then B10FORMID.delete; end if;
      for i__ in 1..j__ loop
      begin
      B10RS(i__) := B10RS(i__);
      exception when others then
        B10RS(i__) := null;

      end;
      begin
      B10PAGE0#SET(i__) := B10PAGE0#SET(i__);
      exception when others then
        B10PAGE0#SET(i__).visible := true;

      end;
      begin
      B10PAGE1#SET(i__) := B10PAGE1#SET(i__);
      exception when others then
        B10PAGE1#SET(i__).visible := true;

      end;
      begin
      B10PAGE2#SET(i__) := B10PAGE2#SET(i__);
      exception when others then
        B10PAGE2#SET(i__).visible := true;

      end;
      begin
      B10PAGE3#SET(i__) := B10PAGE3#SET(i__);
      exception when others then
        B10PAGE3#SET(i__).visible := true;

      end;
      begin
      B10PAGE4#SET(i__) := B10PAGE4#SET(i__);
      exception when others then
        B10PAGE4#SET(i__).visible := true;

      end;
      begin
      B10PAGE5#SET(i__) := B10PAGE5#SET(i__);
      exception when others then
        B10PAGE5#SET(i__).visible := true;

      end;
      begin
      B10PAGE6#SET(i__) := B10PAGE6#SET(i__);
      exception when others then
        B10PAGE6#SET(i__).visible := true;

      end;
      begin
      B10PAGE7#SET(i__) := B10PAGE7#SET(i__);
      exception when others then
        B10PAGE7#SET(i__).visible := true;

      end;
      begin
      B10PAGE8#SET(i__) := B10PAGE8#SET(i__);
      exception when others then
        B10PAGE8#SET(i__).visible := true;

      end;
      begin
      B10PAGE9#SET(i__) := B10PAGE9#SET(i__);
      exception when others then
        B10PAGE9#SET(i__).visible := true;

      end;
      begin
      B10PAGE10#SET(i__) := B10PAGE10#SET(i__);
      exception when others then
        B10PAGE10#SET(i__).visible := true;

      end;
      begin
      B10PAGE11#SET(i__) := B10PAGE11#SET(i__);
      exception when others then
        B10PAGE11#SET(i__).visible := true;

      end;
      begin
      B10PAGE12#SET(i__) := B10PAGE12#SET(i__);
      exception when others then
        B10PAGE12#SET(i__).visible := true;

      end;
      begin
      B10PAGE13#SET(i__) := B10PAGE13#SET(i__);
      exception when others then
        B10PAGE13#SET(i__).visible := true;

      end;
      begin
      B10PAGE14#SET(i__) := B10PAGE14#SET(i__);
      exception when others then
        B10PAGE14#SET(i__).visible := true;

      end;
      begin
      B10PAGE15#SET(i__) := B10PAGE15#SET(i__);
      exception when others then
        B10PAGE15#SET(i__).visible := true;

      end;
      begin
      B10PAGE16#SET(i__) := B10PAGE16#SET(i__);
      exception when others then
        B10PAGE16#SET(i__).visible := true;

      end;
      begin
      B10PAGE17#SET(i__) := B10PAGE17#SET(i__);
      exception when others then
        B10PAGE17#SET(i__).visible := true;

      end;
      begin
      B10PAGE18#SET(i__) := B10PAGE18#SET(i__);
      exception when others then
        B10PAGE18#SET(i__).visible := true;

      end;
      begin
      B10PAGE19#SET(i__) := B10PAGE19#SET(i__);
      exception when others then
        B10PAGE19#SET(i__).visible := true;

      end;
      begin
      B10PAGE99#SET(i__) := B10PAGE99#SET(i__);
      exception when others then
        B10PAGE99#SET(i__).visible := true;

      end;
      null;
      end loop;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B10RS(i__) := null;
        B10FORMID(i__) := null;
        B10BLOCKID(i__) := null;
        B10BLOK(i__) := null;
        B10FIELDID(i__) := null;
        B10PAGE0(i__) := null;
        B10PAGE1(i__) := null;
        B10PAGE2(i__) := null;
        B10PAGE3(i__) := null;
        B10PAGE4(i__) := null;
        B10PAGE5(i__) := null;
        B10PAGE6(i__) := null;
        B10PAGE7(i__) := null;
        B10PAGE8(i__) := null;
        B10PAGE9(i__) := null;
        B10PAGE10(i__) := null;
        B10PAGE11(i__) := null;
        B10PAGE12(i__) := null;
        B10PAGE13(i__) := null;
        B10PAGE14(i__) := null;
        B10PAGE15(i__) := null;
        B10PAGE16(i__) := null;
        B10PAGE17(i__) := null;
        B10PAGE18(i__) := null;
        B10PAGE19(i__) := null;
        B10PAGE99(i__) := null;
        B10RF1(i__) := null;
        B10RF2(i__) := null;
        B10RF3(i__) := null;
        B10RF4(i__) := null;
        B10RF5(i__) := null;
        B10RF6(i__) := null;
        B10RF7(i__) := null;
        B10RF8(i__) := null;
        B10RF9(i__) := null;
        B10RF10(i__) := null;
        B10RF11(i__) := null;
        B10RF12(i__) := null;
        B10RF13(i__) := null;
        B10RF14(i__) := null;
        B10RF15(i__) := null;
        B10RF16(i__) := null;
        B10RF17(i__) := null;
        B10RF18(i__) := null;
        B10RF19(i__) := null;
        B10RF99(i__) := null;
        B10PAGE0#SET(i__).visible := true;
        B10PAGE1#SET(i__).visible := true;
        B10PAGE2#SET(i__).visible := true;
        B10PAGE3#SET(i__).visible := true;
        B10PAGE4#SET(i__).visible := true;
        B10PAGE5#SET(i__).visible := true;
        B10PAGE6#SET(i__).visible := true;
        B10PAGE7#SET(i__).visible := true;
        B10PAGE8#SET(i__).visible := true;
        B10PAGE9#SET(i__).visible := true;
        B10PAGE10#SET(i__).visible := true;
        B10PAGE11#SET(i__).visible := true;
        B10PAGE12#SET(i__).visible := true;
        B10PAGE13#SET(i__).visible := true;
        B10PAGE14#SET(i__).visible := true;
        B10PAGE15#SET(i__).visible := true;
        B10PAGE16#SET(i__).visible := true;
        B10PAGE17#SET(i__).visible := true;
        B10PAGE18#SET(i__).visible := true;
        B10PAGE19#SET(i__).visible := true;
        B10PAGE99#SET(i__).visible := true;

      end loop;
  end;
  procedure pclear_B30(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 0;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else
       if i__ > 0 then  k__ := i__ + 0;
       else k__ := 0 + 0;
       end if;
      end if;
      j__ := i__;
if pstart = 0 then B30TEXT.delete; end if;
      for i__ in 1..j__ loop
      null;
      end loop;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B30TEXT(i__) := null;

      end loop;
  end;
  procedure pclear_form is
  begin
    RECNUMB10 := null;
    RECNUMP := 1;
    GBUTTONCLR := 'GBUTTONCLR';
    PAGE := 0;
    LANG := null;
    PFORMID := null;
    PFORM := null;
    GBUTTONSRC := RASDI_TRNSLT.text('Search',LANG);
    GBUTTONSAVE := RASDI_TRNSLT.text('Save',LANG);
    GBUTTONCOMPILE := RASDI_TRNSLT.text('Compile',LANG);
    GBUTTONRES := RASDI_TRNSLT.text('Reset',LANG);
    GBUTTONPREV := RASDI_TRNSLT.text('Preview',LANG);
    ERROR := null;
    MESSAGE := null;
    WARNING := null;
    VUSER := null;
    VLOB := null;
    COMPID := null;
    HINTCONTENT := null;
    UNLINK := null;
    XRFORM := null;
  null; end;
  procedure pclear is
  begin
-- Clears all fields on form and blocks.
    pclear_form;
    pclear_P(0);
    pclear_B10(0);
    pclear_B30(0);

  null;
  end;
  procedure pselect_P is
    i__ pls_integer;
  begin
      pclear_P(PFILTER.count);
  null; end;
  procedure pselect_B10 is
    i__ pls_integer;
  begin
      B10FORMID.delete;
      B10BLOCKID.delete;
      B10BLOK.delete;
      B10FIELDID.delete;
      B10PAGE0.delete;
      B10PAGE1.delete;
      B10PAGE2.delete;
      B10PAGE3.delete;
      B10PAGE4.delete;
      B10PAGE5.delete;
      B10PAGE6.delete;
      B10PAGE7.delete;
      B10PAGE8.delete;
      B10PAGE9.delete;
      B10PAGE10.delete;
      B10PAGE11.delete;
      B10PAGE12.delete;
      B10PAGE13.delete;
      B10PAGE14.delete;
      B10PAGE15.delete;
      B10PAGE16.delete;
      B10PAGE17.delete;
      B10PAGE18.delete;
      B10PAGE19.delete;
      B10PAGE99.delete;
      B10RF1.delete;
      B10RF2.delete;
      B10RF3.delete;
      B10RF4.delete;
      B10RF5.delete;
      B10RF6.delete;
      B10RF7.delete;
      B10RF8.delete;
      B10RF9.delete;
      B10RF10.delete;
      B10RF11.delete;
      B10RF12.delete;
      B10RF13.delete;
      B10RF14.delete;
      B10RF15.delete;
      B10RF16.delete;
      B10RF17.delete;
      B10RF18.delete;
      B10RF19.delete;
      B10RF99.delete;

      declare
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR
--<SQL formid="91" blockid="B10">
SELECT
FORMID,
BLOCKID,
BLOK,
FIELDID,
PAGE0,
PAGE1,
PAGE2,
PAGE3,
PAGE4,
PAGE5,
PAGE6,
PAGE7,
PAGE8,
PAGE9,
PAGE10,
PAGE11,
PAGE12,
PAGE13,
PAGE14,
PAGE15,
PAGE16,
PAGE17,
PAGE18,
PAGE19,
PAGE99,
RF1,
RF2,
RF3,
RF4,
RF5,
RF6,
RF7,
RF8,
RF9,
RF10,
RF11,
RF12,
RF13,
RF14,
RF15,
RF16,
RF17,
RF18,
RF19,
RF99 from(

   select b.formid,

                 b.blockid,

                 '<B>'||b.blockid||'</B> <span style="font-size: x-small;">'||b.label||'</span>' blok,

                 '' fieldid,

(select min(x.orderby) from rasd_fields x

where x.formid = b.formid

and x.blockid = b.blockid) orderby,

                  1 vr,

                  decode(s0.page,

                        0,

                        'Y',

                        decode(s1.page,

                               to_number(null),

                               decode(s2.page,

                                      to_number(null),

                                      decode(s3.page,

                                             to_number(null),

                                             decode(s4.page,

                                                    to_number(null),

                                                    decode(s5.page,

                                                           to_number(null),

                                                           decode(s6.page,

                                                                  to_number(null),

                                                                  decode(s7.page,

                                                                         to_number(null),

                                                                         decode(s8.page,

                                                                                to_number(null),

                                                                                decode(s9.page,

                                                                                       to_number(null),

                                        decode(s10.page,

                        to_number(null),

                        decode(s11.page,

                               to_number(null),

                               decode(s12.page,

                                      to_number(null),

                                      decode(s13.page,

                                             to_number(null),

                                             decode(s14.page,

                                                    to_number(null),

                                                    decode(s15.page,

                                                           to_number(null),

                                                           decode(s16.page,

                                                                  to_number(null),

                                                                  decode(s17.page,

                                                                         to_number(null),

                                                                         decode(s18.page,

                                                                                to_number(null),

                                                                                decode(s19.page,

                                                                                       to_number(null),

                        'Y',

                                                                                       'N'),

                                                                                'N'),

                                                                         'N'),

                                                                  'N'),

                                                           'N'),

                                                    'N'),

                                             'N'),

                                      'N'),

                               'N')),

                                                                                       'N'),

                                                                                'N'),

                                                                         'N'),

                                                                  'N'),

                                                           'N'),

                                                    'N'),

                                             'N'),

                                      'N'),

                               'N')) page0,

                 decode(s1.page, 1, 'Y', 'N') page1,

                 decode(s2.page, 2, 'Y', 'N') page2,

                 decode(s3.page, 3, 'Y', 'N') page3,

                 decode(s4.page, 4, 'Y', 'N') page4,

                 decode(s5.page, 5, 'Y', 'N') page5,

                 decode(s6.page, 6, 'Y', 'N') page6,

                 decode(s7.page, 7, 'Y', 'N') page7,

                 decode(s8.page, 8, 'Y', 'N') page8,

                 decode(s9.page, 9, 'Y', 'N') page9,

                 decode(s10.page, 10, 'Y', 'N') page10,

                 decode(s11.page, 11, 'Y', 'N') page11,

                 decode(s12.page, 12, 'Y', 'N') page12,

                 decode(s13.page, 13, 'Y', 'N') page13,

                 decode(s14.page, 14, 'Y', 'N') page14,

                 decode(s15.page, 15, 'Y', 'N') page15,

                 decode(s16.page, 16, 'Y', 'N') page16,

                 decode(s17.page, 17, 'Y', 'N') page17,

                 decode(s18.page, 18, 'Y', 'N') page18,

                 decode(s19.page, 19, 'Y', 'N') page19,

                 decode(s99.page, 99, 'Y', 'N') page99,

                 s1.rform rf1,

                 s2.rform rf2,

                 s3.rform rf3,

                 s4.rform rf4,

                 s5.rform rf5,

                 s6.rform rf6,

                 s7.rform rf7,

                 s8.rform rf8,

                 s9.rform rf9,

                 s10.rform rf10,

                 s11.rform rf11,

                 s12.rform rf12,

                 s13.rform rf13,

                 s14.rform rf14,

                 s15.rform rf15,

                 s16.rform rf16,

                 s17.rform rf17,

                 s18.rform rf18,

                 s19.rform rf19,

                 s99.rform rf99

            from RASD_BLOCKS b,

                 RASD_PAGES  s0,

                 RASD_PAGES  s1,

                 RASD_PAGES  s2,

                 RASD_PAGES  s3,

                 RASD_PAGES  s4,

                 RASD_PAGES  s5,

                 RASD_PAGES  s6,

                 RASD_PAGES  s7,

                 RASD_PAGES  s8,

                 RASD_PAGES  s9,

                 RASD_PAGES  s10,

                 RASD_PAGES  s11,

                 RASD_PAGES  s12,

                 RASD_PAGES  s13,

                 RASD_PAGES  s14,

                 RASD_PAGES  s15,

                 RASD_PAGES  s16,

                 RASD_PAGES  s17,

                 RASD_PAGES  s18,

                 RASD_PAGES  s19,

                 RASD_PAGES  s99

           where b.formid = PFORMID

             and b.blockid like upper(PFILTER(1))||'%'

             and b.formid = s0.formid(+)

             and b.blockid = s0.blockid(+)

             and s0.page(+) = 0

             and b.formid = s1.formid(+)

             and b.blockid = s1.blockid(+)

             and s1.page(+) = 1

             and b.formid = s2.formid(+)

             and b.blockid = s2.blockid(+)

             and s2.page(+) = 2

             and b.formid = s3.formid(+)

             and b.blockid = s3.blockid(+)

             and s3.page(+) = 3

             and b.formid = s4.formid(+)

             and b.blockid = s4.blockid(+)

             and s4.page(+) = 4

             and b.formid = s5.formid(+)

             and b.blockid = s5.blockid(+)

             and s5.page(+) = 5

             and b.formid = s6.formid(+)

             and b.blockid = s6.blockid(+)

             and s6.page(+) = 6

             and b.formid = s7.formid(+)

             and b.blockid = s7.blockid(+)

             and s7.page(+) = 7

             and b.formid = s8.formid(+)

             and b.blockid = s8.blockid(+)

             and s8.page(+) = 8

             and b.formid = s9.formid(+)

             and b.blockid = s9.blockid(+)

             and s9.page(+) = 9

             and b.formid = s10.formid(+)

             and b.blockid = s10.blockid(+)

             and s10.page(+) = 10

             and b.formid = s11.formid(+)

             and b.blockid = s11.blockid(+)

             and s11.page(+) = 11

             and b.formid = s12.formid(+)

             and b.blockid = s12.blockid(+)

             and s12.page(+) = 12

             and b.formid = s13.formid(+)

             and b.blockid = s13.blockid(+)

             and s13.page(+) = 13

             and b.formid = s14.formid(+)

             and b.blockid = s14.blockid(+)

             and s14.page(+) = 14

             and b.formid = s15.formid(+)

             and b.blockid = s15.blockid(+)

             and s15.page(+) = 15

             and b.formid = s16.formid(+)

             and b.blockid = s16.blockid(+)

             and s16.page(+) = 16

             and b.formid = s17.formid(+)

             and b.blockid = s17.blockid(+)

             and s17.page(+) = 17

             and b.formid = s18.formid(+)

             and b.blockid = s18.blockid(+)

             and s18.page(+) = 18

             and b.formid = s19.formid(+)

             and b.blockid = s19.blockid(+)

             and s19.page(+) = 19

             and b.formid = s99.formid(+)

             and b.blockid = s99.blockid(+)

             and s99.page(+) = 99

union

          select f.formid,

                 f.blockid,

                 f.fieldid||' <span style="font-size: x-small;">'|| RASDI_TRNSLT.text('Show on UI:',lang)||' '||nvl(f.elementyn,rasd_engine11.c_false)||'</span>' blok,

                 f.fieldid fieldid,

                 f.orderby,

                 2 vr,

                 decode(s0.page,

                        0,

                        'Y',

                        decode(s1.page,

                               to_number(null),

                               decode(s2.page,

                                      to_number(null),

                                      decode(s3.page,

                                             to_number(null),

                                             decode(s4.page,

                                                    to_number(null),

                                                    decode(s5.page,

                                                           to_number(null),

                                                           decode(s6.page,

                                                                  to_number(null),

                                                                  decode(s7.page,

                                                                         to_number(null),

                                                                         decode(s8.page,

                                                                                to_number(null),

                                                                                decode(s9.page,

                                                                                       to_number(null),

                      decode(s10.page,

                        to_number(null),

                        decode(s11.page,

                               to_number(null),

                               decode(s12.page,

                                      to_number(null),

                                      decode(s13.page,

                                             to_number(null),

                                             decode(s14.page,

                                                    to_number(null),

                                                    decode(s15.page,

                                                           to_number(null),

                                                           decode(s16.page,

                                                                  to_number(null),

                                                                  decode(s17.page,

                                                                         to_number(null),

                                                                         decode(s18.page,

                                                                                to_number(null),

                                                                                decode(s19.page,

                                                                                       to_number(null),

                        'Y',

                                                                                       'N'),

                                                                                'N'),

                                                                         'N'),

                                                                  'N'),

                                                           'N'),

                                                    'N'),

                                             'N'),

                                      'N'),

                               'N')),

                                                                                       'N'),

                                                                                'N'),

                                                                         'N'),

                                                                  'N'),

                                                           'N'),

                                                    'N'),

                                             'N'),

                                      'N'),

                               'N')) page0,

                 decode(s1.page, 1, 'Y', 'N') page1,

                 decode(s2.page, 2, 'Y', 'N') page2,

                 decode(s3.page, 3, 'Y', 'N') page3,

                 decode(s4.page, 4, 'Y', 'N') page4,

                 decode(s5.page, 5, 'Y', 'N') page5,

                 decode(s6.page, 6, 'Y', 'N') page6,

                 decode(s7.page, 7, 'Y', 'N') page7,

                 decode(s8.page, 8, 'Y', 'N') page8,

                 decode(s9.page, 9, 'Y', 'N') page9,

                 decode(s10.page, 10, 'Y', 'N') page10,

                 decode(s11.page, 11, 'Y', 'N') page11,

                 decode(s12.page, 12, 'Y', 'N') page12,

                 decode(s13.page, 13, 'Y', 'N') page13,

                 decode(s14.page, 14, 'Y', 'N') page14,

                 decode(s15.page, 15, 'Y', 'N') page15,

                 decode(s16.page, 16, 'Y', 'N') page16,

                 decode(s17.page, 17, 'Y', 'N') page17,

                 decode(s18.page, 18, 'Y', 'N') page18,

                 decode(s19.page, 19, 'Y', 'N') page19,

                 decode(s99.page, 99, 'Y', 'N') page99,

                 s1.rform rf1,

                 s2.rform rf2,

                 s3.rform rf3,

                 s4.rform rf4,

                 s5.rform rf5,

                 s6.rform rf6,

                 s7.rform rf7,

                 s8.rform rf8,

                 s9.rform rf9,

                 s10.rform rf10,

                 s11.rform rf11,

                 s12.rform rf12,

                 s13.rform rf13,

                 s14.rform rf14,

                 s15.rform rf15,

                 s16.rform rf16,

                 s17.rform rf17,

                 s18.rform rf18,

                 s19.rform rf19,

                 s99.rform rf99

            from rasd_fields f,

                 RASD_PAGES  s0,

                 RASD_PAGES  s1,

                 RASD_PAGES  s2,

                 RASD_PAGES  s3,

                 RASD_PAGES  s4,

                 RASD_PAGES  s5,

                 RASD_PAGES  s6,

                 RASD_PAGES  s7,

                 RASD_PAGES  s8,

                 RASD_PAGES  s9,

                 RASD_PAGES  s10,

                 RASD_PAGES  s11,

                 RASD_PAGES  s12,

                 RASD_PAGES  s13,

                 RASD_PAGES  s14,

                 RASD_PAGES  s15,

                 RASD_PAGES  s16,

                 RASD_PAGES  s17,

                 RASD_PAGES  s18,

                 RASD_PAGES  s19,

                 RASD_PAGES  s99

           where f.formid = PFORMID

             and f.blockid is null

             and f.fieldid like upper(PFILTER(1))||'%'

             and nvl(f.element,'INPUT_TEXT') not in ('INPUT_HIDDEN')

             and f.fieldid <> rasd_engine11.c_message

             and f.fieldid <> rasd_engine11.c_action

             and f.fieldid <> rasd_engine11.c_restrestype

             and f.fieldid <> rasd_engine11.c_restrequest

             and f.fieldid <> rasd_engine11.c_fin

             and f.fieldid <> rasd_engine11.c_warning

             and f.fieldid <> rasd_engine11.c_error

             and f.fieldid <> rasd_engine11.c_page

--             and nvl(f.elementyn,rasd_engine11.c_false) = rasd_engine11.c_true

             and f.formid = s0.formid(+)

             and f.fieldid = s0.fieldid(+)

             and s0.page(+) = 0

             and f.formid = s1.formid(+)

             and f.fieldid = s1.fieldid(+)

             and s1.page(+) = 1

             and f.formid = s2.formid(+)

             and f.fieldid = s2.fieldid(+)

             and s2.page(+) = 2

             and f.formid = s3.formid(+)

             and f.fieldid = s3.fieldid(+)

             and s3.page(+) = 3

             and f.formid = s4.formid(+)

             and f.fieldid = s4.fieldid(+)

             and s4.page(+) = 4

             and f.formid = s5.formid(+)

             and f.fieldid = s5.fieldid(+)

             and s5.page(+) = 5

             and f.formid = s6.formid(+)

             and f.fieldid = s6.fieldid(+)

             and s6.page(+) = 6

             and f.formid = s7.formid(+)

             and f.fieldid = s7.fieldid(+)

             and s7.page(+) = 7

             and f.formid = s8.formid(+)

             and f.fieldid = s8.fieldid(+)

             and s8.page(+) = 8

             and f.formid = s9.formid(+)

             and f.fieldid = s9.fieldid(+)

             and s9.page(+) = 9

             and f.formid = s10.formid(+)

             and f.fieldid = s10.fieldid(+)

             and s10.page(+) = 10

             and f.formid = s11.formid(+)

             and f.fieldid = s11.fieldid(+)

             and s11.page(+) = 11

             and f.formid = s12.formid(+)

             and f.fieldid = s12.fieldid(+)

             and s12.page(+) = 12

             and f.formid = s13.formid(+)

             and f.fieldid = s13.fieldid(+)

             and s13.page(+) = 13

             and f.formid = s14.formid(+)

             and f.fieldid = s14.fieldid(+)

             and s14.page(+) = 14

             and f.formid = s15.formid(+)

             and f.fieldid = s15.fieldid(+)

             and s15.page(+) = 15

             and f.formid = s16.formid(+)

             and f.fieldid = s16.fieldid(+)

             and s16.page(+) = 16

             and f.formid = s17.formid(+)

             and f.fieldid = s17.fieldid(+)

             and s17.page(+) = 17

             and f.formid = s18.formid(+)

             and f.fieldid = s18.fieldid(+)

             and s18.page(+) = 18

             and f.formid = s19.formid(+)

             and f.fieldid = s19.fieldid(+)

             and s19.page(+) = 19

             and f.formid = s99.formid(+)

             and f.fieldid = s99.fieldid(+)

             and s99.page(+) = 99

)

order by vr, orderby
--</SQL>
;
        i__ := 1;
        LOOP
          FETCH c__ INTO
            B10FORMID(i__)
           ,B10BLOCKID(i__)
           ,B10BLOK(i__)
           ,B10FIELDID(i__)
           ,B10PAGE0(i__)
           ,B10PAGE1(i__)
           ,B10PAGE2(i__)
           ,B10PAGE3(i__)
           ,B10PAGE4(i__)
           ,B10PAGE5(i__)
           ,B10PAGE6(i__)
           ,B10PAGE7(i__)
           ,B10PAGE8(i__)
           ,B10PAGE9(i__)
           ,B10PAGE10(i__)
           ,B10PAGE11(i__)
           ,B10PAGE12(i__)
           ,B10PAGE13(i__)
           ,B10PAGE14(i__)
           ,B10PAGE15(i__)
           ,B10PAGE16(i__)
           ,B10PAGE17(i__)
           ,B10PAGE18(i__)
           ,B10PAGE19(i__)
           ,B10PAGE99(i__)
           ,B10RF1(i__)
           ,B10RF2(i__)
           ,B10RF3(i__)
           ,B10RF4(i__)
           ,B10RF5(i__)
           ,B10RF6(i__)
           ,B10RF7(i__)
           ,B10RF8(i__)
           ,B10RF9(i__)
           ,B10RF10(i__)
           ,B10RF11(i__)
           ,B10RF12(i__)
           ,B10RF13(i__)
           ,B10RF14(i__)
           ,B10RF15(i__)
           ,B10RF16(i__)
           ,B10RF17(i__)
           ,B10RF18(i__)
           ,B10RF19(i__)
           ,B10RF99(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  1 then
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.

--<post_select formid="91" blockid="B10">
declare

ix number := 0;

begin



select count(*) into ix

from rasd_blocks rb where nvl(rb.numrows, 1) = 1

 and nvl(rb.emptyrows, 0) = 0

-- and nvl(rb.pagingyn,'N') = 'N'

 and nvl(rb.dbblockyn,'N') = 'N'

 and rb.formid = pformid

 and rb.blockid = B10BLOCKID(i__);



select count(*)+ix into ix

from rasd_fields rp

where nvl(rp.element,'INPUT_TEXT') not in ('FONT_' , 'A_' , 'IMG_' ,'PLSQL_', 'INPUT_BUTTON', 'INPUT_SUBMIT', 'INPUT_RESET')

  and rp.fieldid <> rasd_engine10.c_page

           and rp.fieldid <> rasd_engine11.c_message

           and rp.fieldid <> rasd_engine11.c_action

           and rp.fieldid <> rasd_engine11.c_restrestype

           and rp.fieldid <> rasd_engine11.c_restrequest

           and rp.fieldid <> rasd_engine11.c_fin

           and rp.fieldid <> rasd_engine11.c_warning

           and rp.fieldid <> rasd_engine11.c_error

           and rp.fieldid <> rasd_engine11.c_page

           and instr(rp.fieldid, rasd_engine11.c_recnum) = 0

           and rp.elementyn = rasd_engine11.c_true

           and rp.formid = pformid

           and rp.blockid is null

           and rp.fieldid = B10FIELDID(i__);



if ix = 1 then

B10PAGE99#SET(i__).visible := true;

else

B10PAGE99#SET(i__).visible := false;

end if;



end;



if B10RF1(i__) is not null then

  B10PAGE1#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF2(i__) is not null then

  B10PAGE2#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF3(i__) is not null then

  B10PAGE3#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF4(i__) is not null then

  B10PAGE4#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF5(i__) is not null then

  B10PAGE5#SET(i__).custom := 'class="referenceBlock"';

end if;





if B10RF6(i__) is not null then

  B10PAGE6#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF7(i__) is not null then

  B10PAGE7#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF8(i__) is not null then

  B10PAGE8#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF9(i__) is not null then

  B10PAGE9#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF10(i__) is not null then

  B10PAGE10#SET(i__).custom := 'class="referenceBlock"';

end if;





if B10RF11(i__) is not null then

  B10PAGE11#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF12(i__) is not null then

  B10PAGE12#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF13(i__) is not null then

  B10PAGE13#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF14(i__) is not null then

  B10PAGE14#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF15(i__) is not null then

  B10PAGE15#SET(i__).custom := 'class="referenceBlock"';

end if;





if B10RF16(i__) is not null then

  B10PAGE16#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF17(i__) is not null then

  B10PAGE17#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF18(i__) is not null then

  B10PAGE18#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF19(i__) is not null then

  B10PAGE19#SET(i__).custom := 'class="referenceBlock"';

end if;

if B10RF99(i__) is not null then

  B10PAGE99#SET(i__).custom := 'class="referenceBlock"';

end if;





   if B10RF1(i__) is not null

                   or  B10RF2(i__) is not null

                   or  B10RF3(i__) is not null

                   or  B10RF4(i__) is not null

                   or  B10RF5(i__) is not null

                   or  B10RF6(i__) is not null

                   or  B10RF7(i__) is not null

                   or  B10RF8(i__) is not null

                   or  B10RF9(i__) is not null

                   or  B10RF10(i__) is not null

                   or  B10RF11(i__) is not null

                   or  B10RF12(i__) is not null

                   or  B10RF13(i__) is not null

                   or  B10RF14(i__) is not null

                   or  B10RF15(i__) is not null

                   or  B10RF16(i__) is not null

                   or  B10RF17(i__) is not null

                   or  B10RF18(i__) is not null

                   or  B10RF19(i__) is not null

                   or  B10RF99(i__) is not null

                 then

                   xrform := 'Y';

                 end if;



--rlog(xrform);
--</post_select>
            exit when i__ =0;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  1 then
          B10RS.delete(1);
          B10FORMID.delete(1);
          B10BLOCKID.delete(1);
          B10BLOK.delete(1);
          B10FIELDID.delete(1);
          B10PAGE0.delete(1);
          B10PAGE1.delete(1);
          B10PAGE2.delete(1);
          B10PAGE3.delete(1);
          B10PAGE4.delete(1);
          B10PAGE5.delete(1);
          B10PAGE6.delete(1);
          B10PAGE7.delete(1);
          B10PAGE8.delete(1);
          B10PAGE9.delete(1);
          B10PAGE10.delete(1);
          B10PAGE11.delete(1);
          B10PAGE12.delete(1);
          B10PAGE13.delete(1);
          B10PAGE14.delete(1);
          B10PAGE15.delete(1);
          B10PAGE16.delete(1);
          B10PAGE17.delete(1);
          B10PAGE18.delete(1);
          B10PAGE19.delete(1);
          B10PAGE99.delete(1);
          B10RF1.delete(1);
          B10RF2.delete(1);
          B10RF3.delete(1);
          B10RF4.delete(1);
          B10RF5.delete(1);
          B10RF6.delete(1);
          B10RF7.delete(1);
          B10RF8.delete(1);
          B10RF9.delete(1);
          B10RF10.delete(1);
          B10RF11.delete(1);
          B10RF12.delete(1);
          B10RF13.delete(1);
          B10RF14.delete(1);
          B10RF15.delete(1);
          B10RF16.delete(1);
          B10RF17.delete(1);
          B10RF18.delete(1);
          B10RF19.delete(1);
          B10RF99.delete(1);
          i__ := 0;
        end if;
        CLOSE c__;
      end;
      pclear_B10(B10FORMID.count);
  null; end;
  procedure pselect_B30 is
    i__ pls_integer;
  begin
      B30TEXT.delete;

      declare
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR
          select '<A HREF="javascript:var x = window.open(encodeURI(''!RASDC_ERRORS.Program?PPROGRAM=' ||

                 upper(name) || '#' || substr(type, 1, 1) ||

                 substr(type, instr(type, ' ') + 1, 1) || to_char(line) ||

                 '''),''nx'','''');"  style="color: Red;">ERR: (' ||

                 to_char(line) || ',' || to_char(position) || ')  ' || text ||

                 '</A>' text

            from all_errors

           where upper(name) = upper(pform)

             and owner = rasdc_library.currentDADUser

           order by line, position;
        i__ := 1;
        LOOP
          FETCH c__ INTO
            B30TEXT(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  1 then
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.


            exit when i__ =0;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  1 then
          B30TEXT.delete(1);
          i__ := 0;
        end if;
        CLOSE c__;
      end;
      pclear_B30(B30TEXT.count);
  null; end;
  procedure pselect is
  begin

    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
      pselect_B10;
    end if;
    if  nvl(PAGE,0) = 0 then
      pselect_B30;
    end if;

  null;
 end;
  procedure pcommit_P is
  begin
    for i__ in 1..PFILTER.count loop
-- Validating field values before DML. Use (i__) to access fields values.
    null; end loop;
  null; end;
  procedure pcommit_B10 is
  begin
    for i__ in 1..B10RS.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if substr(B10RS(i__),1,1) = 'I' then --INSERT
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

--<on_update formid="91" blockid="B10">
declare

  v_rform varchar2(1);

begin

   if B10RF1(i__) is not null

                   or  B10RF2(i__) is not null

                   or  B10RF3(i__) is not null

                   or  B10RF4(i__) is not null

                   or  B10RF5(i__) is not null

                   or  B10RF6(i__) is not null

                   or  B10RF7(i__) is not null

                   or  B10RF8(i__) is not null

                   or  B10RF9(i__) is not null

                   or  B10RF10(i__) is not null

                   or  B10RF11(i__) is not null

                   or  B10RF12(i__) is not null

                   or  B10RF13(i__) is not null

                   or  B10RF14(i__) is not null

                   or  B10RF15(i__) is not null

                   or  B10RF16(i__) is not null

                   or  B10RF17(i__) is not null

                   or  B10RF18(i__) is not null

                   or  B10RF19(i__) is not null

                   or  B10RF99(i__) is not null

                 then

                   v_rform := 'Y';

				 else

				   v_rform := 'N';

                 end if;

--rlog(v_rform);

--rlog(unlink);

--rlog(B10BLOCKID(i__));

--rlog(B10FIELDID(i__));



if unlink = 'Y' then v_rform := 'N'; end if;



if v_rform <> 'Y' then

--rlog('Sem noter');

	    if (B10BLOCKID(i__) is not null or B10FIELDID(i__) is not null) and B10FORMID(i__) is not null then

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 0 ;

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 1 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 2 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 3 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 4 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 5 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 6 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 7 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 8 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 9 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;



                        begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 10 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 11 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 12 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 13 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 14 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 15 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 16 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 17 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 18 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 19 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = B10FORMID(i__)

                 and (blockid = B10BLOCKID(i__) or fieldid = B10FIELDID(i__))

                 and page = 99 and (rform is null or unlink = 'Y');

            exception

              when others then

                null;

            end;





            if B10PAGE0(i__) = 'Y'  then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 0, B10FIELDID(i__));

            end if;

            if B10PAGE1(i__) = 'Y' and  (B10RF1(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 1, B10FIELDID(i__));

            end if;

            if B10PAGE2(i__) = 'Y' and  (B10RF2(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 2, B10FIELDID(i__));

            end if;

            if B10PAGE3(i__) = 'Y' and  (B10RF3(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 3, B10FIELDID(i__));

            end if;

            if B10PAGE4(i__) = 'Y' and  (B10RF4(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 4, B10FIELDID(i__));

            end if;

            if B10PAGE5(i__) = 'Y' and  (B10RF5(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 5, B10FIELDID(i__));

            end if;

            if B10PAGE6(i__) = 'Y' and  (B10RF6(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 6, B10FIELDID(i__));

            end if;

            if B10PAGE7(i__) = 'Y' and  (B10RF7(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 7, B10FIELDID(i__));

            end if;

            if B10PAGE8(i__) = 'Y' and  (B10RF8(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 8, B10FIELDID(i__));

            end if;

            if B10PAGE9(i__) = 'Y' and  (B10RF9(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 9, B10FIELDID(i__));

            end if;



            if B10PAGE10(i__) = 'Y' and  (B10RF10(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 10, B10FIELDID(i__));

            end if;

            if B10PAGE11(i__) = 'Y' and  (B10RF11(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 11, B10FIELDID(i__));

            end if;

            if B10PAGE12(i__) = 'Y' and  (B10RF12(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 12, B10FIELDID(i__));

            end if;

            if B10PAGE13(i__) = 'Y' and  (B10RF13(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 13, B10FIELDID(i__));

            end if;

            if B10PAGE14(i__) = 'Y' and  (B10RF14(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 14, B10FIELDID(i__));

            end if;

            if B10PAGE15(i__) = 'Y' and  (B10RF15(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 15, B10FIELDID(i__));

            end if;

            if B10PAGE16(i__) = 'Y' and  (B10RF16(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 16, B10FIELDID(i__));

            end if;

            if B10PAGE17(i__) = 'Y' and  (B10RF17(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 17, B10FIELDID(i__));

            end if;

            if B10PAGE18(i__) = 'Y' and  (B10RF18(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 18, B10FIELDID(i__));

            end if;

            if B10PAGE19(i__) = 'Y' and  (B10RF19(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 19, B10FIELDID(i__));

            end if;

                 if B10PAGE99(i__) = 'Y' and  (B10RF99(i__) is null or unlink = 'Y') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (B10FORMID(i__), B10BLOCKID(i__), 99, B10FIELDID(i__));

            end if;

          end if;



end if;



end;
--</on_update>
      null; end if;
    null; end loop;
  null; end;
  procedure pcommit_B30 is
  begin
    for i__ in 1..B30TEXT.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if 1=2 then --INSERT
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

      null; end if;
    null; end loop;
  null; end;
  procedure pcommit is
  begin

    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       pcommit_B10;
    end if;
    if  nvl(PAGE,0) = 0 then
       pcommit_B30;
    end if;

  null;
  end;
  procedure formgen_js is
  begin
  --Input parameter in JS: LANG
  --Input parameter in JS: PAGE
  --Input parameter in JS: PFORM
  --Input parameter in JS: PFORM
  --Input parameter in JS: PFORMID
  --Input parameter in JS: PFORMID
    htp.p('function js_Clink$CHKBXDinit(pvalue, pobjectname) {');
    htp.p('          var element = document.getElementById(pobjectname + ''_RASD'');  ');
    htp.p('          if (element.value == ''Y'') {  element.checked = true;  }  ');
    htp.p('          else { element.checked = false;  } ');
    htp.p('}');
    htp.p('function js_Clink$CHKBXDclick(pvalue, pobjectname) {');
    htp.p('          var element = document.getElementById(pobjectname + ''_RASD'');  ');
    htp.p('          if (element.checked) { element.value=''Y''; } else { element.value=''N'';}  ');
    htp.p('}');
    htp.p('function cMFP() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFB10() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFB30() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMF() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
  end;
  procedure poutput is
    iB10 pls_integer;
    iB30 pls_integer;
  function ShowFieldCOMPID return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldERROR return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONCLR return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONCOMPILE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONPREV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONRES return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONSAVE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONSRC return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldHINTCONTENT return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldMESSAGE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldPFORM return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldUNLINK return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldVLOB return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldVUSER return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldWARNING return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldXRFORM return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB10_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB30_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockP_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 99 then
       return true;
    end if;
    return false;
  end;
  function js_link$blocks(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    end if;
    return v_return;
  end;
  procedure js_link$blocks(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$blocks(value, name));
  end;
  function js_link$lfields(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    end if;
    return v_return;
  end;
  procedure js_link$lfields(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$lfields(value, name));
  end;
  function js_link$download(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    end if;
    return v_return;
  end;
  procedure js_link$download(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$download(value, name));
  end;
  function js_link$git(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    end if;
    return v_return;
  end;
  procedure js_link$git(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$git(value, name));
  end;
  function js_link$reference(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    end if;
    return v_return;
  end;
  procedure js_link$reference(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$reference(value, name));
  end;
  function js_link$lsql(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    end if;
    return v_return;
  end;
  procedure js_link$lsql(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$lsql(value, name));
  end;
  function js_link$ltriggers(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    end if;
    return v_return;
  end;
  procedure js_link$ltriggers(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$ltriggers(value, name));
  end;
  function js_link$triggers(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    end if;
    return v_return;
  end;
  procedure js_link$triggers(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$triggers(value, name));
  end;
  function js_link$xmldownload(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    end if;
    return v_return;
  end;
  procedure js_link$xmldownload(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$xmldownload(value, name));
  end;
procedure output_B10_DIV is begin htp.p('');  if  ShowBlockB10_DIV  then
htp.prn('<div  id="B10_DIV" class="rasdblock"><div>
<caption><div id="B10_LAB" class="labelblock"></div></caption><table border="1" id="B10_TABLE" class="rasdTableN display"><thead><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10BLOK"><span id="B10BLOK_LAB" class="label">'|| showLabel('Block or field on  Page','',0) ||'</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE0"><span id="B10PAGE0_LAB" class="label">0</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE1"><span id="B10PAGE1_LAB" class="label">1</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE2"><span id="B10PAGE2_LAB" class="label">2</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE3"><span id="B10PAGE3_LAB" class="label">3</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE4"><span id="B10PAGE4_LAB" class="label">4</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE5"><span id="B10PAGE5_LAB" class="label">5</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE6"><span id="B10PAGE6_LAB" class="label">6</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE7"><span id="B10PAGE7_LAB" class="label">7</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE8"><span id="B10PAGE8_LAB" class="label">8</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE9"><span id="B10PAGE9_LAB" class="label">9</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE10"><span id="B10PAGE10_LAB" class="label">10</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE11"><span id="B10PAGE11_LAB" class="label">11</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE12"><span id="B10PAGE12_LAB" class="label">12</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE13"><span id="B10PAGE13_LAB" class="label">13</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE14"><span id="B10PAGE14_LAB" class="label">14</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE15"><span id="B10PAGE15_LAB" class="label">15</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE16"><span id="B10PAGE16_LAB" class="label">16</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE17"><span id="B10PAGE17_LAB" class="label">17</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE18"><span id="B10PAGE18_LAB" class="label">18</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE19"><span id="B10PAGE19_LAB" class="label">19</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PAGE99"><span id="B10PAGE99_LAB" class="label">'|| showLabel('Session','',0) ||'</span></td></tr></thead>'); for iB10 in 1..B10FORMID.count loop
htp.prn('<tr id="B10_BLOCK_'||iB10||'">
<span id="" value="'||iB10||'" name="" class="hiddenRowItems"><input name="B10RS_'||iB10||'" id="B10RS_'||iB10||'_RASD" type="hidden" value="'||B10RS(iB10)||'"/>
<input name="B10FORMID_'||iB10||'" id="B10FORMID_'||iB10||'_RASD" type="hidden" value="'||B10FORMID(iB10)||'"/>
<input name="B10BLOCKID_'||iB10||'" id="B10BLOCKID_'||iB10||'_RASD" type="hidden" value="'||B10BLOCKID(iB10)||'"/>
<input name="B10FIELDID_'||iB10||'" id="B10FIELDID_'||iB10||'_RASD" type="hidden" value="'||B10FIELDID(iB10)||'"/>
<input name="B10RF1_'||iB10||'" id="B10RF1_'||iB10||'_RASD" type="hidden" value="'||B10RF1(iB10)||'"/>
<input name="B10RF2_'||iB10||'" id="B10RF2_'||iB10||'_RASD" type="hidden" value="'||B10RF2(iB10)||'"/>
<input name="B10RF3_'||iB10||'" id="B10RF3_'||iB10||'_RASD" type="hidden" value="'||B10RF3(iB10)||'"/>
<input name="B10RF4_'||iB10||'" id="B10RF4_'||iB10||'_RASD" type="hidden" value="'||B10RF4(iB10)||'"/>
<input name="B10RF5_'||iB10||'" id="B10RF5_'||iB10||'_RASD" type="hidden" value="'||B10RF5(iB10)||'"/>
<input name="B10RF6_'||iB10||'" id="B10RF6_'||iB10||'_RASD" type="hidden" value="'||B10RF6(iB10)||'"/>
<input name="B10RF7_'||iB10||'" id="B10RF7_'||iB10||'_RASD" type="hidden" value="'||B10RF7(iB10)||'"/>
<input name="B10RF8_'||iB10||'" id="B10RF8_'||iB10||'_RASD" type="hidden" value="'||B10RF8(iB10)||'"/>
<input name="B10RF9_'||iB10||'" id="B10RF9_'||iB10||'_RASD" type="hidden" value="'||B10RF9(iB10)||'"/>
<input name="B10RF10_'||iB10||'" id="B10RF10_'||iB10||'_RASD" type="hidden" value="'||B10RF10(iB10)||'"/>
<input name="B10RF11_'||iB10||'" id="B10RF11_'||iB10||'_RASD" type="hidden" value="'||B10RF11(iB10)||'"/>
<input name="B10RF12_'||iB10||'" id="B10RF12_'||iB10||'_RASD" type="hidden" value="'||B10RF12(iB10)||'"/>
<input name="B10RF13_'||iB10||'" id="B10RF13_'||iB10||'_RASD" type="hidden" value="'||B10RF13(iB10)||'"/>
<input name="B10RF14_'||iB10||'" id="B10RF14_'||iB10||'_RASD" type="hidden" value="'||B10RF14(iB10)||'"/>
<input name="B10RF15_'||iB10||'" id="B10RF15_'||iB10||'_RASD" type="hidden" value="'||B10RF15(iB10)||'"/>
<input name="B10RF16_'||iB10||'" id="B10RF16_'||iB10||'_RASD" type="hidden" value="'||B10RF16(iB10)||'"/>
<input name="B10RF17_'||iB10||'" id="B10RF17_'||iB10||'_RASD" type="hidden" value="'||B10RF17(iB10)||'"/>
<input name="B10RF18_'||iB10||'" id="B10RF18_'||iB10||'_RASD" type="hidden" value="'||B10RF18(iB10)||'"/>
<input name="B10RF19_'||iB10||'" id="B10RF19_'||iB10||'_RASD" type="hidden" value="'||B10RF19(iB10)||'"/>
<input name="B10RF99_'||iB10||'" id="B10RF99_'||iB10||'_RASD" type="hidden" value="'||B10RF99(iB10)||'"/>
</span><td class="rasdTxB10BLOK rasdTxTypeC" id="rasdTxB10BLOK_'||iB10||'"><font id="B10BLOK_'||iB10||'_RASD" class="rasdFont">'||B10BLOK(iB10)||'</font></td><td class="rasdTxB10PAGE0 rasdTxTypeC" id="rasdTxB10PAGE0_'||iB10||'">');  if B10PAGE0#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE0_'||iB10||''');" name="B10PAGE0_'||iB10||'" id="B10PAGE0_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE0(iB10)||'" class="rasdCheckbox ');  if B10PAGE0#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE0#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE0#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE0#SET(iB10).custom , instr(upper(B10PAGE0#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE0#SET(iB10).custom),'"',instr(upper(B10PAGE0#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE0#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE0#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE0#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE0#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE0#SET(iB10).custom);
htp.prn('');  if B10PAGE0#SET(iB10).error is not null then htp.p(' title="'||B10PAGE0#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE0#SET(iB10).info is not null then htp.p(' title="'||B10PAGE0#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE0(iB10)||''' , ''B10PAGE0_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE1 rasdTxTypeC" id="rasdTxB10PAGE1_'||iB10||'">');  if B10PAGE1#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE1_'||iB10||''');" name="B10PAGE1_'||iB10||'" id="B10PAGE1_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE1(iB10)||'" class="rasdCheckbox ');  if B10PAGE1#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE1#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE1#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE1#SET(iB10).custom , instr(upper(B10PAGE1#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE1#SET(iB10).custom),'"',instr(upper(B10PAGE1#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE1#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE1#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE1#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE1#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE1#SET(iB10).custom);
htp.prn('');  if B10PAGE1#SET(iB10).error is not null then htp.p(' title="'||B10PAGE1#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE1#SET(iB10).info is not null then htp.p(' title="'||B10PAGE1#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE1(iB10)||''' , ''B10PAGE1_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE2 rasdTxTypeC" id="rasdTxB10PAGE2_'||iB10||'">');  if B10PAGE2#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE2_'||iB10||''');" name="B10PAGE2_'||iB10||'" id="B10PAGE2_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE2(iB10)||'" class="rasdCheckbox ');  if B10PAGE2#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE2#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE2#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE2#SET(iB10).custom , instr(upper(B10PAGE2#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE2#SET(iB10).custom),'"',instr(upper(B10PAGE2#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE2#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE2#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE2#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('
');  if B10PAGE2#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE2#SET(iB10).custom);
htp.prn('');  if B10PAGE2#SET(iB10).error is not null then htp.p(' title="'||B10PAGE2#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE2#SET(iB10).info is not null then htp.p(' title="'||B10PAGE2#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE2(iB10)||''' , ''B10PAGE2_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE3 rasdTxTypeC" id="rasdTxB10PAGE3_'||iB10||'">');  if B10PAGE3#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE3_'||iB10||''');" name="B10PAGE3_'||iB10||'" id="B10PAGE3_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE3(iB10)||'" class="rasdCheckbox ');  if B10PAGE3#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE3#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE3#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE3#SET(iB10).custom , instr(upper(B10PAGE3#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE3#SET(iB10).custom),'"',instr(upper(B10PAGE3#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE3#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE3#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE3#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE3#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE3#SET(iB10).custom);
htp.prn('');  if B10PAGE3#SET(iB10).error is not null then htp.p(' title="'||B10PAGE3#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE3#SET(iB10).info is not null then htp.p(' title="'||B10PAGE3#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE3(iB10)||''' , ''B10PAGE3_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE4 rasdTxTypeC" id="rasdTxB10PAGE4_'||iB10||'">');  if B10PAGE4#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE4_'||iB10||''');" name="B10PAGE4_'||iB10||'" id="B10PAGE4_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE4(iB10)||'" class="rasdCheckbox ');  if B10PAGE4#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE4#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE4#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE4#SET(iB10).custom , instr(upper(B10PAGE4#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE4#SET(iB10).custom),'"',instr(upper(B10PAGE4#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE4#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE4#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE4#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE4#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE4#SET(iB10).custom);
htp.prn('');  if B10PAGE4#SET(iB10).error is not null then htp.p(' title="'||B10PAGE4#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE4#SET(iB10).info is not null then htp.p(' title="'||B10PAGE4#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE4(iB10)||''' , ''B10PAGE4_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE5 rasdTxTypeC" id="rasdTxB10PAGE5_'||iB10||'">');  if B10PAGE5#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE5_'||iB10||''');" name="B10PAGE5_'||iB10||'" id="B10PAGE5_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE5(iB10)||'" class="rasdCheckbox ');  if B10PAGE5#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE5#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE5#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE5#SET(iB10).custom , instr(upper(B10PAGE5#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE5#SET(iB10).custom),'"',instr(upper(B10PAGE5#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE5#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE5#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE5#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE5#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE5#SET(iB10).custom);
htp.prn('');  if B10PAGE5#SET(iB10).error is not null then htp.p(' title="'||B10PAGE5#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE5#SET(iB10).info is not null then htp.p(' title="'||B10PAGE5#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE5(iB10)||''' , ''B10PAGE5_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE6 rasdTxTypeC" id="rasdTxB10PAGE6_'||iB10||'">');  if B10PAGE6#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE6_'||iB10||''');" name="B10PAGE6_'||iB10||'" id="B10PAGE6_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE6(iB10)||'" class="rasdCheckbox ');  if B10PAGE6#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE6#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE6#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE6#SET(iB10).custom , instr(upper(B10PAGE6#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE6#SET(iB10).custom),'"',instr(upper(B10PAGE6#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE6#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE6#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE6#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE6#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE6#SET(iB10).custom);
htp.prn('');  if B10PAGE6#SET(iB10).error is not null then htp.p(' title="'||B10PAGE6#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE6#SET(iB10).info is not null then htp.p(' title="'||B10PAGE6#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE6(iB10)||''' , ''B10PAGE6_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE7 rasdTxTypeC" id="rasdTxB10PAGE7_'||iB10||'">');  if B10PAGE7#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE7_'||iB10||''');" name="B10PAGE7_'||iB10||'" id="B10PAGE7_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE7(iB10)||'" class="rasdCheckbox ');  if B10PAGE7#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE7#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE7#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE7#SET(iB10).custom , instr(upper(B10PAGE7#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE7#SET(iB10).custom),'"',instr(upper(B10PAGE7#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE7#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE7#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE7#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE7#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE7#SET(iB10).custom);
htp.prn('');  if B10PAGE7#SET(iB10).error is not null then htp.p(' title="'||B10PAGE7#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE7#SET(iB10).info is not null then htp.p(' title="'||B10PAGE7#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE7(iB10)||''' , ''B10PAGE7_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE8 rasdTxTypeC" id="rasdTxB10PAGE8_'||iB10||'">');  if B10PAGE8#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE8_'||iB10||''');" name="B10PAGE8_'||iB10||'" id="B10PAGE8_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE8(iB10)||'" class="rasdCheckbox ');  if B10PAGE8#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE8#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE8#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE8#SET(iB10).custom , instr(upper(B10PAGE8#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE8#SET(iB10).custom),'"',instr(upper(B10PAGE8#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE8#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE8#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE8#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE8#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE8#SET(iB10).custom);
htp.prn('');  if B10PAGE8#SET(iB10).error is not null then htp.p(' title="'||B10PAGE8#SET(iB10).error||'"'); end if;
htp.prn('
');  if B10PAGE8#SET(iB10).info is not null then htp.p(' title="'||B10PAGE8#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE8(iB10)||''' , ''B10PAGE8_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE9 rasdTxTypeC" id="rasdTxB10PAGE9_'||iB10||'">');  if B10PAGE9#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE9_'||iB10||''');" name="B10PAGE9_'||iB10||'" id="B10PAGE9_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE9(iB10)||'" class="rasdCheckbox ');  if B10PAGE9#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE9#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE9#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE9#SET(iB10).custom , instr(upper(B10PAGE9#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE9#SET(iB10).custom),'"',instr(upper(B10PAGE9#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE9#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE9#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE9#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE9#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE9#SET(iB10).custom);
htp.prn('');  if B10PAGE9#SET(iB10).error is not null then htp.p(' title="'||B10PAGE9#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE9#SET(iB10).info is not null then htp.p(' title="'||B10PAGE9#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE9(iB10)||''' , ''B10PAGE9_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE10 rasdTxTypeC" id="rasdTxB10PAGE10_'||iB10||'">');  if B10PAGE10#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE10_'||iB10||''');" name="B10PAGE10_'||iB10||'" id="B10PAGE10_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE10(iB10)||'" class="rasdCheckbox ');  if B10PAGE10#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE10#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE10#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE10#SET(iB10).custom , instr(upper(B10PAGE10#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE10#SET(iB10).custom),'"',instr(upper(B10PAGE10#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE10#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE10#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE10#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE10#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE10#SET(iB10).custom);
htp.prn('');  if B10PAGE10#SET(iB10).error is not null then htp.p(' title="'||B10PAGE10#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE10#SET(iB10).info is not null then htp.p(' title="'||B10PAGE10#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE10(iB10)||''' , ''B10PAGE10_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE11 rasdTxTypeC" id="rasdTxB10PAGE11_'||iB10||'">');  if B10PAGE11#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE11_'||iB10||''');" name="B10PAGE11_'||iB10||'" id="B10PAGE11_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE11(iB10)||'" class="rasdCheckbox ');  if B10PAGE11#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE11#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE11#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE11#SET(iB10).custom , instr(upper(B10PAGE11#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE11#SET(iB10).custom),'"',instr(upper(B10PAGE11#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE11#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE11#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE11#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE11#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE11#SET(iB10).custom);
htp.prn('');  if B10PAGE11#SET(iB10).error is not null then htp.p(' title="'||B10PAGE11#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE11#SET(iB10).info is not null then htp.p(' title="'||B10PAGE11#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE11(iB10)||''' , ''B10PAGE11_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE12 rasdTxTypeC" id="rasdTxB10PAGE12_'||iB10||'">');  if B10PAGE12#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE12_'||iB10||''');" name="B10PAGE12_'||iB10||'" id="B10PAGE12_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE12(iB10)||'" class="rasdCheckbox ');  if B10PAGE12#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE12#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE12#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE12#SET(iB10).custom , instr(upper(B10PAGE12#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE12#SET(iB10).custom),'"',instr(upper(B10PAGE12#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE12#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE12#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE12#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE12#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE12#SET(iB10).custom);
htp.prn('');  if B10PAGE12#SET(iB10).error is not null then htp.p(' title="'||B10PAGE12#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE12#SET(iB10).info is not null then htp.p(' title="'||B10PAGE12#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE12(iB10)||''' , ''B10PAGE12_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE13 rasdTxTypeC" id="rasdTxB10PAGE13_'||iB10||'">');  if B10PAGE13#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE13_'||iB10||''');" name="B10PAGE13_'||iB10||'" id="B10PAGE13_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE13(iB10)||'" class="rasdCheckbox ');  if B10PAGE13#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE13#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE13#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE13#SET(iB10).custom , instr(upper(B10PAGE13#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE13#SET(iB10).custom),'"',instr(upper(B10PAGE13#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE13#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE13#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE13#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE13#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE13#SET(iB10).custom);
htp.prn('');  if B10PAGE13#SET(iB10).error is not null then htp.p(' title="'||B10PAGE13#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE13#SET(iB10).info is not null then htp.p(' title="'||B10PAGE13#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE13(iB10)||''' , ''B10PAGE13_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE14 rasdTxTypeC" id="rasdTxB10PAGE14_'||iB10||'">');  if B10PAGE14#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE14_'||iB10||''');" name="B10PAGE14_'||iB10||'" id="B10PAGE14_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE14(iB10)||'" class="rasdCheckbox ');  if B10PAGE14#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE14#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE14#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE14#SET(iB10).custom , instr(upper(B10PAGE14#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE14#SET(iB10).custom),'"',instr(upper(B10PAGE14#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE14#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE14#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE14#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE14#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE14#SET(iB10).custom);
htp.prn('');  if B10PAGE14#SET(iB10).error is not null then htp.p(' title="'||B10PAGE14#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE14#SET(iB10).info is not null then htp.p(' title="'||B10PAGE14#SET(iB10).info||'"'); end if;
htp.prn('
/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE14(iB10)||''' , ''B10PAGE14_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE15 rasdTxTypeC" id="rasdTxB10PAGE15_'||iB10||'">');  if B10PAGE15#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE15_'||iB10||''');" name="B10PAGE15_'||iB10||'" id="B10PAGE15_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE15(iB10)||'" class="rasdCheckbox ');  if B10PAGE15#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE15#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE15#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE15#SET(iB10).custom , instr(upper(B10PAGE15#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE15#SET(iB10).custom),'"',instr(upper(B10PAGE15#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE15#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE15#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE15#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE15#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE15#SET(iB10).custom);
htp.prn('');  if B10PAGE15#SET(iB10).error is not null then htp.p(' title="'||B10PAGE15#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE15#SET(iB10).info is not null then htp.p(' title="'||B10PAGE15#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE15(iB10)||''' , ''B10PAGE15_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE16 rasdTxTypeC" id="rasdTxB10PAGE16_'||iB10||'">');  if B10PAGE16#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE16_'||iB10||''');" name="B10PAGE16_'||iB10||'" id="B10PAGE16_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE16(iB10)||'" class="rasdCheckbox ');  if B10PAGE16#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE16#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE16#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE16#SET(iB10).custom , instr(upper(B10PAGE16#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE16#SET(iB10).custom),'"',instr(upper(B10PAGE16#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE16#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE16#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE16#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE16#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE16#SET(iB10).custom);
htp.prn('');  if B10PAGE16#SET(iB10).error is not null then htp.p(' title="'||B10PAGE16#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE16#SET(iB10).info is not null then htp.p(' title="'||B10PAGE16#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE16(iB10)||''' , ''B10PAGE16_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE17 rasdTxTypeC" id="rasdTxB10PAGE17_'||iB10||'">');  if B10PAGE17#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE17_'||iB10||''');" name="B10PAGE17_'||iB10||'" id="B10PAGE17_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE17(iB10)||'" class="rasdCheckbox ');  if B10PAGE17#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE17#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE17#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE17#SET(iB10).custom , instr(upper(B10PAGE17#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE17#SET(iB10).custom),'"',instr(upper(B10PAGE17#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE17#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE17#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE17#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE17#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE17#SET(iB10).custom);
htp.prn('');  if B10PAGE17#SET(iB10).error is not null then htp.p(' title="'||B10PAGE17#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE17#SET(iB10).info is not null then htp.p(' title="'||B10PAGE17#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE17(iB10)||''' , ''B10PAGE17_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE18 rasdTxTypeC" id="rasdTxB10PAGE18_'||iB10||'">');  if B10PAGE18#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE18_'||iB10||''');" name="B10PAGE18_'||iB10||'" id="B10PAGE18_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE18(iB10)||'" class="rasdCheckbox ');  if B10PAGE18#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE18#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE18#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE18#SET(iB10).custom , instr(upper(B10PAGE18#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE18#SET(iB10).custom),'"',instr(upper(B10PAGE18#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE18#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE18#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE18#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE18#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE18#SET(iB10).custom);
htp.prn('');  if B10PAGE18#SET(iB10).error is not null then htp.p(' title="'||B10PAGE18#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE18#SET(iB10).info is not null then htp.p(' title="'||B10PAGE18#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE18(iB10)||''' , ''B10PAGE18_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE19 rasdTxTypeC" id="rasdTxB10PAGE19_'||iB10||'">');  if B10PAGE19#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE19_'||iB10||''');" name="B10PAGE19_'||iB10||'" id="B10PAGE19_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE19(iB10)||'" class="rasdCheckbox ');  if B10PAGE19#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE19#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE19#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE19#SET(iB10).custom , instr(upper(B10PAGE19#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE19#SET(iB10).custom),'"',instr(upper(B10PAGE19#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE19#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE19#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE19#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE19#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE19#SET(iB10).custom);
htp.prn('');  if B10PAGE19#SET(iB10).error is not null then htp.p(' title="'||B10PAGE19#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE19#SET(iB10).info is not null then htp.p(' title="'||B10PAGE19#SET(iB10).info||'"'); end if;
htp.prn('/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE19(iB10)||''' , ''B10PAGE19_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td><td class="rasdTxB10PAGE99 rasdTxTypeC" id="rasdTxB10PAGE99_'||iB10||'">');  if B10PAGE99#SET(iB10).visible then
htp.prn('<input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10PAGE99_'||iB10||''');" name="B10PAGE99_'||iB10||'" id="B10PAGE99_'||iB10||'_RASD" type="checkbox" value="'||B10PAGE99(iB10)||'" class="rasdCheckbox ');  if B10PAGE99#SET(iB10).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if B10PAGE99#SET(iB10).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(B10PAGE99#SET(iB10).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PAGE99#SET(iB10).custom , instr(upper(B10PAGE99#SET(iB10).custom),'CLASS="')+7 , instr(upper(B10PAGE99#SET(iB10).custom),'"',instr(upper(B10PAGE99#SET(iB10).custom),'CLASS="')+8)-instr(upper(B10PAGE99#SET(iB10).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if B10PAGE99#SET(iB10).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if B10PAGE99#SET(iB10).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if B10PAGE99#SET(iB10).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||B10PAGE99#SET(iB10).custom);
htp.prn('');  if B10PAGE99#SET(iB10).error is not null then htp.p(' title="'||B10PAGE99#SET(iB10).error||'"'); end if;
htp.prn('');  if B10PAGE99#SET(iB10).info is not null then htp.p(' title="'||B10PAGE99#SET(iB10).info||'"'); end if;
htp.prn('
/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10PAGE99(iB10)||''' , ''B10PAGE99_'||iB10||''' ); }) </SCRIPT>');  end if;
htp.prn('
</td></tr>'); end loop;
htp.prn('</table></div></div>');  end if;
htp.prn(''); end;
procedure output_B30_DIV is begin htp.p('');  if  ShowBlockB30_DIV  then
htp.prn('<div  id="B30_DIV" class="rasdblock"><div>
<caption><div id="B30_LAB" class="labelblock">'|| RASDI_TRNSLT.text('Errors',LANG)||'</div></caption><table border="1" id="B30_TABLE" class="rasdTableN display"><thead><tr><td class="rasdTxLab rasdTxLabBlockB30" id="rasdTxLabB30TEXT"><span id="B30TEXT_LAB" class="label"></span></td></tr></thead>'); for iB30 in 1..B30TEXT.count loop
htp.prn('<tr id="B30_BLOCK_'||iB30||'"><td class="rasdTxB30TEXT rasdTxTypeC" id="rasdTxB30TEXT_'||iB30||'"><font id="B30TEXT_'||iB30||'_RASD" class="rasdFont">'||B30TEXT(iB30)||'</font></td></tr>'); end loop;
htp.prn('</table></div></div>');  end if;
htp.prn(''); end;
procedure output_P_DIV is begin htp.p('');  if  ShowBlockP_DIV  then
htp.prn('<div  id="P_DIV" class="rasdblock"><div>
<caption><div id="P_LAB" class="labelblock"></div></caption>
<table border="0" id="P_TABLE"><thead><tr><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPFILTER"><span id="PFILTER_LAB" class="label">'|| showLabel('Filter','',0) ||'</span></td><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPGBTNSRC"><span id="PGBTNSRC_LAB" class="label"></span></td></tr></thead><tr id="P_BLOCK_1"><span id="" value="1" name="" class="hiddenRowItems"><input name="SESSSTORAGEENABLED_1" id="SESSSTORAGEENABLED_1_RASD" type="hidden" value="'||PSESSSTORAGEENABLED(1)||'"/>
</span><td class="rasdTxPFILTER rasdTxTypeC" id="rasdTxPFILTER_1"><input name="PFILTER_1" id="PFILTER_1_RASD" type="text" value="'||PFILTER(1)||'" class="rasdTextC "/>
</td><td class="rasdTxPGBTNSRC rasdTxTypeC" id="rasdTxPGBTNSRC_1"><input onclick=" ACTION.value=this.value; submit();" name="PGBTNSRC_1" id="PGBTNSRC_1_RASD" type="button" value="'||PGBTNSRC(1)||'" class="rasdButton"/>
</td></tr></table></div></div>');  end if;
htp.prn(''); end;
  begin
if set_session_block__ is not null then  execute immediate set_session_block__;  end if;
    htp.prn('<html>
<head>');
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||''));
htp.p('');
htp.p('<script type="text/javascript">');
formgen_js;
htp.p('</script>');
htpClob(FORM_UIHEAD);
htp.p('<style type="text/css">');
htpClob(FORM_CSS);
htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>');

htp.prn('</head>
<body><div id="RASDC2_PAGES_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASDC2_PAGES_LAB',''|| rasdc_library.formName(PFORMID, LANG) ||'') ||'     </div><div id="RASDC2_PAGES_MENU" class="rasdFormMenu">'|| rasd_client.getHtmlMenuList('RASDC2_PAGES_MENU') ||'     </div>
<form name="RASDC2_PAGES" method="post" action="?"><div id="RASDC2_PAGES_DIV" class="rasdForm"><div id="RASDC2_PAGES_HEAD" class="rasdFormHead"><input name="RECNUMP" id="RECNUMP_RASD" type="hidden" value="'||ltrim(to_char(RECNUMP))||'"/>
<input name="RECNUMB10" id="RECNUMB10_RASD" type="hidden" value="'||RECNUMB10||'"/>
<input name="ACTION" id="ACTION_RASD" type="hidden" value="'||ACTION||'"/>
<input name="PAGE" id="PAGE_RASD" type="hidden" value="'||ltrim(to_char(PAGE))||'"/>
<input name="LANG" id="LANG_RASD" type="hidden" value="'||LANG||'"/>
<input name="PFORMID" id="PFORMID_RASD" type="hidden" value="'||PFORMID||'"/>
');
if  ShowFieldGBUTTONSAVE  then
htp.prn('');  if GBUTTONSAVE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSAVE" id="GBUTTONSAVE_RASD" type="button" value="'||GBUTTONSAVE||'" class="rasdButton');  if GBUTTONSAVE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONSAVE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONSAVE#SET.custom),'"',instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONSAVE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONSAVE#SET.custom);
htp.prn('');  if GBUTTONSAVE#SET.error is not null then htp.p(' title="'||GBUTTONSAVE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' title="'||GBUTTONSAVE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('');  if GBUTTONCOMPILE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONCOMPILE" id="GBUTTONCOMPILE_RASD" type="button" value="'||GBUTTONCOMPILE||'" class="rasdButton');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),'"',instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONCOMPILE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONCOMPILE#SET.custom);
htp.prn('');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONRES  then
htp.prn('');  if GBUTTONRES#SET.visible then
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton');  if GBUTTONRES#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONRES#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONRES#SET.custom),'"',instr(upper(GBUTTONRES#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONRES#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONRES#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONRES#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONRES#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONRES#SET.custom);
htp.prn('');  if GBUTTONRES#SET.error is not null then htp.p(' title="'||GBUTTONRES#SET.error||'"'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' title="'||GBUTTONRES#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONPREV  then
htp.prn('');  if GBUTTONPREV#SET.visible then
htp.prn('<span id="GBUTTONPREV_RASD"');  if GBUTTONPREV#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONPREV#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONPREV#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONPREV#SET.custom);
htp.prn('');  if GBUTTONPREV#SET.error is not null then htp.p(' title="'||GBUTTONPREV#SET.error||'"'); end if;
htp.prn('');  if GBUTTONPREV#SET.info is not null then htp.p(' title="'||GBUTTONPREV#SET.info||'"'); end if;
htp.prn('>');  end if;
htp.prn('');  htp.p( '<input type=button class="rasdButton" value="' ||

                   RASDI_TRNSLT.text('Preview', lang) || '" ' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                'disabled="disabled" style="background-color: red;" title="' ||RASDI_TRNSLT.text('Program has ERRORS!',lang)||'" ',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     'style="background-color: orange;" title="' ||RASDI_TRNSLT.text('Programa has changes. Compile it.',lang) ||

                                     '" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

									 ,

                                     'style="background-color: green;" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

                                )

			       ) || '>'

);
htp.prn('</span>');  end if;
htp.prn('');
if  ShowFieldHINTCONTENT  then
htp.prn('<span id="HINTCONTENT_RASD">');  htp.p('<div style="display: none;">');

htp.p(rasdc_hints.getHint(replace(this_form,'2','')||'_DIALOG',lang));

htp.p('</div>');
htp.prn('</span>');  end if;
htp.prn('</div><div id="RASDC2_PAGES_RESPONSE" class="rasdFormResponse"><div id="RASDC2_PAGES_ERROR" class="rasdFormMessage error"><font id="ERROR_RASD" class="rasdFont">'||ERROR||'</font></div><div id="RASDC2_PAGES_WARNING" class="rasdFormMessage warning"><font id="WARNING_RASD" class="rasdFont">'||WARNING||'</font></div><div id="RASDC2_PAGES_MESSAGE" class="rasdFormMessage"><font id="MESSAGE_RASD" class="rasdFont">'||MESSAGE||'</font></div></div><div id="RASDC2_PAGES_BODY" class="rasdFormBody">'); output_P_DIV; htp.p(''); output_B10_DIV; htp.p(''); output_B30_DIV; htp.p('</div><div id="RASDC2_PAGES_FOOTER" class="rasdFormFooter">');
if  ShowFieldGBUTTONSAVE  then
htp.prn('');  if GBUTTONSAVE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSAVE" id="GBUTTONSAVE_RASD" type="button" value="'||GBUTTONSAVE||'" class="rasdButton');  if GBUTTONSAVE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONSAVE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONSAVE#SET.custom),'"',instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONSAVE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONSAVE#SET.custom);
htp.prn('');  if GBUTTONSAVE#SET.error is not null then htp.p(' title="'||GBUTTONSAVE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' title="'||GBUTTONSAVE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('');  if GBUTTONCOMPILE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONCOMPILE" id="GBUTTONCOMPILE_RASD" type="button" value="'||GBUTTONCOMPILE||'" class="rasdButton');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),'"',instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"
');  if GBUTTONCOMPILE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONCOMPILE#SET.custom);
htp.prn('');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONRES  then
htp.prn('');  if GBUTTONRES#SET.visible then
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton');  if GBUTTONRES#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONRES#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONRES#SET.custom),'"',instr(upper(GBUTTONRES#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONRES#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONRES#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONRES#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONRES#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONRES#SET.custom);
htp.prn('');  if GBUTTONRES#SET.error is not null then htp.p(' title="'||GBUTTONRES#SET.error||'"'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' title="'||GBUTTONRES#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONPREV  then
htp.prn('');  if GBUTTONPREV#SET.visible then
htp.prn('<span id="GBUTTONPREV_RASD"');  if GBUTTONPREV#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONPREV#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONPREV#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONPREV#SET.custom);
htp.prn('');  if GBUTTONPREV#SET.error is not null then htp.p(' title="'||GBUTTONPREV#SET.error||'"'); end if;
htp.prn('');  if GBUTTONPREV#SET.info is not null then htp.p(' title="'||GBUTTONPREV#SET.info||'"'); end if;
htp.prn('>');  end if;
htp.prn('');  htp.p( '<input type=button class="rasdButton" value="' ||

                   RASDI_TRNSLT.text('Preview', lang) || '" ' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                'disabled="disabled" style="background-color: red;" title="' ||RASDI_TRNSLT.text('Program has ERRORS!',lang)||'" ',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     'style="background-color: orange;" title="' ||RASDI_TRNSLT.text('Programa has changes. Compile it.',lang) ||

                                     '" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

									 ,

                                     'style="background-color: green;" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

                                )

			       ) || '>'

);
htp.prn('</span>');  end if;
htp.prn('');
if  ShowFieldHINTCONTENT  then
htp.prn('<span id="HINTCONTENT_RASD">');  htp.p('<div style="display: none;">');

htp.p(rasdc_hints.getHint(replace(this_form,'2','')||'_DIALOG',lang));

htp.p('</div>');
htp.prn('</span>');  end if;
htp.prn('</div></div></form><div id="RASDC2_PAGES_BOTTOM" class="rasdFormBottom">'|| rasd_client.getHtmlFooter(version , substr('RASDC2_PAGES_BOTTOM',1,instr('RASDC2_PAGES_BOTTOM', '_',-1)-1) , '') ||'</div></body></html>
    ');
  null; end;
  function poutputrest return clob is
    v_firstrow__ boolean;
    v_clob__ clob;
    procedure htpp(v_str varchar2) is
    begin
      v_clob__ := v_clob__ || v_str;
    end;
    function escapeRest(v_str varchar2) return varchar2 is
    begin
      return replace(v_str,'"','&quot;');
    end;
    function escapeRest(v_str clob) return clob is
    begin
      return replace(v_str,'"','&quot;');
    end;
  function ShowBlockB10_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB30_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockP_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 99 then
       return true;
    end if;
    return false;
  end;
  begin
if RESTRESTYPE = 'XML' then
    htpp('<?xml version="1.0" encoding="UTF-8"?>');
    htpp('<form name="RASDC2_PAGES" version="'||version||'">');
    htpp('<formfields>');
    htpp('<recnumb10><![CDATA['||RECNUMB10||']]></recnumb10>');
    htpp('<recnump>'||RECNUMP||'</recnump>');
    htpp('<action><![CDATA['||ACTION||']]></action>');
    htpp('<page>'||PAGE||'</page>');
    htpp('<lang><![CDATA['||LANG||']]></lang>');
    htpp('<pformid><![CDATA['||PFORMID||']]></pformid>');
    htpp('<gbuttonsave><![CDATA['||GBUTTONSAVE||']]></gbuttonsave>');
    htpp('<gbuttoncompile><![CDATA['||GBUTTONCOMPILE||']]></gbuttoncompile>');
    htpp('<gbuttonres><![CDATA['||GBUTTONRES||']]></gbuttonres>');
    htpp('<gbuttonprev><![CDATA['||GBUTTONPREV||']]></gbuttonprev>');
    htpp('<error><![CDATA['||ERROR||']]></error>');
    htpp('<message><![CDATA['||MESSAGE||']]></message>');
    htpp('<warning><![CDATA['||WARNING||']]></warning>');
    htpp('<hintcontent><![CDATA['||HINTCONTENT||']]></hintcontent>');
    htpp('</formfields>');
    if ShowBlockp_DIV then
    htpp('<p>');
    htpp('<element>');
    htpp('<pfilter><![CDATA['||PFILTER(1)||']]></pfilter>');
    htpp('<pgbtnsrc><![CDATA['||PGBTNSRC(1)||']]></pgbtnsrc>');
    htpp('<sessstorageenabled><![CDATA['||PSESSSTORAGEENABLED(1)||']]></sessstorageenabled>');
    htpp('</element>');
  htpp('</p>');
  end if;
    if ShowBlockb10_DIV then
    htpp('<b10>');
  for i__ in 1..
B10FORMID
.count loop
    htpp('<element>');
    htpp('<b10rs><![CDATA['||B10RS(i__)||']]></b10rs>');
    htpp('<b10formid><![CDATA['||B10FORMID(i__)||']]></b10formid>');
    htpp('<b10blockid><![CDATA['||B10BLOCKID(i__)||']]></b10blockid>');
    htpp('<b10blok><![CDATA['||B10BLOK(i__)||']]></b10blok>');
    htpp('<b10fieldid><![CDATA['||B10FIELDID(i__)||']]></b10fieldid>');
    htpp('<b10page0><![CDATA['||B10PAGE0(i__)||']]></b10page0>');
    htpp('<b10page1><![CDATA['||B10PAGE1(i__)||']]></b10page1>');
    htpp('<b10page2><![CDATA['||B10PAGE2(i__)||']]></b10page2>');
    htpp('<b10page3><![CDATA['||B10PAGE3(i__)||']]></b10page3>');
    htpp('<b10page4><![CDATA['||B10PAGE4(i__)||']]></b10page4>');
    htpp('<b10page5><![CDATA['||B10PAGE5(i__)||']]></b10page5>');
    htpp('<b10page6><![CDATA['||B10PAGE6(i__)||']]></b10page6>');
    htpp('<b10page7><![CDATA['||B10PAGE7(i__)||']]></b10page7>');
    htpp('<b10page8><![CDATA['||B10PAGE8(i__)||']]></b10page8>');
    htpp('<b10page9><![CDATA['||B10PAGE9(i__)||']]></b10page9>');
    htpp('<b10page10><![CDATA['||B10PAGE10(i__)||']]></b10page10>');
    htpp('<b10page11><![CDATA['||B10PAGE11(i__)||']]></b10page11>');
    htpp('<b10page12><![CDATA['||B10PAGE12(i__)||']]></b10page12>');
    htpp('<b10page13><![CDATA['||B10PAGE13(i__)||']]></b10page13>');
    htpp('<b10page14><![CDATA['||B10PAGE14(i__)||']]></b10page14>');
    htpp('<b10page15><![CDATA['||B10PAGE15(i__)||']]></b10page15>');
    htpp('<b10page16><![CDATA['||B10PAGE16(i__)||']]></b10page16>');
    htpp('<b10page17><![CDATA['||B10PAGE17(i__)||']]></b10page17>');
    htpp('<b10page18><![CDATA['||B10PAGE18(i__)||']]></b10page18>');
    htpp('<b10page19><![CDATA['||B10PAGE19(i__)||']]></b10page19>');
    htpp('<b10page99><![CDATA['||B10PAGE99(i__)||']]></b10page99>');
    htpp('<b10rf1><![CDATA['||B10RF1(i__)||']]></b10rf1>');
    htpp('<b10rf2><![CDATA['||B10RF2(i__)||']]></b10rf2>');
    htpp('<b10rf3><![CDATA['||B10RF3(i__)||']]></b10rf3>');
    htpp('<b10rf4><![CDATA['||B10RF4(i__)||']]></b10rf4>');
    htpp('<b10rf5><![CDATA['||B10RF5(i__)||']]></b10rf5>');
    htpp('<b10rf6><![CDATA['||B10RF6(i__)||']]></b10rf6>');
    htpp('<b10rf7><![CDATA['||B10RF7(i__)||']]></b10rf7>');
    htpp('<b10rf8><![CDATA['||B10RF8(i__)||']]></b10rf8>');
    htpp('<b10rf9><![CDATA['||B10RF9(i__)||']]></b10rf9>');
    htpp('<b10rf10><![CDATA['||B10RF10(i__)||']]></b10rf10>');
    htpp('<b10rf11><![CDATA['||B10RF11(i__)||']]></b10rf11>');
    htpp('<b10rf12><![CDATA['||B10RF12(i__)||']]></b10rf12>');
    htpp('<b10rf13><![CDATA['||B10RF13(i__)||']]></b10rf13>');
    htpp('<b10rf14><![CDATA['||B10RF14(i__)||']]></b10rf14>');
    htpp('<b10rf15><![CDATA['||B10RF15(i__)||']]></b10rf15>');
    htpp('<b10rf16><![CDATA['||B10RF16(i__)||']]></b10rf16>');
    htpp('<b10rf17><![CDATA['||B10RF17(i__)||']]></b10rf17>');
    htpp('<b10rf18><![CDATA['||B10RF18(i__)||']]></b10rf18>');
    htpp('<b10rf19><![CDATA['||B10RF19(i__)||']]></b10rf19>');
    htpp('<b10rf99><![CDATA['||B10RF99(i__)||']]></b10rf99>');
    htpp('</element>');
  end loop;
  htpp('</b10>');
  end if;
    if ShowBlockb30_DIV then
    htpp('<b30>');
  for i__ in 1..
B30TEXT
.count loop
    htpp('<element>');
    htpp('<b30text><![CDATA['||B30TEXT(i__)||']]></b30text>');
    htpp('</element>');
  end loop;
  htpp('</b30>');
  end if;
    htpp('</form>');
else
    htpp('{"form":{"@name":"RASDC2_PAGES","@version":"'||version||'",' );
    htpp('"formfields": {');
    htpp('"recnumb10":"'||escapeRest(RECNUMB10)||'"');
    htpp(',"recnump":"'||RECNUMP||'"');
    htpp(',"action":"'||escapeRest(ACTION)||'"');
    htpp(',"page":"'||PAGE||'"');
    htpp(',"lang":"'||escapeRest(LANG)||'"');
    htpp(',"pformid":"'||escapeRest(PFORMID)||'"');
    htpp(',"gbuttonsave":"'||escapeRest(GBUTTONSAVE)||'"');
    htpp(',"gbuttoncompile":"'||escapeRest(GBUTTONCOMPILE)||'"');
    htpp(',"gbuttonres":"'||escapeRest(GBUTTONRES)||'"');
    htpp(',"gbuttonprev":"'||escapeRest(GBUTTONPREV)||'"');
    htpp(',"error":"'||escapeRest(ERROR)||'"');
    htpp(',"message":"'||escapeRest(MESSAGE)||'"');
    htpp(',"warning":"'||escapeRest(WARNING)||'"');
    htpp(',"hintcontent":"'||escapeRest(HINTCONTENT)||'"');
    htpp('},');
    if ShowBlockp_DIV then
    htpp('"p":[');
     htpp('{');
    htpp('"pfilter":"'||escapeRest(PFILTER(1))||'"');
    htpp(',"pgbtnsrc":"'||escapeRest(PGBTNSRC(1))||'"');
    htpp(',"sessstorageenabled":"'||escapeRest(PSESSSTORAGEENABLED(1))||'"');
    htpp('}');
    htpp(']');
  else
    htpp('"p":[]');
  end if;
    if ShowBlockb10_DIV then
    htpp(',"b10":[');
  v_firstrow__ := true;
  for i__ in 1..
B10FORMID
.count loop
    if v_firstrow__ then
     htpp('{');
     v_firstrow__ := false;
    else
     htpp(',{');
    end if;
    htpp('"b10rs":"'||escapeRest(B10RS(i__))||'"');
    htpp(',"b10formid":"'||escapeRest(B10FORMID(i__))||'"');
    htpp(',"b10blockid":"'||escapeRest(B10BLOCKID(i__))||'"');
    htpp(',"b10blok":"'||escapeRest(B10BLOK(i__))||'"');
    htpp(',"b10fieldid":"'||escapeRest(B10FIELDID(i__))||'"');
    htpp(',"b10page0":"'||escapeRest(B10PAGE0(i__))||'"');
    htpp(',"b10page1":"'||escapeRest(B10PAGE1(i__))||'"');
    htpp(',"b10page2":"'||escapeRest(B10PAGE2(i__))||'"');
    htpp(',"b10page3":"'||escapeRest(B10PAGE3(i__))||'"');
    htpp(',"b10page4":"'||escapeRest(B10PAGE4(i__))||'"');
    htpp(',"b10page5":"'||escapeRest(B10PAGE5(i__))||'"');
    htpp(',"b10page6":"'||escapeRest(B10PAGE6(i__))||'"');
    htpp(',"b10page7":"'||escapeRest(B10PAGE7(i__))||'"');
    htpp(',"b10page8":"'||escapeRest(B10PAGE8(i__))||'"');
    htpp(',"b10page9":"'||escapeRest(B10PAGE9(i__))||'"');
    htpp(',"b10page10":"'||escapeRest(B10PAGE10(i__))||'"');
    htpp(',"b10page11":"'||escapeRest(B10PAGE11(i__))||'"');
    htpp(',"b10page12":"'||escapeRest(B10PAGE12(i__))||'"');
    htpp(',"b10page13":"'||escapeRest(B10PAGE13(i__))||'"');
    htpp(',"b10page14":"'||escapeRest(B10PAGE14(i__))||'"');
    htpp(',"b10page15":"'||escapeRest(B10PAGE15(i__))||'"');
    htpp(',"b10page16":"'||escapeRest(B10PAGE16(i__))||'"');
    htpp(',"b10page17":"'||escapeRest(B10PAGE17(i__))||'"');
    htpp(',"b10page18":"'||escapeRest(B10PAGE18(i__))||'"');
    htpp(',"b10page19":"'||escapeRest(B10PAGE19(i__))||'"');
    htpp(',"b10page99":"'||escapeRest(B10PAGE99(i__))||'"');
    htpp(',"b10rf1":"'||escapeRest(B10RF1(i__))||'"');
    htpp(',"b10rf2":"'||escapeRest(B10RF2(i__))||'"');
    htpp(',"b10rf3":"'||escapeRest(B10RF3(i__))||'"');
    htpp(',"b10rf4":"'||escapeRest(B10RF4(i__))||'"');
    htpp(',"b10rf5":"'||escapeRest(B10RF5(i__))||'"');
    htpp(',"b10rf6":"'||escapeRest(B10RF6(i__))||'"');
    htpp(',"b10rf7":"'||escapeRest(B10RF7(i__))||'"');
    htpp(',"b10rf8":"'||escapeRest(B10RF8(i__))||'"');
    htpp(',"b10rf9":"'||escapeRest(B10RF9(i__))||'"');
    htpp(',"b10rf10":"'||escapeRest(B10RF10(i__))||'"');
    htpp(',"b10rf11":"'||escapeRest(B10RF11(i__))||'"');
    htpp(',"b10rf12":"'||escapeRest(B10RF12(i__))||'"');
    htpp(',"b10rf13":"'||escapeRest(B10RF13(i__))||'"');
    htpp(',"b10rf14":"'||escapeRest(B10RF14(i__))||'"');
    htpp(',"b10rf15":"'||escapeRest(B10RF15(i__))||'"');
    htpp(',"b10rf16":"'||escapeRest(B10RF16(i__))||'"');
    htpp(',"b10rf17":"'||escapeRest(B10RF17(i__))||'"');
    htpp(',"b10rf18":"'||escapeRest(B10RF18(i__))||'"');
    htpp(',"b10rf19":"'||escapeRest(B10RF19(i__))||'"');
    htpp(',"b10rf99":"'||escapeRest(B10RF99(i__))||'"');
    htpp('}');
  end loop;
    htpp(']');
  else
    htpp(',"b10":[]');
  end if;
    if ShowBlockb30_DIV then
    htpp(',"b30":[');
  v_firstrow__ := true;
  for i__ in 1..
B30TEXT
.count loop
    if v_firstrow__ then
     htpp('{');
     v_firstrow__ := false;
    else
     htpp(',{');
    end if;
    htpp('"b30text":"'||escapeRest(B30TEXT(i__))||'"');
    htpp('}');
  end loop;
    htpp(']');
  else
    htpp(',"b30":[]');
  end if;
    htpp('}}');
end if;
return v_clob__;
null; end;
procedure poutputrest is
begin
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
else
    OWA_UTIL.mime_header('application/json', FALSE ,'utf-8');
    OWA_UTIL.http_header_close;
end if;
htpclob(poutputrest);
end;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

--<ON_ACTION formid="91" blockid="">
  rasdc_library.log(this_form,pformid, 'START', compid);



  psubmit(name_array ,value_array);

  rasd_client.secCheckPermission(this_form,ACTION);



  if ACTION is null then null;

    RECNUMP := 1;

    RECNUMB10 := 1;

    pselect;

    poutput;

  elsif ACTION = GBUTTONSRC then     RECNUMB10 := 1;

    RECNUMP := 1;

    pselect;

    poutput;

  elsif ACTION = GBUTTONSAVE then



           pcommit;

           rasdc_library.RefData(PFORMID);

           pselect;



        if xrform = 'Y' then

		   message :=  message ||  RASDI_TRNSLT.text('To unlink referenced code check:', lang)||'<input type="checkbox" name="UNLINK" value="Y"/>.';

		end if;





    message := message || RASDI_TRNSLT.text('Changes are saved.', lang) ;





    poutput;



  elsif ACTION = GBUTTONCLR then     pclear;

    poutput;



  elsif ACTION = GBUTTONCOMPILE then



        rasdc_library.log(this_form,pformid, 'COMMIT_S', compid);

        pcommit;

        rasdc_library.log(this_form,pformid, 'COMMIT_E', compid);

        commit;

        rasdc_library.log(this_form,pformid, 'REF_S', compid);

        rasdc_library.RefData(PFORMID);

        rasdc_library.log(this_form,pformid, 'REF_E', compid);



        compile(pformid , pform , lang ,  message, xrform  , compid);



        rasdc_library.log(this_form,pformid, 'SELECT_S', compid);

        pselect;

        rasdc_library.log(this_form,pformid, 'SELECT_E', compid);



      rasdc_library.log(this_form,pformid, 'POUTPUT_S', compid);

    poutput;

      rasdc_library.log(this_form,pformid, 'POUTPUT_E', compid);





  end if;

      rasdc_library.log(this_form,pformid, 'END', compid);
--</ON_ACTION>
--<POST_ACTION formid="91" blockid="">
--



/*

-- manjka še:



 - v SESSSTORAGEENABLED dodati SQL kodo če ne bo popup iste LISTLOV

 - v LISTLOV prenesti paramtrer FORMID







*/
--</POST_ACTION>
    pLog;
exception
  when rasd_client.e_finished then pLog;
  when others then
    htp.p('<html>
<head>');
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||''));
htp.p('');
htp.p('<script type="text/javascript">');
formgen_js;
htp.p('</script>');
htpClob(FORM_UIHEAD);
htp.p('<style type="text/css">');
htpClob(FORM_CSS);
htp.p('</style><script type="text/javascript">');  htp.p('</script>');

htp.prn('</head><body><div id="RASDC2_PAGES_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASDC2_PAGES_LAB',''|| rasdc_library.formName(PFORMID, LANG) ||'') ||'     </div><div class="rasdForm"><div class="rasdFormHead"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton"></div><div class="rasdHtmlError">  <div class="rasdHtmlErrorText"><div class="rasdHtmlErrorText">'||sqlerrm||'('||sqlcode||')</div></div><div class="rasdHtmlErrorTextDetail">');declare   v_trace varchar2(32000) := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;   v_nl varchar2(2) := chr(10); begin rlog('ERROR:'||v_trace); htp.p ( 'Error trace'||':'||'<br/>'|| replace(v_trace, v_nl ,'<br/>'));htp.p ( '</div><div class="rasdHtmlErrorTextDetail">'||'Error stack'||':'||'<br/>'|| replace(DBMS_UTILITY.FORMAT_ERROR_STACK, v_nl ,'<br/>'));rlog('ERROR:'||DBMS_UTILITY.FORMAT_ERROR_STACK); htp.p('</div>');rlog('ERROR:...'); declare   v_line  number;  v_x varchar2(32000); begin v_x := substr(v_trace,1,instr(v_trace, v_nl )-1 );  v_line := substr(v_x,instr(v_x,' ',-1));for r in  (select line, text from user_source s where s.name = 'RASDC2_PAGES' and line > v_line-5 and line < v_line+5 ) loop rlog('ERROR:'||r.line||' - '||r.text); end loop;  rlog('ERROR:...'); exception when others then null;end;end;htp.p('</div><div class="rasdFormFooter"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton">'|| rasd_client.getHtmlFooter(version , substr('RASDC2_PAGES_FOOTER',1,instr('RASDC2_PAGES_FOOTER', '_',-1)-1) , '') ||'</div></div></body></html>    ');
    pLog;
end;
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_PAGES',ACTION);
  if ACTION = GBUTTONSAVE then     pselect;
    pcommit;
  end if;

--<POST_ACTION formid="91" blockid="">
--



/*

-- manjka še:



 - v SESSSTORAGEENABLED dodati SQL kodo če ne bo popup iste LISTLOV

 - v LISTLOV prenesti paramtrer FORMID







*/
--</POST_ACTION>
-- Error handler for the main program.
 exception
  when rasd_client.e_finished then null;

end;
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

  -- The program execution sequence based on  ACTION defined.
  psubmitrest(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_PAGES',ACTION);
  if ACTION is null then null;
    RECNUMP := 1;
    RECNUMB10 := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSRC or ACTION is null  then     RECNUMP := 1;
    RECNUMB10 := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSAVE then     pcommit;
    pselect;
    --if MESSAGE is null then
    --MESSAGE := 'Form is changed.';
    --end if;
    poutputrest;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutputrest;
  end if;

--<POST_ACTION formid="91" blockid="">
--



/*

-- manjka še:



 - v SESSSTORAGEENABLED dodati SQL kodo če ne bo popup iste LISTLOV

 - v LISTLOV prenesti paramtrer FORMID







*/
--</POST_ACTION>
-- Error handler for the rest program.
 exception
  when rasd_client.e_finished then null;
  when others then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
    htp.p('<?xml version="1.0" encoding="UTF-8"?>
<form name="RASDC2_PAGES" version="'||version||'">');     htp.p('<error>');     htp.p('  <errorcode>'||sqlcode||'</errorcode>');     htp.p('  <errormessage>'||replace(sqlerrm,'<','&lt;')||'</errormessage>');     htp.p('</error>');     htp.p('</form>'); else
    OWA_UTIL.mime_header('application/json', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
    htp.p('{"form":{"@name":"RASDC2_PAGES","@version":"'||version||'",' );     htp.p('"error":{');     htp.p('  "errorcode":"'||sqlcode||'",');     htp.p('  "errormessage":"'||replace(sqlerrm,'"','\"')||'"');     htp.p('}');     htp.p('}}'); end if;

end;
function metadata_xml return cctab is
  v_clob clob := '';
  v_vc cctab;
  begin
 v_vc(1) := '<form><formid>91</formid><form>RASDC2_PAGES</form><version>1</version><change>28.02.2024 09/09/07</change><user>RASDDEV</user><label><![CDATA[<%= rasdc_library.formName(PFORMID, LANG) %>]]></label><lobid>RASDDEV</lobid><program>?</program><referenceyn>Y</referenceyn><autodeletehtmlyn>Y</autodeletehtmlyn><autocreaterestyn>Y</autocreaterestyn><autocreatebatchyn>Y</autocreatebatchyn><addmetadatainfoyn>Y</addmetadatainfoyn><compiler><engineid>11</engineid><server>rasd_engine11</server><client>rasd_enginehtml11</client><library>rasd_client</library></compiler><compiledInfo><info><engineid>11</engineid><change>29.03.2023 11/32/12</change><compileyn>Y</compileyn><application>RASD 2.0</application><owner>domen</owner><editor>domen</editor></info></compiledInfo><blocks><block><blockid>B10</blockid><sqltable></sqltable><numrows>0</numrows><emptyrows></emptyrows><dbblockyn>Y</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext><![CDATA[from(

   select b.formid,

                 b.blockid,

                 ''<B>''||b.blockid||''</B> <span style="font-size: x-small;">''||b.label||''</span>'' blok,

                 '''' fieldid,

(select min(x.orderby) from rasd_fields x

where x.formid = b.formid

and x.blockid = b.blockid) orderby,

                  1 vr,

                  decode(s0.page,

                        0,

                        ''Y'',

                        decode(s1.page,

                               to_number(null),

                               decode(s2.page,

                                      to_number(null),

                                      decode(s3.page,

                                             to_number(null),

                                             decode(s4.page,

                                                    to_number(null),

                                                    decode(s5.page,

                                                           to_number(null),

                                                           decode(s6.page,

                                                                  to_number(null),

                                                                  decode(s7.page,

                                                                         to_number(null),

                                                                         decode(s8.page,

  ';
 v_vc(2) := '                                                                              to_number(null),

                                                                                decode(s9.page,

                                                                                       to_number(null),

                                        decode(s10.page,

                        to_number(null),

                        decode(s11.page,

                               to_number(null),

                               decode(s12.page,

                                      to_number(null),

                                      decode(s13.page,

                                             to_number(null),

                                             decode(s14.page,

                                                    to_number(null),

                                                    decode(s15.page,

                                                           to_number(null),

                                                           decode(s16.page,

                                                                  to_number(null),

                                                                  decode(s17.page,

                                                                         to_number(null),

                                                                         decode(s18.page,

                                                                                to_number(null),

                                                                                decode(s19.page,

                                                                                       to_number(null),

                        ''Y'',

                                                                                       ''N''),

                                                                                ''N''),

                                                                         ''N''),

                                                                  ''N''),

                                                           ''N''),

                                                    ''N''),

                                             ''N''),

                                      ''N''),

                               ''N'')),

                                                      ';
 v_vc(3) := '                                 ''N''),

                                                                                ''N''),

                                                                         ''N''),

                                                                  ''N''),

                                                           ''N''),

                                                    ''N''),

                                             ''N''),

                                      ''N''),

                               ''N'')) page0,

                 decode(s1.page, 1, ''Y'', ''N'') page1,

                 decode(s2.page, 2, ''Y'', ''N'') page2,

                 decode(s3.page, 3, ''Y'', ''N'') page3,

                 decode(s4.page, 4, ''Y'', ''N'') page4,

                 decode(s5.page, 5, ''Y'', ''N'') page5,

                 decode(s6.page, 6, ''Y'', ''N'') page6,

                 decode(s7.page, 7, ''Y'', ''N'') page7,

                 decode(s8.page, 8, ''Y'', ''N'') page8,

                 decode(s9.page, 9, ''Y'', ''N'') page9,

                 decode(s10.page, 10, ''Y'', ''N'') page10,

                 decode(s11.page, 11, ''Y'', ''N'') page11,

                 decode(s12.page, 12, ''Y'', ''N'') page12,

                 decode(s13.page, 13, ''Y'', ''N'') page13,

                 decode(s14.page, 14, ''Y'', ''N'') page14,

                 decode(s15.page, 15, ''Y'', ''N'') page15,

                 decode(s16.page, 16, ''Y'', ''N'') page16,

                 decode(s17.page, 17, ''Y'', ''N'') page17,

                 decode(s18.page, 18, ''Y'', ''N'') page18,

                 decode(s19.page, 19, ''Y'', ''N'') page19,

                 decode(s99.page, 99, ''Y'', ''N'') page99,

                 s1.rform rf1,

                 s2.rform rf2,

                 s3.rform rf3,

                 s4.rform rf4,

                 s5.rform rf5,

                 s6.rform rf6,

                 s7.rform rf7,

                 s8.rform rf8,

                 s9.rform rf9,

                 s10.rform rf10,

                 s11.rform rf11,

                 s12.rform rf12,

                 s13.rform rf13,

                 s14.rform rf14,

                 s15.rform rf15,

                 s16.rform rf16,

                 s17.rform rf17,

                 s18.rform rf18,

                 s19.rform rf19,

                 s99.rform rf99

            from RASD_BLOCKS b,

                 RASD_PAGES  s0,

            ';
 v_vc(4) := '     RASD_PAGES  s1,

                 RASD_PAGES  s2,

                 RASD_PAGES  s3,

                 RASD_PAGES  s4,

                 RASD_PAGES  s5,

                 RASD_PAGES  s6,

                 RASD_PAGES  s7,

                 RASD_PAGES  s8,

                 RASD_PAGES  s9,

                 RASD_PAGES  s10,

                 RASD_PAGES  s11,

                 RASD_PAGES  s12,

                 RASD_PAGES  s13,

                 RASD_PAGES  s14,

                 RASD_PAGES  s15,

                 RASD_PAGES  s16,

                 RASD_PAGES  s17,

                 RASD_PAGES  s18,

                 RASD_PAGES  s19,

                 RASD_PAGES  s99

           where b.formid = PFORMID

             and b.blockid like upper(pfilter)||''%''

             and b.formid = s0.formid(+)

             and b.blockid = s0.blockid(+)

             and s0.page(+) = 0

             and b.formid = s1.formid(+)

             and b.blockid = s1.blockid(+)

             and s1.page(+) = 1

             and b.formid = s2.formid(+)

             and b.blockid = s2.blockid(+)

             and s2.page(+) = 2

             and b.formid = s3.formid(+)

             and b.blockid = s3.blockid(+)

             and s3.page(+) = 3

             and b.formid = s4.formid(+)

             and b.blockid = s4.blockid(+)

             and s4.page(+) = 4

             and b.formid = s5.formid(+)

             and b.blockid = s5.blockid(+)

             and s5.page(+) = 5

             and b.formid = s6.formid(+)

             and b.blockid = s6.blockid(+)

             and s6.page(+) = 6

             and b.formid = s7.formid(+)

             and b.blockid = s7.blockid(+)

             and s7.page(+) = 7

             and b.formid = s8.formid(+)

             and b.blockid = s8.blockid(+)

             and s8.page(+) = 8

             and b.formid = s9.formid(+)

             and b.blockid = s9.blockid(+)

             and s9.page(+) = 9

             and b.formid = s10.formid(+)

             and b.blockid = s10.blockid(+)

             and s10.page(+) = 10

             and b.formid = s11.formid(+)

             and b.blockid = s11.blockid(+)

             and s11.page(+) = 11

             and b.formid = s12.formid(+)

             and b.blockid = s12.blockid(+)

             and s12.page(+) = 12

             and b.formid = s13.formid(+)

             and b.blockid = ';
 v_vc(5) := 's13.blockid(+)

             and s13.page(+) = 13

             and b.formid = s14.formid(+)

             and b.blockid = s14.blockid(+)

             and s14.page(+) = 14

             and b.formid = s15.formid(+)

             and b.blockid = s15.blockid(+)

             and s15.page(+) = 15

             and b.formid = s16.formid(+)

             and b.blockid = s16.blockid(+)

             and s16.page(+) = 16

             and b.formid = s17.formid(+)

             and b.blockid = s17.blockid(+)

             and s17.page(+) = 17

             and b.formid = s18.formid(+)

             and b.blockid = s18.blockid(+)

             and s18.page(+) = 18

             and b.formid = s19.formid(+)

             and b.blockid = s19.blockid(+)

             and s19.page(+) = 19

             and b.formid = s99.formid(+)

             and b.blockid = s99.blockid(+)

             and s99.page(+) = 99

union

          select f.formid,

                 f.blockid,

                 f.fieldid||'' <span style="font-size: x-small;">''|| RASDI_TRNSLT.text(''Show on UI:'',lang)||'' ''||nvl(f.elementyn,rasd_engine11.c_false)||''</span>'' blok,

                 f.fieldid fieldid,

                 f.orderby,

                 2 vr,

                 decode(s0.page,

                        0,

                        ''Y'',

                        decode(s1.page,

                               to_number(null),

                               decode(s2.page,

                                      to_number(null),

                                      decode(s3.page,

                                             to_number(null),

                                             decode(s4.page,

                                                    to_number(null),

                                                    decode(s5.page,

                                                           to_number(null),

                                                           decode(s6.page,

                                                                  to_number(null),

                                                                  decode(s7.page,

                                                                         to_number(null),

                                                                         decode(s8.page,

                                                           ';
 v_vc(6) := '                     to_number(null),

                                                                                decode(s9.page,

                                                                                       to_number(null),

                      decode(s10.page,

                        to_number(null),

                        decode(s11.page,

                               to_number(null),

                               decode(s12.page,

                                      to_number(null),

                                      decode(s13.page,

                                             to_number(null),

                                             decode(s14.page,

                                                    to_number(null),

                                                    decode(s15.page,

                                                           to_number(null),

                                                           decode(s16.page,

                                                                  to_number(null),

                                                                  decode(s17.page,

                                                                         to_number(null),

                                                                         decode(s18.page,

                                                                                to_number(null),

                                                                                decode(s19.page,

                                                                                       to_number(null),

                        ''Y'',

                                                                                       ''N''),

                                                                                ''N''),

                                                                         ''N''),

                                                                  ''N''),

                                                           ''N''),

                                                    ''N''),

                                             ''N''),

                                      ''N''),

                               ''N'')),

                                                                                       ''N''),

                                   ';
 v_vc(7) := '                                             ''N''),

                                                                         ''N''),

                                                                  ''N''),

                                                           ''N''),

                                                    ''N''),

                                             ''N''),

                                      ''N''),

                               ''N'')) page0,

                 decode(s1.page, 1, ''Y'', ''N'') page1,

                 decode(s2.page, 2, ''Y'', ''N'') page2,

                 decode(s3.page, 3, ''Y'', ''N'') page3,

                 decode(s4.page, 4, ''Y'', ''N'') page4,

                 decode(s5.page, 5, ''Y'', ''N'') page5,

                 decode(s6.page, 6, ''Y'', ''N'') page6,

                 decode(s7.page, 7, ''Y'', ''N'') page7,

                 decode(s8.page, 8, ''Y'', ''N'') page8,

                 decode(s9.page, 9, ''Y'', ''N'') page9,

                 decode(s10.page, 10, ''Y'', ''N'') page10,

                 decode(s11.page, 11, ''Y'', ''N'') page11,

                 decode(s12.page, 12, ''Y'', ''N'') page12,

                 decode(s13.page, 13, ''Y'', ''N'') page13,

                 decode(s14.page, 14, ''Y'', ''N'') page14,

                 decode(s15.page, 15, ''Y'', ''N'') page15,

                 decode(s16.page, 16, ''Y'', ''N'') page16,

                 decode(s17.page, 17, ''Y'', ''N'') page17,

                 decode(s18.page, 18, ''Y'', ''N'') page18,

                 decode(s19.page, 19, ''Y'', ''N'') page19,

                 decode(s99.page, 99, ''Y'', ''N'') page99,

                 s1.rform rf1,

                 s2.rform rf2,

                 s3.rform rf3,

                 s4.rform rf4,

                 s5.rform rf5,

                 s6.rform rf6,

                 s7.rform rf7,

                 s8.rform rf8,

                 s9.rform rf9,

                 s10.rform rf10,

                 s11.rform rf11,

                 s12.rform rf12,

                 s13.rform rf13,

                 s14.rform rf14,

                 s15.rform rf15,

                 s16.rform rf16,

                 s17.rform rf17,

                 s18.rform rf18,

                 s19.rform rf19,

                 s99.rform rf99

            from rasd_fields f,

                 RASD_PAGES  s0,

                 RASD_PAGES  s1,

                 RASD_PAGES  s2,

                 RA';
 v_vc(8) := 'SD_PAGES  s3,

                 RASD_PAGES  s4,

                 RASD_PAGES  s5,

                 RASD_PAGES  s6,

                 RASD_PAGES  s7,

                 RASD_PAGES  s8,

                 RASD_PAGES  s9,

                 RASD_PAGES  s10,

                 RASD_PAGES  s11,

                 RASD_PAGES  s12,

                 RASD_PAGES  s13,

                 RASD_PAGES  s14,

                 RASD_PAGES  s15,

                 RASD_PAGES  s16,

                 RASD_PAGES  s17,

                 RASD_PAGES  s18,

                 RASD_PAGES  s19,

                 RASD_PAGES  s99

           where f.formid = PFORMID

             and f.blockid is null

             and f.fieldid like upper(pfilter)||''%''

             and nvl(f.element,''INPUT_TEXT'') not in (''INPUT_HIDDEN'')

             and f.fieldid <> rasd_engine11.c_message

             and f.fieldid <> rasd_engine11.c_action

             and f.fieldid <> rasd_engine11.c_restrestype

             and f.fieldid <> rasd_engine11.c_restrequest

             and f.fieldid <> rasd_engine11.c_fin

             and f.fieldid <> rasd_engine11.c_warning

             and f.fieldid <> rasd_engine11.c_error

             and f.fieldid <> rasd_engine11.c_page

--             and nvl(f.elementyn,rasd_engine11.c_false) = rasd_engine11.c_true

             and f.formid = s0.formid(+)

             and f.fieldid = s0.fieldid(+)

             and s0.page(+) = 0

             and f.formid = s1.formid(+)

             and f.fieldid = s1.fieldid(+)

             and s1.page(+) = 1

             and f.formid = s2.formid(+)

             and f.fieldid = s2.fieldid(+)

             and s2.page(+) = 2

             and f.formid = s3.formid(+)

             and f.fieldid = s3.fieldid(+)

             and s3.page(+) = 3

             and f.formid = s4.formid(+)

             and f.fieldid = s4.fieldid(+)

             and s4.page(+) = 4

             and f.formid = s5.formid(+)

             and f.fieldid = s5.fieldid(+)

             and s5.page(+) = 5

             and f.formid = s6.formid(+)

             and f.fieldid = s6.fieldid(+)

             and s6.page(+) = 6

             and f.formid = s7.formid(+)

             and f.fieldid = s7.fieldid(+)

             and s7.page(+) = 7

             and f.formid = s8.formid(+)

             and f.fieldid = s8.fieldid(+)

             and s8.page(+) = 8
';
 v_vc(9) := '
             and f.formid = s9.formid(+)

             and f.fieldid = s9.fieldid(+)

             and s9.page(+) = 9

             and f.formid = s10.formid(+)

             and f.fieldid = s10.fieldid(+)

             and s10.page(+) = 10

             and f.formid = s11.formid(+)

             and f.fieldid = s11.fieldid(+)

             and s11.page(+) = 11

             and f.formid = s12.formid(+)

             and f.fieldid = s12.fieldid(+)

             and s12.page(+) = 12

             and f.formid = s13.formid(+)

             and f.fieldid = s13.fieldid(+)

             and s13.page(+) = 13

             and f.formid = s14.formid(+)

             and f.fieldid = s14.fieldid(+)

             and s14.page(+) = 14

             and f.formid = s15.formid(+)

             and f.fieldid = s15.fieldid(+)

             and s15.page(+) = 15

             and f.formid = s16.formid(+)

             and f.fieldid = s16.fieldid(+)

             and s16.page(+) = 16

             and f.formid = s17.formid(+)

             and f.fieldid = s17.fieldid(+)

             and s17.page(+) = 17

             and f.formid = s18.formid(+)

             and f.fieldid = s18.fieldid(+)

             and s18.page(+) = 18

             and f.formid = s19.formid(+)

             and f.fieldid = s19.fieldid(+)

             and s19.page(+) = 19

             and f.formid = s99.formid(+)

             and f.fieldid = s99.fieldid(+)

             and s99.page(+) = 99

)

order by vr, orderby]]></sqltext><label></label><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B10</blockid><fieldid>BLOCKID</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>1011</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10BLOCKID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>BLOK</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>1012</orderby><pkyn>N</pkyn><selectyn>Y</select';
 v_vc(10) := 'yn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10BLOK</nameid><label><![CDATA[<%= showLabel(''Block or field on  Page'','''',0) %>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>FIELDID</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>1014</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10FIELDID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>FORMID</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>1000</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10FORMID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>PAGE0</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1017</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE0</nameid><label><![CDATA[0]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE1</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1020</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</del';
 v_vc(11) := 'eteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE1</nameid><label><![CDATA[1]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE10</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1061</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE10</nameid><label><![CDATA[10]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE11</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1062</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE11</nameid><label><![CDATA[11]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE12</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1063</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE12</nameid><label><![CDATA[12]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE13</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1064</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn';
 v_vc(12) := '>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE13</nameid><label><![CDATA[13]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE14</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1065</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE14</nameid><label><![CDATA[14]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE15</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1070</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE15</nameid><label><![CDATA[15]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE16</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1500</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE16</nameid><label><![CDATA[16]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE17</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1538</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><default';
 v_vc(13) := 'val></defaultval><elementyn>Y</elementyn><nameid>B10PAGE17</nameid><label><![CDATA[17]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE18</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1550</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE18</nameid><label><![CDATA[18]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE19</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1560</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE19</nameid><label><![CDATA[19]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE2</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1022</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE2</nameid><label><![CDATA[2]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE3</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1024</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><na';
 v_vc(14) := 'meid>B10PAGE3</nameid><label><![CDATA[3]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE4</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1027</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE4</nameid><label><![CDATA[4]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE5</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1030</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE5</nameid><label><![CDATA[5]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE6</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1032</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE6</nameid><label><![CDATA[6]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE7</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1034</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE7</nameid><label><![CDATA[7]]></label><l';
 v_vc(15) := 'inkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE8</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1036</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE8</nameid><label><![CDATA[8]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE9</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1060</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE9</nameid><label><![CDATA[9]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PAGE99</fieldid><type>C</type><format></format><element>INPUT_CHECKBOX</element><hiddenyn></hiddenyn><orderby>1570</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PAGE99</nameid><label><![CDATA[<%= showLabel(''Session'','''',0) %>]]></label><linkid>link$CHKBXD</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>RF1</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2018</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF1</nameid><label></label><linkid></linkid><source>V</source><rlob';
 v_vc(16) := 'id></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF10</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2060</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF10</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF11</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2061</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF11</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF12</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2062</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF12</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF13</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2063</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF13</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF14</fieldid><type>C</';
 v_vc(17) := 'type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2064</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF14</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF15</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2065</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF15</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF16</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2070</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF16</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF17</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2500</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF17</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF18</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2550</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn';
 v_vc(18) := '>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF18</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF19</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2560</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF19</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF2</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2020</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF2</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF3</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2022</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF3</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF4</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2024</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF4</n';
 v_vc(19) := 'ameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF5</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2027</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF5</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF6</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2030</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF6</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF7</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2032</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF7</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF8</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2034</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF8</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><';
 v_vc(20) := 'blockid>B10</blockid><fieldid>RF9</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2036</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF9</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RF99</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>2570</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RF99</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RS</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>999</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RS</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block><block><blockid>B30</blockid><sqltable></sqltable><numrows>0</numrows><emptyrows></emptyrows><dbblockyn>Y</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext><![CDATA[          select ''<A HREF="javascript:var x = window.open(encodeURI(''''!RASDC_ERRORS.Program?PPROGRAM='' ||

                 upper(name) || ''#'' || substr(type, 1, 1) ||

                 substr(type, instr(type, '' '') + 1, 1) || to_char(line) ||

                 ''''''),''''nx'''','''''''');"  style="color: Red;">ERR: ('' ||

                 to_char(line) || '','' || to_char(position) || '')  '' || text ||

                 ''</A>'' text

            from all_errors

           where upper(name) = upper(pform)

             and owner = ';
 v_vc(21) := 'rasdc_library.currentDADUser

           order by line, position]]></sqltext><label><![CDATA[<%= RASDI_TRNSLT.text(''Errors'',LANG)%>]]></label><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B30</blockid><fieldid>TEXT</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>10000</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B30TEXT</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block><block><blockid>P</blockid><sqltable></sqltable><numrows>1</numrows><emptyrows></emptyrows><dbblockyn>N</dbblockyn><rowidyn>N</rowidyn><pagingyn>Y</pagingyn><clearyn>N</clearyn><sqltext></sqltext><label></label><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>P</blockid><fieldid>FILTER</fieldid><type>C</type><format></format><element>INPUT_TEXT</element><hiddenyn></hiddenyn><orderby>100</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PFILTER</nameid><label><![CDATA[<%= showLabel(''Filter'','''',0) %>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>P</blockid><fieldid>GBTNSRC</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>120</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[gbuttonsrc]]></defaultval><elementyn>Y</elementyn><nameid>PGBTNSRC</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>P</blockid><fieldid>PF</fieldid><type>C</type><format></format><element>INPUT_TE';
 v_vc(22) := 'XT</element><hiddenyn></hiddenyn><orderby>110</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>PF</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>P</blockid><fieldid>SESSSTORAGEENABLED</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>300</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>SESSSTORAGEENABLED</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block></blocks><fields><field><blockid></blockid><fieldid>ACTION</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ACTION</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>ACTION</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>COMPID</fieldid><type>N</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>COMPID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>ERROR</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>50</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn>';
 v_vc(23) := '<updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ERROR</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>ERROR</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONCLR</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''GBUTTONCLR'']]></defaultval><elementyn>N</elementyn><nameid>GBUTTONCLR</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONCLR</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>13</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Compile'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONCOMPILE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONCOMPILE</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONPREV</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>15</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Preview'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONPREV</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONPREV</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONRES</fieldid><type>C</type><format></format><element>INPUT_RESET</element><hiddenyn';
 v_vc(24) := '></hiddenyn><orderby>14</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Reset'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONRES</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONRES</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>12</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Save'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONSAVE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONSAVE</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONSRC</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>9</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Search'',LANG)]]></defaultval><elementyn>N</elementyn><nameid>GBUTTONSRC</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONSRC</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>HINTCONTENT</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>HINTCONTENT</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>HINTCONTENT</rfieldid><includevis>N</includevis></field><field><blocki';
 v_vc(25) := 'd></blockid><fieldid>LANG</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>5</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>LANG</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>LANG</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>MESSAGE</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>51</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>MESSAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>MESSAGE</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PAGE</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[0]]></defaultval><elementyn>Y</elementyn><nameid>PAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PAGE</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PFORM</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>7</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>PFORM</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PFORM</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PFORMID</fieldid><type>C</type><format></format><element>INPUT_HIDDE';
 v_vc(26) := 'N</element><hiddenyn></hiddenyn><orderby>6</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PFORMID</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PFORMID</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMB10</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-1</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>RECNUMB10</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMP</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-1</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[1]]></defaultval><elementyn>Y</elementyn><nameid>RECNUMP</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>UNLINK</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>UNLINK</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>VLOB</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>81</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</delete';
 v_vc(27) := 'yn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>VLOB</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>VLOB</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>VUSER</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>80</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>VUSER</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>VUSER</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>WARNING</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>52</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>WARNING</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>WARNING</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>XRFORM</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>XRFORM</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields><links><link><linkid>link$CHKBXD</linkid><link>CHKBXD</link><type>C</type><location></location><text></text><source>G</source><hiddenyn>N</hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>FALSE</paramid><type>FALSE</type><orderby>2</orderby><blockid></blockid><fieldid>THIS</fieldid><namecid></namecid><code>N</code><value><![CDATA[N]]><';
 v_vc(28) := '/value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>TRUE</paramid><type>TRUE</type><orderby>1</orderby><blockid></blockid><fieldid>THIS</fieldid><namecid></namecid><code>Y</code><value><![CDATA[Y]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$blocks</linkid><link>BLOCKS</link><type>F</type><location><![CDATA[I]]></location><text><![CDATA[!rasdc2_fieldsonblock.webclient]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>LANG6</paramid><type>OUT</type><orderby>6</orderby><blockid></blockid><fieldid>LANG</fieldid><namecid>LANG</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>PFORMID7</paramid><type>OUT</type><orderby>7</orderby><blockid></blockid><fieldid>PFORMID</fieldid><namecid>PFORMID</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$download</linkid><link>DOWNLOAD</link><type>F</type><location><![CDATA[N]]></location><text><![CDATA[RASDC2_FILES.downloadplsql]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>PFORMID2</paramid><type>OUT</type><orderby>2</orderby><blockid></blockid><fieldid>PFORMID</fieldid><namecid>PDATOTEKA</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$git</linkid><link>GIT</link><type>F</type><location><![CDATA[N]]></location><text><![CDATA[!rasdc2_git.webclient]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>10</paramid><type>OUT</type><orderby>10</orderby><blockid></blockid><fieldid></fieldid><namecid>ACTION</namecid><code></code><value><![CDATA[<%= RASDI_TRNSLT.text(''Search'', lang)%>]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>11</paramid><type>OUT</type><orderby>11</orderby><blockid></blockid><fieldid></fieldid><namecid></namecid><code></code><value><![CDATA[PBLOCKID=X]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>LANG8</paramid><type>OUT</type><orderby>8</orderby><blockid></blockid><fieldid>LANG</fieldid><namecid>LANG</namecid><co';
 v_vc(29) := 'de></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>PFORMID9</paramid><type>OUT</type><orderby>9</orderby><blockid></blockid><fieldid>PFORMID</fieldid><namecid>PFORMID</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$lfields</linkid><link>LFIELDS</link><type>F</type><location><![CDATA[I]]></location><text><![CDATA[!rasdc2_fieldsonblock.webclient]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>B20BLOCKID10</paramid><type>OUT</type><orderby>10</orderby><blockid>B20</blockid><fieldid>BLOCKID</fieldid><namecid>Pblockid</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>LANG11</paramid><type>OUT</type><orderby>11</orderby><blockid></blockid><fieldid>LANG</fieldid><namecid>LANG</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>PFORMID12</paramid><type>OUT</type><orderby>12</orderby><blockid></blockid><fieldid>PFORMID</fieldid><namecid>PFORMID</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$lsql</linkid><link>LSQL</link><type>F</type><location><![CDATA[N]]></location><text><![CDATA[!rasdc2_sql.webclient]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>18</paramid><type>OUT</type><orderby>18</orderby><blockid></blockid><fieldid></fieldid><namecid></namecid><code></code><value><![CDATA[ACTION=<%= RASDI_TRNSLT.text(''Search'', lang)%>]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>B20BLOCKID15</paramid><type>OUT</type><orderby>15</orderby><blockid>B20</blockid><fieldid>BLOCKID</fieldid><namecid>Pblockid</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>LANG16</paramid><type>OUT</type><orderby>16</orderby><blockid></blockid><fieldid>LANG</fieldid><namecid>LANG</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>PFORMID17</paramid><type>OUT</type><orderby>17</orderby><blockid></blockid><fieldid>PFORMID</fieldid><namecid>PFORMID</namecid><c';
 v_vc(30) := 'ode></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$ltriggers</linkid><link>LTRIGGERS</link><type>F</type><location><![CDATA[I]]></location><text><![CDATA[!rasdc2_triggers.webclient]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>B20BLOCKID7</paramid><type>OUT</type><orderby>7</orderby><blockid>B20</blockid><fieldid>BLOCKID</fieldid><namecid>Pblockid</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>LANG8</paramid><type>OUT</type><orderby>8</orderby><blockid></blockid><fieldid>LANG</fieldid><namecid>LANG</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>PFORMID9</paramid><type>OUT</type><orderby>9</orderby><blockid></blockid><fieldid>PFORMID</fieldid><namecid>PFORMID</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$reference</linkid><link>REFERENCE</link><type>F</type><location><![CDATA[I]]></location><text><![CDATA[!rasdc2_references.webclient]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>LANG2</paramid><type>OUT</type><orderby>2</orderby><blockid></blockid><fieldid>LANG</fieldid><namecid>LANG</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>PFORMID3</paramid><type>OUT</type><orderby>3</orderby><blockid></blockid><fieldid>PFORMID</fieldid><namecid>PFORMID</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$triggers</linkid><link>TRIGGERS</link><type>F</type><location><![CDATA[I]]></location><text><![CDATA[!rasdc2_triggers.webclient]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>LANG2</paramid><type>OUT</type><orderby>2</orderby><blockid></blockid><fieldid>LANG</fieldid><namecid>LANG</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>PFORMID3</paramid><type>OUT</type><orderby>3</orderby><blockid></blockid><fieldid>PFORMID</fieldid><namecid>PFORMID</namecid><code></co';
 v_vc(31) := 'de><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$xmldownload</linkid><link>XMLDOWNLOAD</link><type>F</type><location><![CDATA[N]]></location><text><![CDATA[#THIS_FORM#.metadata]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params></params></link><link><linkid>lov$pages</linkid><link>PAGES</link><type>S</type><location></location><text><![CDATA[                  select '''' id, '''' label, -1000 vr

                    from dual

                  union

                  select distinct to_char(page) id, decode(page,99,RASDI_TRNSLT.text(''SESSION'', lang),RASDI_TRNSLT.text(''Page'', lang)||'' ''||to_char(page)) label, page vr

                  from rasd_pages where formid = PFORMID

                   order by 3, 1]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params></params></link><link><linkid>lov$showmesslist</linkid><link>SHOWMESSLIST</link><type>T</type><location></location><text></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>TEXT0</paramid><type>TEXT</type><orderby>0</orderby><blockid></blockid><fieldid></fieldid><namecid></namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>TEXT1</paramid><type>TEXT</type><orderby>1</orderby><blockid></blockid><fieldid></fieldid><namecid></namecid><code>T</code><value><![CDATA[^]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>TEXT2</paramid><type>TEXT</type><orderby>2</orderby><blockid></blockid><fieldid></fieldid><namecid></namecid><code>B</code><value><![CDATA[ˇ]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>lov$sqlobjects</linkid><link>OBJECTS (TABLE, VIEW, ...)</link><type>S</type><location></location><text><![CDATA[                   select '''' id, '''' label, 1 x

                    from dual

                  union

                   select b.sqltable, sqltable || '' ... F'' , 3

                   from rasd_blocks b where b.formid = PFORMID

                   and b.sqltable is not null

                   union

                  select /*+ RULE*/ OBJECT_NAME id,

                          OBJECT_NAME || '' ... '' || substr(object_ty';
 v_vc(32) := 'pe, 1, 1) label,

                          2 x

                    from all_objects

                   where object_type in (''TABLE'', ''VIEW'')

                     and (owner = rasdc_library.currentDADUser)

                  union

                  select /*+ RULE*/ distinct SYNONYM_NAME id,

                                   SYNONYM_NAME || '' ... S'' label,

                                   2 x

                    from all_synonyms s, all_tab_columns tc

                   where s.table_name = tc.table_name

                     and s.table_owner = tc.owner

                     and (s.owner = rasdc_library.currentDADUser)

                   union

                   select distinct owner||''.''||table_name id,

                          owner||''.''||table_name  /*|| '' ... '' || substr(type, 1, 1) */ label, 2 x

                   from dba_tab_privs x

                   where --type in (''TABLE'', ''VIEW'') and

                    grantee = rasdc_library.currentDADUser

                   order by 3, 1           ]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params></params></link></links><pages><pagedata><page>0</page><blockid></blockid><fieldid>PF</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONBCK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONFWD</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid>B10</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid>B10</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid>B30</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid>P</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid>P</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>9';
 v_vc(33) := '9</page><blockid>P</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>COMPID</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>COMPID</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONSRC</fieldid><rlo';
 v_vc(34) := 'bid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONSRC</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>MESSAGE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>MESSAGE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>XRFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>XRFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata></pages><triggers><trigger><blockid></blockid><triggerid>FORM_CSS</triggerid><plsql><![CDATA[#B10LABEL_1_RASD {

   width: 300px;

}



#rasdTxB10VERSION_1 {

   text-align: center;

}





.rasdTxB20VRSTNIRED {

font-size: x-small;

}



.rasdTxB20LABEL INPUT {

width: 150px;

}



.rasdTxB20SQLTABLE {

width:';
 v_vc(35) := ' 150px;

}
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_JS</triggerid><plsql><![CDATA[$(function() {



//  addSpinner();

//   initRowStatus();

//   transformVerticalTable("B15_TABLE", 4 );

   setShowHideDiv("B30_DIV", true);

//   CheckFieldValue(pid , pname)

//   CheckFieldMandatory(pid , pname)





HighLightCell(''referenceBlock'', ''#aaccf7'');





$(".rasdTxB20BLOCKID INPUT").attr("maxlength", 30);

$(".rasdTxB20SQLTABLE INPUT").attr("maxlength", 30);

$(".rasdTxB20LABEL INPUT").attr("maxlength", 100);

$(".rasdTxB20NUMROWS INPUT").attr("maxlength", 5);

$(".rasdTxB20EMPTYROWS INPUT").attr("maxlength", 5);





ShowLOVIcon();





 });
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_JS_REF(82)</triggerid><plsql><![CDATA[$(function() {



  addSpinner();



});



$(function() {



  $(".rasdFormMenu").html("<%= RASDC_LIBRARY.showMeni(THIS_FORM, PFORMID, null, lang) %>");





  $(document).ready(function () {

   $(".dialog").dialog({ autoOpen: false });

  });





});
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>ON_ACTION</triggerid><plsql><![CDATA[  rasdc_library.log(this_form,pformid, ''START'', compid);



  psubmit(name_array ,value_array);

  rasd_client.secCheckPermission(this_form,ACTION);



  if ACTION is null then null;

    RECNUMP := 1;

    RECNUMB10 := 1;

    pselect;

    poutput;

  elsif ACTION = GBUTTONSRC then     RECNUMB10 := 1;

    RECNUMP := 1;

    pselect;

    poutput;

  elsif ACTION = GBUTTONSAVE then



           pcommit;

           rasdc_library.RefData(PFORMID);

           pselect;



        if xrform = ''Y'' then

		   message :=  message ||  RASDI_TRNSLT.text(''To unlink referenced code check:'', lang)||''<input type="checkbox" name="UNLINK" value="Y"/>.'';

		end if;





    message := message || RASDI_TRNSLT.text(''Changes are saved.'', lang) ;





    poutput;



  elsif ACTION = GBUTTONCLR then     pclear;

    poutput;



  els';
 v_vc(36) := 'if ACTION = GBUTTONCOMPILE then



        rasdc_library.log(this_form,pformid, ''COMMIT_S'', compid);

        pcommit;

        rasdc_library.log(this_form,pformid, ''COMMIT_E'', compid);

        commit;

        rasdc_library.log(this_form,pformid, ''REF_S'', compid);

        rasdc_library.RefData(PFORMID);

        rasdc_library.log(this_form,pformid, ''REF_E'', compid);



        compile(pformid , pform , lang ,  message, xrform  , compid);



        rasdc_library.log(this_form,pformid, ''SELECT_S'', compid);

        pselect;

        rasdc_library.log(this_form,pformid, ''SELECT_E'', compid);



      rasdc_library.log(this_form,pformid, ''POUTPUT_S'', compid);

    poutput;

      rasdc_library.log(this_form,pformid, ''POUTPUT_E'', compid);





  end if;

      rasdc_library.log(this_form,pformid, ''END'', compid);
]]></plsql><plsqlspec><![CDATA[  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission(''RASDC2_PAGES'',ACTION);
  if ACTION is null then null;
    RECNUMP := 1;
    RECNUMB10 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSRC then     RECNUMP := 1;
    RECNUMB10 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSAVE then     pcommit;
    pselect;
    --if MESSAGE is null then
    --MESSAGE := ''Form is changed.'';
    --end if;
    poutput;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutput;
  end if;

]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>GBUTTONPREV</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p( ''<input type=button class="rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

				';
 v_vc(37) := '					 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid>GBUTTONPREV</rblockid></trigger><trigger><blockid>HINTCONTENT</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid>HINTCONTENT</rblockid></trigger><trigger><blockid>B10</blockid><triggerid>ON_UPDATE</triggerid><plsql><![CDATA[declare

  v_rform varchar2(1);

begin

   if B10rf1 is not null

                   or  B10rf2 is not null

                   or  B10rf3 is not null

                   or  B10rf4 is not null

                   or  B10rf5 is not null

                   or  B10rf6 is not null

                   or  B10rf7 is not null

                   or  B10rf8 is not null

                   or  B10rf9 is not null

                   or  B10rf10 is not null

                   or  B10rf11 is not null

                   or  B10rf12 is not null

                   or  B10rf13 is not null

                   or  B10rf14 is not null

                   or  B10rf15 is not null

                   or  B10rf16 is not null

                   or  B10rf17 is not null

                   or  B10rf18 is not null

                   or  B10rf19 is not null

                   or  B10rf99 is not null

                 then

                   v_rform := ''Y'';

				 else

				   v_rform := ''N'';

                 end if;

--rlog(v_rform);

--rlog(unlink);

--rlog(B10BLOCKID);

--rlog(B10FIELDID);



if unlink = ''Y'' then v_rform := ''N''; end if;



if v_rform <> ''Y'' then

--rlog(''Sem noter'');

	    if (b10blockid is not null or b10fieldid is not null) and b10formid is not null then

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 0 ;

            exception

              when others then

';
 v_vc(38) := '                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 1 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 2 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 3 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 4 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 5 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 6 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 7 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete fr';
 v_vc(39) := 'om RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 8 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 9 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;



                        begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 10 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 11 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 12 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 13 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 14 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10form';
 v_vc(40) := 'id

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 15 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 16 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 17 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 18 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 19 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;

            begin

              delete from RASD_PAGES

               where formid = b10formid

                 and (blockid = b10blockid or fieldid = b10fieldid)

                 and page = 99 and (rform is null or unlink = ''Y'');

            exception

              when others then

                null;

            end;





            if b10page0 = ''Y''  then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 0, b10fieldid);

            end if;

            if b10page1 = ''Y'' and  (b10rf1 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 1, b10fieldid';
 v_vc(41) := ');

            end if;

            if b10page2 = ''Y'' and  (b10rf2 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 2, b10fieldid);

            end if;

            if b10page3 = ''Y'' and  (b10rf3 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 3, b10fieldid);

            end if;

            if b10page4 = ''Y'' and  (b10rf4 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 4, b10fieldid);

            end if;

            if b10page5 = ''Y'' and  (b10rf5 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 5, b10fieldid);

            end if;

            if b10page6 = ''Y'' and  (b10rf6 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 6, b10fieldid);

            end if;

            if b10page7 = ''Y'' and  (b10rf7 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 7, b10fieldid);

            end if;

            if b10page8 = ''Y'' and  (b10rf8 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 8, b10fieldid);

            end if;

            if b10page9 = ''Y'' and  (b10rf9 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 9, b10fieldid);

            end if;



            if b10page10 = ''Y'' and  (b10rf10 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 10, b10fieldid);

            end if;

';
 v_vc(42) := '            if b10page11 = ''Y'' and  (b10rf11 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 11, b10fieldid);

            end if;

            if b10page12 = ''Y'' and  (b10rf12 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 12, b10fieldid);

            end if;

            if b10page13 = ''Y'' and  (b10rf13 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 13, b10fieldid);

            end if;

            if b10page14 = ''Y'' and  (b10rf14 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 14, b10fieldid);

            end if;

            if b10page15 = ''Y'' and  (b10rf15 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 15, b10fieldid);

            end if;

            if b10page16 = ''Y'' and  (b10rf16 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 16, b10fieldid);

            end if;

            if b10page17 = ''Y'' and  (b10rf17 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 17, b10fieldid);

            end if;

            if b10page18 = ''Y'' and  (b10rf18 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 18, b10fieldid);

            end if;

            if b10page19 = ''Y'' and  (b10rf19 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 19, b10fieldid);

            end if;

   ';
 v_vc(43) := '              if b10page99 = ''Y'' and  (b10rf99 is null or unlink = ''Y'') then

              insert into RASD_PAGES

                (formid, blockid, page, fieldid)

              values

                (b10formid, b10blockid, 99, b10fieldid);

            end if;

          end if;



end if;



end;
]]></plsql><plsqlspec><![CDATA[-- Generated UPDATE statement. Use (i__) to access fields values.
if substr(B10RS(i__),1,1) = ''U'' then
update  set
where 1=2;
 MESSAGE := ''Data is changed.'';
end if;

]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>POST_ACTION</triggerid><plsql><![CDATA[--



/*

-- manjka še:



 - v SESSSTORAGEENABLED dodati SQL kodo če ne bo popup iste LISTLOV

 - v LISTLOV prenesti paramtrer FORMID







*/
]]></plsql><plsqlspec><![CDATA[  -- The execution after default execution based on  ACTION.
  -- Delete this code when you have new actions and add your own.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONSRC, GBUTTONSAVE, GBUTTONCLR ) then
    raise_application_error(''-20000'', ''ACTION="''||ACTION||''" is not defined. Define it in POST_ACTION trigger.'');
  end if;

]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B10</blockid><triggerid>POST_SELECT</triggerid><plsql><![CDATA[declare

ix number := 0;

begin



select count(*) into ix

from rasd_blocks rb where nvl(rb.numrows, 1) = 1

 and nvl(rb.emptyrows, 0) = 0

-- and nvl(rb.pagingyn,''N'') = ''N''

 and nvl(rb.dbblockyn,''N'') = ''N''

 and rb.formid = pformid

 and rb.blockid = b10blockid;



select count(*)+ix into ix

from rasd_fields rp

where nvl(rp.element,''INPUT_TEXT'') not in (''FONT_'' , ''A_'' , ''IMG_'' ,''PLSQL_'', ''INPUT_BUTTON'', ''INPUT_SUBMIT'', ''INPUT_RESET'')

  and rp.fieldid <> rasd_engine10.c_page

           and rp.fieldid <> rasd_engine11.c_message

           and rp.fieldid <> rasd_engine11.c_action

           and rp.fieldid <> rasd_engine11.c_restrestype

           and rp.fieldid <> rasd_engine11.c_restrequest

           and rp.fieldid <> rasd_engine11.c_fin

           and rp.fieldid <> rasd_engine11.c_warning

           and rp.fieldid <> rasd_engine11.c_error

           and rp.fieldid <> rasd_engine11.c_page

           and instr(rp.fieldid, rasd_engine11.c_recnum) ';
 v_vc(44) := '= 0

           and rp.elementyn = rasd_engine11.c_true

           and rp.formid = pformid

           and rp.blockid is null

           and rp.fieldid = b10fieldid;



if ix = 1 then

B10PAGE99#SET.visible := true;

else

B10PAGE99#SET.visible := false;

end if;



end;



if B10RF1 is not null then

  B10PAGE1#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF2 is not null then

  B10PAGE2#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF3 is not null then

  B10PAGE3#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF4 is not null then

  B10PAGE4#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF5 is not null then

  B10PAGE5#SET.custom := ''class="referenceBlock"'';

end if;





if B10RF6 is not null then

  B10PAGE6#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF7 is not null then

  B10PAGE7#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF8 is not null then

  B10PAGE8#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF9 is not null then

  B10PAGE9#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF10 is not null then

  B10PAGE10#SET.custom := ''class="referenceBlock"'';

end if;





if B10RF11 is not null then

  B10PAGE11#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF12 is not null then

  B10PAGE12#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF13 is not null then

  B10PAGE13#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF14 is not null then

  B10PAGE14#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF15 is not null then

  B10PAGE15#SET.custom := ''class="referenceBlock"'';

end if;





if B10RF16 is not null then

  B10PAGE16#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF17 is not null then

  B10PAGE17#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF18 is not null then

  B10PAGE18#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF19 is not null then

  B10PAGE19#SET.custom := ''class="referenceBlock"'';

end if;

if B10RF99 is not null then

  B10PAGE99#SET.custom := ''class="referenceBlock"'';

end if;





   if B10rf1 is not null

                   or  B10rf2 is not null

                   or  B10rf3 is not null

                   or  B10rf4 is not null

                   or  B10rf5 is not null

                   or  B10rf6 is not null

                   or  B10rf7 is not null

               ';
 v_vc(45) := '    or  B10rf8 is not null

                   or  B10rf9 is not null

                   or  B10rf10 is not null

                   or  B10rf11 is not null

                   or  B10rf12 is not null

                   or  B10rf13 is not null

                   or  B10rf14 is not null

                   or  B10rf15 is not null

                   or  B10rf16 is not null

                   or  B10rf17 is not null

                   or  B10rf18 is not null

                   or  B10rf19 is not null

                   or  B10rf99 is not null

                 then

                   xrform := ''Y'';

                 end if;



--rlog(xrform);
]]></plsql><plsqlspec><![CDATA[-- Triggers after executing SQL. Use (i__) to access fields values.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>POST_SUBMIT</triggerid><plsql><![CDATA[page := 1;

----put procedure in the begining of trigger;

post_submit_template;



--if action is null then action := RASDI_TRNSLT.text(GBUTTONSRC, lang); end if;
]]></plsql><plsqlspec><![CDATA[-- Executing after filling fields on submit.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>_ changes</triggerid><plsql><![CDATA[  function changes(p_log out varchar2) return varchar2 is

  begin



    p_log := ''/* Change LOG:

20230301 - Created new 2 version

20170105 - Modified filter on Pages page

20161207 - Added filter on Pages page

20161124 - Added possibility to check blocks and form fields for setting SESSION variables. Default setting is - not checked. Be careful with programs using SESSION variables in past.

20160627 - Included reference form future.

20160607 - Added 10 more pages to select.

20160406 - Problem when field type was null, it was not shown on pages. Solved.

20151202 - Included session variables in filters

20150814 - Added superuser

20141027 - Added footer on all pages

*/'';

    return version;

 end;
]]></plsql><plsqlspec><![CDATA[  function changes(p_log out varchar2) return varchar2;
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure custom template</trig';
 v_vc(46) := 'gerid><plsql><![CDATA[function showLabel(plabel varchar2, pcolor varchar2 default ''U'', pshowdialog number default 0)

return varchar2 is

   v__ varchar2(32000);

begin



v__ := RASDI_TRNSLT.text(plabel, lang);



if pshowdialog = 1 then

v__ := v__ || rasdc_hints.linkDialog(replace(replace(plabel,'' '',''''),''.'',''''),lang,replace(this_form,''2'','''')||''_DIALOG'');

end if;



if pcolor is null then



return v__;



else



return ''<font color="''||pcolor||''">''||v__||''</font>'';



end if;





end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure post_submit template</triggerid><plsql><![CDATA[procedure post_submit_template is

begin

RASDC_LIBRARY.checkprivileges(PFORMID);

begin

      select upper(form)

        into pform

        from RASD_FORMS

       where formid = PFORMID;

exception when others then null;

end;



if ACTION = GBUTTONSAVE or ACTION = GBUTTONCOMPILE then

  if rasdc_library.allowEditing(pformid) then

     null;

  else

     ACTION := GBUTTONSRC;

	 message := message || RASDI_TRNSLT.text(''User has no privileges to save data!'', lang);

  end if;

end if;





if rasdc_library.allowEditing(pformid) then

   GBUTTONSAVE#SET.visible := true;

   GBUTTONCOMPILE#SET.visible := true;

else

   GBUTTONSAVE#SET.visible := false;

   GBUTTONCOMPILE#SET.visible := false;

end if;





VUSER := rasdi_client.secGetUsername;

VLOB := rasdi_client.secGetLOB;

if lang is null then lang := rasdi_client.c_defaultLanguage; end if;



end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure_compile_template</triggerid><plsql><![CDATA[procedure compile(pformid number, pform varchar2, lang varchar2,  sporocilo in out varchar2, refform varchar2 default ''''  , pcompid in out number) is

      v_server RASD_ENGINES.server%type;

      cid      pls_integer;

      n        pls_integer;

      vup      varchar2(30) := rasdi_client.secGetUsername;

begin

        rasdc_library.log(this_form,pformid, ''COMPILE_S'', pcompid);



        begin

          if instr( upper(rasd_client.secGet_PATH_INFO), upper(pform)) > 0

			 then  ';
 v_vc(47) := '

            sporocilo := RASDI_TRNSLT.text(''From is not generated.'', lang);

          else



            select server

              into v_server

              from RASD_FORMS_COMPILED fg, RASD_ENGINES g

             where fg.engineid = g.engineid

               and fg.formid = PFORMID

               and fg.editor = vup

               and (fg.lobid = rasdi_client.secGetLOB or

                   fg.lobid is null and rasdi_client.secGetLOB is null);



            cid     := dbms_sql.open_cursor;



            dbms_sql.parse(cid,

                           ''begin '' || v_server || ''.c_debug := false;''|| v_server || ''.form('' || PFORMID ||

                           '','''''' || lang || '''''');end;'',

                           dbms_sql.native);



            n       := dbms_sql.execute(cid);



            dbms_sql.close_cursor(cid);



            sporocilo := RASDI_TRNSLT.text(''From is generated.'', lang) || rasdc_library.checknumberofsubfields(PFORMID);



        if refform is not null then

           sporocilo :=  sporocilo || ''<br/> - ''||  RASDI_TRNSLT.text(''To unlink referenced code check:'', lang)||''<input type="checkbox" name="UNLINK" value="Y"/>.'';

        end if;



          end if;

        exception

          when others then

            if sqlcode = -24344 then



            sporocilo := RASDI_TRNSLT.text(''Form is generated with compilation error. Check your code.'', lang)||''(''||sqlerrm||'')'';



            else

            sporocilo := RASDI_TRNSLT.text(''Form is NOT generated - internal RASD error.'', lang) || ''(''||sqlerrm||'')<br>''||

                         RASDI_TRNSLT.text(''To debug run: '', lang) || ''begin '' || v_server || ''.form('' || PFORMID ||

                         '','''''' || lang || '''''');end;'' ;

            end if;

        end;

        rasdc_library.log(this_form,pformid, ''COMPILE_E'', pcompid);

end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger></triggers><elements><element><elementid>1</elementid><pelementid>0</pelementid><orderby>1</orderby><element>HTML_</element><type></type><id>HTML</id><nameid></nameid><endtagelementid>0</endtagelementid><source></sourc';
 v_vc(48) := 'e><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>2</elementid><pelementid>1</pelementid><orderby>1</orderby><element>HEAD_</element><type></type><id>HEAD</id><nameid>HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''<%= rasdc_library.formName(PFORMID, LANG) %>''));
htp.p('''');
htp.p(''<script type="text/javascript">'');
formgen_js;
htp.p(''</script>'');
htpClob(FORM_UIHEAD);
htp.p(''<style type="text/css">'');
htpClob(FORM_CSS);
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>'');
%>]]></value';
 v_vc(49) := '><valuecode><![CDATA['');
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''''|| rasdc_library.formName(PFORMID, LANG) ||''''));
htp.p('''');
htp.p(''<script type="text/javascript">'');
formgen_js;
htp.p(''</script>'');
htpClob(FORM_UIHEAD);
htp.p(''<style type="text/css">'');
htpClob(FORM_CSS);
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>'');

htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>15</elementid><pelementid>1</pelementid><orderby>1</orderby><element>BODY_</element><type></type><id>BODY</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>16</elementid><pelementid>15</pelementid><orderby>3</orderby><element>FORM_</element><type>F</type><id>RASDC2_PAGES</id><nameid>RASDC2_PAGES</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_ACTION</attribute><type>A</type><text></text><name><![CDATA[action]]></name><value><![CDATA[?]]></value><valuecode><![CDATA[="?"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><a';
 v_vc(50) := 'ttribute><orderby>3</orderby><attribute>A_METHOD</attribute><type>A</type><text></text><name><![CDATA[method]]></name><value><![CDATA[post]]></value><valuecode><![CDATA[="post"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RASDC2_PAGES]]></value><valuecode><![CDATA[="RASDC2_PAGES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>17</elementid><pelementid>15</pelementid><orderby>1</orderby><element>FORM_LAB</element><type>F</type><id>RASDC2_PAGES_LAB</id><nameid>RASDC2_PAGES_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormLab]]></value><valuecode><![CDATA[="rasdFormLab"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><val';
 v_vc(51) := 'ue><![CDATA[RASDC2_PAGES_LAB]]></value><valuecode><![CDATA[="RASDC2_PAGES_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlHeaderDataTable(''RASDC2_PAGES_LAB'',''<%= rasdc_library.formName(PFORMID, LANG) %>'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlHeaderDataTable(''RASDC2_PAGES_LAB'',''''|| rasdc_library.formName(PFORMID, LANG) ||'''') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>18</elementid><pelementid>15</pelementid><orderby>2</orderby><element>FORM_MENU</element><type>F</type><id>RASDC2_PAGES_MENU</id><nameid>RASDC2_PAGES_MENU</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMenu]]></value><valuecode><![CDATA[="rasdFormMenu"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribut';
 v_vc(52) := 'e><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_PAGES_MENU]]></value><valuecode><![CDATA[="RASDC2_PAGES_MENU"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlMenuList(''RASDC2_PAGES_MENU'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlMenuList(''RASDC2_PAGES_MENU'') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>19</elementid><pelementid>15</pelementid><orderby>9999</orderby><element>FORM_BOTTOM</element><type>F</type><id>RASDC2_PAGES_BOTTOM</id><nameid>RASDC2_PAGES_BOTTOM</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBottom]]></value><valuecode><![CDATA[="rasdFormBottom"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><na';
 v_vc(53) := 'me><![CDATA[id]]></name><value><![CDATA[RASDC2_PAGES_BOTTOM]]></value><valuecode><![CDATA[="RASDC2_PAGES_BOTTOM"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlFooter(version , substr(''RASDC2_PAGES_BOTTOM'',1,instr(''RASDC2_PAGES_BOTTOM'', ''_'',-1)-1) , '''') %>]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlFooter(version , substr(''RASDC2_PAGES_BOTTOM'',1,instr(''RASDC2_PAGES_BOTTOM'', ''_'',-1)-1) , '''') ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>20</elementid><pelementid>16</pelementid><orderby>3</orderby><element>FORM_DIV</element><type>F</type><id>RASDC2_PAGES_DIV</id><nameid>RASDC2_PAGES_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdForm]]></value><valuecode><![CDATA[="rasdForm"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2<';
 v_vc(54) := '/orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_PAGES_DIV]]></value><valuecode><![CDATA[="RASDC2_PAGES_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>21</elementid><pelementid>20</pelementid><orderby>1</orderby><element>FORM_HEAD</element><type>F</type><id>RASDC2_PAGES_HEAD</id><nameid>RASDC2_PAGES_HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormHead]]></value><valuecode><![CDATA[="rasdFormHead"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_PAGES_HEAD]]></value><valuecode><![CDATA[="RASDC2_PAGES_HEAD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></va';
 v_vc(55) := 'lue><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>22</elementid><pelementid>20</pelementid><orderby>5</orderby><element>FORM_BODY</element><type>F</type><id>RASDC2_PAGES_BODY</id><nameid>RASDC2_PAGES_BODY</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBody]]></value><valuecode><![CDATA[="rasdFormBody"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_PAGES_BODY]]></value><valuecode><![CDATA[="RASDC2_PAGES_BODY"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></te';
 v_vc(56) := 'xtid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>23</elementid><pelementid>20</pelementid><orderby>9999</orderby><element>FORM_FOOTER</element><type>F</type><id>RASDC2_PAGES_FOOTER</id><nameid>RASDC2_PAGES_FOOTER</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormFooter]]></value><valuecode><![CDATA[="rasdFormFooter"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_PAGES_FOOTER]]></value><valuecode><![CDATA[="RASDC2_PAGES_FOOTER"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>24</elementid><pelementid>20</pelementid><orderby>2</orderby><element>FORM_RESPONSE</element><type>F</type><id>RASDC2_PAGES_RESPONSE</id><nameid>RASDC2_PAGES_RESPONSE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A';
 v_vc(57) := '_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormResponse]]></value><valuecode><![CDATA[="rasdFormResponse"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_PAGES_RESPONSE]]></value><valuecode><![CDATA[="RASDC2_PAGES_RESPONSE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>25</elementid><pelementid>24</pelementid><orderby>9998</orderby><element>FORM_MESSAGE</element><type>F</type><id>RASDC2_PAGES_MESSAGE</id><nameid>RASDC2_PAGES_MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage]]></value><valuecode><![CDATA[="rasdFormMessage"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDAT';
 v_vc(58) := 'A[RASDC2_PAGES_MESSAGE]]></value><valuecode><![CDATA[="RASDC2_PAGES_MESSAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>26</elementid><pelementid>24</pelementid><orderby>9996</orderby><element>FORM_ERROR</element><type>F</type><id>RASDC2_PAGES_ERROR</id><nameid>RASDC2_PAGES_ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage error]]></value><valuecode><![CDATA[="rasdFormMessage error"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_PAGES_ERROR]]></value><valuecode><![CDATA[="RASDC2_PAGES_ERROR"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></';
 v_vc(59) := 'endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>27</elementid><pelementid>24</pelementid><orderby>9997</orderby><element>FORM_WARNING</element><type>F</type><id>RASDC2_PAGES_WARNING</id><nameid>RASDC2_PAGES_WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage warning]]></value><valuecode><![CDATA[="rasdFormMessage warning"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_PAGES_WARNING]]></value><valuecode><![CDATA[="RASDC2_PAGES_WARNING"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlo';
 v_vc(60) := 'bid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>28</elementid><pelementid>22</pelementid><orderby>105</orderby><element>BLOCK_DIV</element><type>B</type><id>P_DIV</id><nameid>P_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_DIV]]></value><valuecode><![CDATA[="P_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockP_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockP_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>29</e';
 v_vc(61) := 'lementid><pelementid>28</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>30</elementid><pelementid>29</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>P_LAB</id><nameid>P_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_LAB]]></value><valuecode><![CDATA[="P_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><';
 v_vc(62) := '/textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>31</elementid><pelementid>28</pelementid><orderby>106</orderby><element>TABLE_</element><type></type><id>P_TABLE</id><nameid>P_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[0]]></value><valuecode><![CDATA[="0"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_TABLE_RASD]]></value><valuecode><![CDATA[="P_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><fo';
 v_vc(63) := 'rloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>32</elementid><pelementid>31</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>P_BLOCK</id><nameid>P_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_BLOCK_NAME]]></value><valuecode><![CDATA[="P_BLOCK_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>33</elementid><pelementid>31</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>P_THEAD</id><nameid>P_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><att';
 v_vc(64) := 'ribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>34</elementid><pelementid>33</pelementid><orderby>2</orderby><element>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>35</elementid><pelementid>34</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>PFILTER_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid';
 v_vc(65) := '></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPFILTER]]></value><valuecode><![CDATA[="rasdTxLabPFILTER"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>36</elementid><pelementid>35</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>PFILTER_LAB</id><nameid>PFILTER_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PFILTER_LAB]]></value><valuecode><![CDATA[="PFILTER_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute';
 v_vc(66) := '><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Filter'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(''Filter'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>37</elementid><pelementid>34</pelementid><orderby>2</orderby><element>TX_</element><type>E</type><id></id><nameid>PGBTNSRC_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPGBTNSRC]]></value><valuecode><![CDATA[="rasdTxLabPGBTNSRC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![C';
 v_vc(67) := 'DATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>38</elementid><pelementid>37</pelementid><orderby>2</orderby><element>FONT_</element><type>L</type><id>PGBTNSRC_LAB</id><nameid>PGBTNSRC_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGBTNSRC_LAB]]></value><valuecode><![CDATA[="PGBTNSRC_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rfor';
 v_vc(68) := 'm><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>39</elementid><pelementid>22</pelementid><orderby>115</orderby><element>BLOCK_DIV</element><type>B</type><id>B10_DIV</id><nameid>B10_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_DIV]]></value><valuecode><![CDATA[="B10_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB10_DIV  then %><div ]]></value><valuecode>';
 v_vc(69) := '<![CDATA['');  if  ShowBlockB10_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>40</elementid><pelementid>39</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>41</elementid><pelementid>40</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B10_LAB</id><nameid>B10_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribut';
 v_vc(70) := 'e><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_LAB]]></value><valuecode><![CDATA[="B10_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>42</elementid><pelementid>39</pelementid><orderby>116</orderby><element>TABLE_N</element><type></type><id>B10_TABLE</id><nameid>B10_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTableN display]]></value><valuecode><![CDATA[="rasdTableN display"]]></valuecode><forloop></forloop><endloo';
 v_vc(71) := 'p></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_TABLE_RASD]]></value><valuecode><![CDATA[="B10_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>43</elementid><pelementid>42</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B10_BLOCK</id><nameid>B10_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_BLOCK_NAME]]></value><valuecode><![CDATA[="B10_BLOCK_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop><![CDATA[''); end loop;
htp.prn('']]></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rfo';
 v_vc(72) := 'rm><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop><![CDATA[''); for iB10 in 1..B10FORMID.count loop
htp.prn('']]></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>44</elementid><pelementid>42</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>B10_THEAD</id><nameid>B10_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>45</elementid><pelementid>44</pelementid><orderby>2</orderby><element>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></for';
 v_vc(73) := 'loop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>46</elementid><pelementid>45</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>B10BLOK_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10BLOK]]></value><valuecode><![CDATA[="rasdTxLabB10BLOK"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>47</elementid><pelementid>46</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B10BLOK_LAB</id><nameid>B10BLOK_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attri';
 v_vc(74) := 'butes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10BLOK_LAB]]></value><valuecode><![CDATA[="B10BLOK_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Block or field on  Page'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(''Block or field on  Page'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>48</elementid><pelementid>45</pelementid><orderby>2</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE0_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]><';
 v_vc(75) := '/name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE0]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE0"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>49</elementid><pelementid>48</pelementid><orderby>2</orderby><element>FONT_</element><type>L</type><id>B10PAGE0_LAB</id><nameid>B10PAGE0_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE0_LAB]]></value><valuecode><![CDATA[="B10PAGE0_LAB"]]></valuecode><forloop></forloop><endloop>';
 v_vc(76) := '</endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[0]]></value><valuecode><![CDATA[0]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>50</elementid><pelementid>45</pelementid><orderby>3</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE1_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE1]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attrib';
 v_vc(77) := 'ute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>51</elementid><pelementid>50</pelementid><orderby>3</orderby><element>FONT_</element><type>L</type><id>B10PAGE1_LAB</id><nameid>B10PAGE1_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE1_LAB]]></value><valuecode><![CDATA[="B10PAGE1_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forl';
 v_vc(78) := 'oop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[1]]></value><valuecode><![CDATA[1]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>52</elementid><pelementid>45</pelementid><orderby>4</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE2_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE2]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE2"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform>';
 v_vc(79) := '<rid></rid></attribute></attributes></element><element><elementid>53</elementid><pelementid>52</pelementid><orderby>4</orderby><element>FONT_</element><type>L</type><id>B10PAGE2_LAB</id><nameid>B10PAGE2_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE2_LAB]]></value><valuecode><![CDATA[="B10PAGE2_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[2]]></value><valuecode><![CDATA[2]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>54</elementid><pelementid>45</pelementid><orderby>5</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE3_TX_LAB</nam';
 v_vc(80) := 'eid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE3]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE3"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>55</elementid><pelementid>54</pelementid><orderby>5</orderby><element>FONT_</element><type>L</type><id>B10PAGE3_LAB</id><nameid>B10PAGE3_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlob';
 v_vc(81) := 'id><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE3_LAB]]></value><valuecode><![CDATA[="B10PAGE3_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[3]]></value><valuecode><![CDATA[3]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>56</elementid><pelementid>45</pelementid><orderby>6</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE4_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDAT';
 v_vc(82) := 'A[rasdTxLabB10PAGE4]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE4"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>57</elementid><pelementid>56</pelementid><orderby>6</orderby><element>FONT_</element><type>L</type><id>B10PAGE4_LAB</id><nameid>B10PAGE4_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE4_LAB]]></value><valuecode><![CDATA[="B10PAGE4_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><text';
 v_vc(83) := 'id></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[4]]></value><valuecode><![CDATA[4]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>58</elementid><pelementid>45</pelementid><orderby>7</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE5_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE5]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE5"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><';
 v_vc(84) := 'text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>59</elementid><pelementid>58</pelementid><orderby>7</orderby><element>FONT_</element><type>L</type><id>B10PAGE5_LAB</id><nameid>B10PAGE5_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE5_LAB]]></value><valuecode><![CDATA[="B10PAGE5_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[5]]></value><valuecode><![CDATA[5]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid>';
 v_vc(85) := '<textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>60</elementid><pelementid>45</pelementid><orderby>8</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE6_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE6]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE6"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>61</elementid><pelementid>60</pelementid><orderby>8</orderby><element>FONT_</element><type>L</type><id>B10PAGE6_LAB</id><nameid>B10PAGE6_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</t';
 v_vc(86) := 'ype><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE6_LAB]]></value><valuecode><![CDATA[="B10PAGE6_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[6]]></value><valuecode><![CDATA[6]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>62</elementid><pelementid>45</pelementid><orderby>9</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE7_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source';
 v_vc(87) := '></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE7]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE7"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>63</elementid><pelementid>62</pelementid><orderby>9</orderby><element>FONT_</element><type>L</type><id>B10PAGE7_LAB</id><nameid>B10PAGE7_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE7_LAB]]></value><valuecode><![CDATA[="B10PAGE7_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribut';
 v_vc(88) := 'e><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[7]]></value><valuecode><![CDATA[7]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>64</elementid><pelementid>45</pelementid><orderby>10</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE8_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE8]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE8"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]><';
 v_vc(89) := '/valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>65</elementid><pelementid>64</pelementid><orderby>10</orderby><element>FONT_</element><type>L</type><id>B10PAGE8_LAB</id><nameid>B10PAGE8_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE8_LAB]]></value><valuecode><![CDATA[="B10PAGE8_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></';
 v_vc(90) := 'rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[8]]></value><valuecode><![CDATA[8]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>66</elementid><pelementid>45</pelementid><orderby>11</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE9_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE9]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE9"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>67</elementid><pelementid>66</pelementid><orderby>11</orderby><element>FONT_</element><type>L</type><';
 v_vc(91) := 'id>B10PAGE9_LAB</id><nameid>B10PAGE9_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE9_LAB]]></value><valuecode><![CDATA[="B10PAGE9_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[9]]></value><valuecode><![CDATA[9]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>68</elementid><pelementid>45</pelementid><orderby>12</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE10_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attri';
 v_vc(92) := 'bute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE10]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>69</elementid><pelementid>68</pelementid><orderby>12</orderby><element>FONT_</element><type>L</type><id>B10PAGE10_LAB</id><nameid>B10PAGE10_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value';
 v_vc(93) := '><![CDATA[B10PAGE10_LAB]]></value><valuecode><![CDATA[="B10PAGE10_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[10]]></value><valuecode><![CDATA[10]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>70</elementid><pelementid>45</pelementid><orderby>13</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE11_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE11]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE11"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N';
 v_vc(94) := '</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>71</elementid><pelementid>70</pelementid><orderby>13</orderby><element>FONT_</element><type>L</type><id>B10PAGE11_LAB</id><nameid>B10PAGE11_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE11_LAB]]></value><valuecode><![CDATA[="B10PAGE11_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><t';
 v_vc(95) := 'ype>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[11]]></value><valuecode><![CDATA[11]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>72</elementid><pelementid>45</pelementid><orderby>14</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE12_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE12]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE12"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloo';
 v_vc(96) := 'p><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>73</elementid><pelementid>72</pelementid><orderby>14</orderby><element>FONT_</element><type>L</type><id>B10PAGE12_LAB</id><nameid>B10PAGE12_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE12_LAB]]></value><valuecode><![CDATA[="B10PAGE12_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[12]]></value><valuecode><![CDATA[12]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><element';
 v_vc(97) := 'id>74</elementid><pelementid>45</pelementid><orderby>15</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE13_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE13]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE13"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>75</elementid><pelementid>74</pelementid><orderby>15</orderby><element>FONT_</element><type>L</type><id>B10PAGE13_LAB</id><nameid>B10PAGE13_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><for';
 v_vc(98) := 'loop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE13_LAB]]></value><valuecode><![CDATA[="B10PAGE13_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[13]]></value><valuecode><![CDATA[13]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>76</elementid><pelementid>45</pelementid><orderby>16</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE14_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform';
 v_vc(99) := '><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE14]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE14"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>77</elementid><pelementid>76</pelementid><orderby>16</orderby><element>FONT_</element><type>L</type><id>B10PAGE14_LAB</id><nameid>B10PAGE14_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE14_LAB]]></value><valuecode><![CDATA[="B10PAGE14_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><';
 v_vc(100) := '![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[14]]></value><valuecode><![CDATA[14]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>78</elementid><pelementid>45</pelementid><orderby>17</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE15_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE15]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE15"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><';
 v_vc(101) := 'textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>79</elementid><pelementid>78</pelementid><orderby>17</orderby><element>FONT_</element><type>L</type><id>B10PAGE15_LAB</id><nameid>B10PAGE15_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE15_LAB]]></value><valuecode><![CDATA[="B10PAGE15_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name';
 v_vc(102) := '></name><value><![CDATA[15]]></value><valuecode><![CDATA[15]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>80</elementid><pelementid>45</pelementid><orderby>18</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE16_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE16]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE16"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>81</elementid><pelementid>80</pelementid><orderby>18</orderby><element>FONT_</element><type>L</type><id>B10PAGE16_LAB</id><nameid>B10PAGE16_LAB</nameid><endtagelementid>0</endtagelementid><source></so';
 v_vc(103) := 'urce><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE16_LAB]]></value><valuecode><![CDATA[="B10PAGE16_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[16]]></value><valuecode><![CDATA[16]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>82</elementid><pelementid>45</pelementid><orderby>19</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE17_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CD';
 v_vc(104) := 'ATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE17]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE17"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>83</elementid><pelementid>82</pelementid><orderby>19</orderby><element>FONT_</element><type>L</type><id>B10PAGE17_LAB</id><nameid>B10PAGE17_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE17_LAB]]></value><valuecode><![CDATA[="B10PAGE17_LAB"]]></valuecode><forloop';
 v_vc(105) := '></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[17]]></value><valuecode><![CDATA[17]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>84</elementid><pelementid>45</pelementid><orderby>20</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE18_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE18]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE18"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform><';
 v_vc(106) := '/rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>85</elementid><pelementid>84</pelementid><orderby>20</orderby><element>FONT_</element><type>L</type><id>B10PAGE18_LAB</id><nameid>B10PAGE18_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE18_LAB]]></value><valuecode><![CDATA[="B10PAGE18_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode';
 v_vc(107) := '><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[18]]></value><valuecode><![CDATA[18]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>86</elementid><pelementid>45</pelementid><orderby>21</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE19_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE19]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE19"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></text';
 v_vc(108) := 'code><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>87</elementid><pelementid>86</pelementid><orderby>21</orderby><element>FONT_</element><type>L</type><id>B10PAGE19_LAB</id><nameid>B10PAGE19_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE19_LAB]]></value><valuecode><![CDATA[="B10PAGE19_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[19]]></value><valuecode><![CDATA[19]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>88</elementid><pelementid>45</pelementid><orderby>22</orderby><element>TX_</element><type>';
 v_vc(109) := 'E</type><id></id><nameid>B10PAGE99_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PAGE99]]></value><valuecode><![CDATA[="rasdTxLabB10PAGE99"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>89</elementid><pelementid>88</pelementid><orderby>22</orderby><element>FONT_</element><type>L</type><id>B10PAGE99_LAB</id><nameid>B10PAGE99_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><';
 v_vc(110) := 'textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE99_LAB]]></value><valuecode><![CDATA[="B10PAGE99_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Session'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(''Session'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>90</elementid><pelementid>22</pelementid><orderby>125</orderby><element>BLOCK_DIV</element><type>B</type><id>B30_DIV</id><nameid>B30_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribu';
 v_vc(111) := 'te>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30_DIV]]></value><valuecode><![CDATA[="B30_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB30_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB30_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>91</elementid><pelementid>90</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><value';
 v_vc(112) := 'code><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>92</elementid><pelementid>91</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B30_LAB</id><nameid>B30_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30_LAB]]></value><valuecode><![CDATA[="B30_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attri';
 v_vc(113) := 'bute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= RASDI_TRNSLT.text(''Errors'',LANG)%>]]></value><valuecode><![CDATA[''|| RASDI_TRNSLT.text(''Errors'',LANG)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>93</elementid><pelementid>90</pelementid><orderby>126</orderby><element>TABLE_N</element><type></type><id>B30_TABLE</id><nameid>B30_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTableN display]]></value><valuecode><![CDATA[="rasdTableN display"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30_TABLE_RASD]]></value><valuecode><![CDATA[="B30_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[';
 v_vc(114) := '<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>94</elementid><pelementid>93</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B30_BLOCK</id><nameid>B30_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30_BLOCK_NAME]]></value><valuecode><![CDATA[="B30_BLOCK_''||iB30||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop><![CDATA[''); end loop;
htp.prn('']]></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop><![CDATA[''); for iB30 in 1..B30TEXT.count loop
htp.prn('']]></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>95</elementid><pelementid>93</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>B30_THEAD</id><nameid>B30_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></s';
 v_vc(115) := 'ource><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>96</elementid><pelementid>95</pelementid><orderby>2</orderby><element>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>97</elementid><pelementid>96</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>B30TEXT_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB30]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB30"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text>';
 v_vc(116) := '<name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB30TEXT]]></value><valuecode><![CDATA[="rasdTxLabB30TEXT"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>98</elementid><pelementid>97</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B30TEXT_LAB</id><nameid>B30TEXT_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30TEXT_LAB]]></value><valuecode><![CDATA[="B30TEXT_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N';
 v_vc(117) := '</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>99</elementid><pelementid>21</pelementid><orderby>0</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMP</id><nameid>RECNUMP</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMP_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMP_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><';
 v_vc(118) := '![CDATA[RECNUMP_NAME]]></value><valuecode><![CDATA[="RECNUMP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMP_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(RECNUMP))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textco';
 v_vc(119) := 'de><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>100</elementid><pelementid>21</pelementid><orderby>0</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMB10</id><nameid>RECNUMB10</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMB10_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMB10_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMB10_NAME]]></value><valuecode><![CDATA[="RECNUMB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name';
 v_vc(120) := '><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMB10_VALUE]]></value><valuecode><![CDATA[="''||RECNUMB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>101</elementid><pelementid>21</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>ACTION</id><nameid>ACTION</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform><';
 v_vc(121) := '/rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ACTION_NAME_RASD]]></value><valuecode><![CDATA[="ACTION_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[ACTION_NAME]]></value><valuecode><![CDATA[="ACTION"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[ACTION_VALUE]]></value><val';
 v_vc(122) := 'uecode><![CDATA[="''||ACTION||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>102</elementid><pelementid>21</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>PAGE</id><nameid>PAGE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PAGE_NAME_RASD]]></value><valuecode><![CDATA[="PAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby';
 v_vc(123) := '>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PAGE_NAME]]></value><valuecode><![CDATA[="PAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PAGE_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(PAGE))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop>';
 v_vc(124) := '<source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>103</elementid><pelementid>21</pelementid><orderby>6</orderby><element>INPUT_HIDDEN</element><type>D</type><id>LANG</id><nameid>LANG</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[LANG_NAME_RASD]]></value><valuecode><![CDATA[="LANG_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[LANG_NAME]]></value><valuecode><![CDATA[="LANG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribut';
 v_vc(125) := 'e><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[LANG_VALUE]]></value><valuecode><![CDATA[="''||LANG||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>104</elementid><pelementid>21</pelementid><orderby>7</orderby><element>INPUT_HIDDEN</element><type>D</type><id>PFORMID</id><nameid>PFORMID</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid>';
 v_vc(126) := '</textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PFORMID_NAME_RASD]]></value><valuecode><![CDATA[="PFORMID_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PFORMID_NAME]]></value><valuecode><![CDATA[="PFORMID"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA';
 v_vc(127) := '[value]]></name><value><![CDATA[PFORMID_VALUE]]></value><valuecode><![CDATA[="''||PFORMID||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>105</elementid><pelementid>21</pelementid><orderby>13</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONSAVE</id><nameid>GBUTTONSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom';
 v_vc(128) := '),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONSAVE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONSAVE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSAVE_NAME]]></value><valuecode><![CDATA[="GBUTTONSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![';
 v_vc(129) := 'CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSAVE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONSAVE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></en';
 v_vc(130) := 'dloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONSAVE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONSAVE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONSAVE  then
htp.prn('''');  if GBUTTONSAVE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONSAVE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></for';
 v_vc(131) := 'loop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>106</elementid><pelementid>23</pelementid><orderby>10011</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONSAVE</id><nameid>GBUTTONSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONSAVE_NAME_RASD]]></value><valuecode><![C';
 v_vc(132) := 'DATA[="GBUTTONSAVE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSAVE_NAME]]></value><valuecode><![CDATA[="GBUTTONSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSAVE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONSAVE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn(''';
 v_vc(133) := ']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONSAVE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONSAVE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if';
 v_vc(134) := '; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONSAVE  then
htp.prn('''');  if GBUTTONSAVE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONSAVE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>107</elementid><pelementid>21</pelementid><orderby>14</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONCOMPILE</id><nameid>GBUTTONCOMPILE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONCOMPILE#SET.info is not null then htp.p(';
 v_vc(135) := ''' infoField''); end if; %><% if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONCOMPILE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONCOMPILE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONCOMPILE_NAME]]></value><valuecode><![CDATA[="GBUTTONCOMPILE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><';
 v_vc(136) := 'value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONCOMPILE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONCOMPILE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn>';
 v_vc(137) := '<valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONCOMPILE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONCOMPILE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end i';
 v_vc(138) := 'f;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('''');  if GBUTTONCOMPILE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>108</elementid><pelementid>23</pelementid><orderby>10012</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONCOMPILE</id><nameid>GBUTTONCOMPILE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET';
 v_vc(139) := '.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONCOMPILE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONCOMPILE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONCOMPILE_NAME]]></value><valuecode><![CDATA[="GBUTTONCOMPILE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><att';
 v_vc(140) := 'ribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONCOMPILE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONCOMPILE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.required then htp.p('' required="required"';
 v_vc(141) := '''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONCOMPILE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONCOMPILE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONCOMPILE  then
';
 v_vc(142) := 'htp.prn('''');  if GBUTTONCOMPILE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>109</elementid><pelementid>21</pelementid><orderby>15</orderby><element>INPUT_RESET</element><type>D</type><id>GBUTTONRES</id><nameid>GBUTTONRES</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rfor';
 v_vc(143) := 'm><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONRES_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONRES_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONRES_NAME]]></value><valuecode><![CDATA[="GBUTTONRES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[reset]]></value><valuecode><![CDATA[="reset"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONRES_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONRES||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name>';
 v_vc(144) := '</name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONRES#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONRES#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTT';
 v_vc(145) := 'ONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONRES  then
htp.prn('''');  if GBUTTONRES#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONRES#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>110</elementid><pelementid>23</pelementid><orderby>10013</orderby><element>INPUT_RESET</element><type>D</type><id>GBUTTONRES</id><nameid>GBUTTONRES</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONRES#SET.error is not null then htp.p('' errorField''';
 v_vc(146) := '); end if; %><% if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONRES_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONRES_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONRES_NAME]]></value><valuecode><![CDATA[="GBUTTONRES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></va';
 v_vc(147) := 'lue><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[reset]]></value><valuecode><![CDATA[="reset"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONRES_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONRES||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.disabled';
 v_vc(148) := ' then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONRES#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONRES#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><';
 v_vc(149) := 'rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONRES  then
htp.prn('''');  if GBUTTONRES#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONRES#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>111</elementid><pelementid>21</pelementid><orderby>16</orderby><element>PLSQL_</element><type>D</type><id>GBUTTONPREV</id><nameid>GBUTTONPREV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONPREV_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONPREV_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid><';
 v_vc(150) := '/valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONPREV#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONPREV#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV';
 v_vc(151) := '#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span>'');  end if;
htp.prn('']]></name><value><![CDATA[</span><% end if; %>]]></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONPREV  then
htp.prn('''');  if GBUTTONPREV#SET.visible then
htp.prn(''<span]]></name><value><![CDATA[<% if GBUTTONPREV#SET.visible then %><span]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% GBUTTONPREV_VALUE %>]]></value><valuecode><![CDATA['');  htp.p( ''<input type=button class="rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn>';
 v_vc(152) := '<valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>112</elementid><pelementid>23</pelementid><orderby>10014</orderby><element>PLSQL_</element><type>D</type><id>GBUTTONPREV</id><nameid>GBUTTONPREV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONPREV_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONPREV_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.required then htp.p';
 v_vc(153) := '('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONPREV#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONPREV#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span>'');  end if;
htp.prn('']]></name><value><![CDATA[</span><% end if; %>]]></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text>';
 v_vc(154) := '</text><name><![CDATA['');
if  ShowFieldGBUTTONPREV  then
htp.prn('''');  if GBUTTONPREV#SET.visible then
htp.prn(''<span]]></name><value><![CDATA[<% if GBUTTONPREV#SET.visible then %><span]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% GBUTTONPREV_VALUE %>]]></value><valuecode><![CDATA['');  htp.p( ''<input type=button class="rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>113</elementid><pelementid>26</pelementid><orderby>10046</orderby><element>FONT_</element><type>D</type><id>ERROR</id><nameid>ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode>';
 v_vc(155) := '</textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ERROR_NAME_RASD]]></value><valuecode><![CDATA[="ERROR_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source';
 v_vc(156) := '><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[ERROR_VALUE]]></value><valuecode><![CDATA[''||ERROR||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>114</elementid><pelementid>25</pelementid><orderby>10049</orderby><element>FONT_</element><type>D</type><id>MESSAGE</id><nameid>MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><nam';
 v_vc(157) := 'e><![CDATA[id]]></name><value><![CDATA[MESSAGE_NAME_RASD]]></value><valuecode><![CDATA[="MESSAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></a';
 v_vc(158) := 'ttribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[MESSAGE_VALUE]]></value><valuecode><![CDATA[''||MESSAGE||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>115</elementid><pelementid>27</pelementid><orderby>10049</orderby><element>FONT_</element><type>D</type><id>WARNING</id><nameid>WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[WARNING_NAME_RASD]]></value><valuecode><![CDATA[="WARNING_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></';
 v_vc(159) := 'endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[WA';
 v_vc(160) := 'RNING_VALUE]]></value><valuecode><![CDATA[''||WARNING||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>116</elementid><pelementid>21</pelementid><orderby>290876</orderby><element>PLSQL_</element><type>D</type><id>HINTCONTENT</id><nameid>HINTCONTENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[HINTCONTENT_NAME_RASD]]></value><valuecode><![CDATA[="HINTCONTENT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldHINTCONTENT  then
htp.prn(''<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% HINTCONTENT_VALUE %>]]></value><valuecode><![CDATA['');  htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>117</elementid><peleme';
 v_vc(161) := 'ntid>23</pelementid><orderby>300874</orderby><element>PLSQL_</element><type>D</type><id>HINTCONTENT</id><nameid>HINTCONTENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[HINTCONTENT_NAME_RASD]]></value><valuecode><![CDATA[="HINTCONTENT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldHINTCONTENT  then
htp.prn(''<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% HINTCONTENT_VALUE %>]]></value><valuecode><![CDATA['');  htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>118</elementid><pelementid>32</pelementid><orderby>1</orderby><element>HIDDFIELD_</element><type></type><id></id><nameid>P</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_CLASS</attribute>';
 v_vc(162) := '<type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[hiddenRowItems]]></value><valuecode><![CDATA[="hiddenRowItems"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[P_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>119</elementid><pelementid>118</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>SESSSTORAGEENABLED</id><nameid>SESSSTORAGEENABLED</na';
 v_vc(163) := 'meid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[SESSSTORAGEENABLED_NAME_RASD]]></value><valuecode><![CDATA[="SESSSTORAGEENABLED_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[SESSSTORAGEENABLED_NAME]]></value><valuecode><![CDATA[="SESSSTORAGEENABLED_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><r';
 v_vc(164) := 'id></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[SESSSTORAGEENABLED_VALUE]]></value><valuecode><![CDATA[="''||PSESSSTORAGEENABLED(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>120</elementid><pelementid>32</pelementid><orderby>200</orderby><element>TX_</element><type>E</type><id></id><nameid>PFILTER_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPFILTER rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPFILTER rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDA';
 v_vc(165) := 'TA[rasdTxPFILTER_NAME]]></value><valuecode><![CDATA[="rasdTxPFILTER_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>121</elementid><pelementid>120</pelementid><orderby>1</orderby><element>INPUT_TEXT</element><type>D</type><id>PFILTER</id><nameid>PFILTER</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTextC ]]></value><valuecode><![CDATA[="rasdTextC "]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PFILTER_NAME_RASD]]></value><valuecode><![CDATA[="PFILTER_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid';
 v_vc(166) := '><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PFILTER_NAME]]></value><valuecode><![CDATA[="PFILTER_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ondblclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>A_SIZE</attribute><type>A</type><text></text><name><![CDATA[size]]></name><value><![CDATA[10]]></value><valuecode><![CDATA[="10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[text]]></value><valuecode><![CDATA[="text"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PFILTER_VALUE]]></value><valuecode><![CDATA[="''||PFILTER(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><te';
 v_vc(167) := 'xt></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>122</elementid><pelementid>32</pelementid><orderby>240</orderby><element>TX_</element><type>E</type><id></id><nameid>PGBTNSRC_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPGBTNSRC rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPGBTNSRC rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPGBTNSRC_NAME]]></value><valuecode><![CDATA[="rasdTxPGBTNSRC_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valu';
 v_vc(168) := 'eid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>123</elementid><pelementid>122</pelementid><orderby>1</orderby><element>INPUT_SUBMIT</element><type>D</type><id>PGBTNSRC</id><nameid>PGBTNSRC</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGBTNSRC_NAME_RASD]]></value><valuecode><![CDATA[="PGBTNSRC_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PGBTNSRC_NAME]]></value><valuecode><![CDATA[="PGBTNSRC_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rfo';
 v_vc(169) := 'rm><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PGBTNSRC_VALUE]]></value><valuecode><![CDATA[="''||PGBTNSRC(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>124</elementid><pelementid>43</pelementid><orderby>1</orderby><element>HIDDFIELD_</element><type></type><id></id><nameid>B10</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[hiddenRowItems]]></value><valuecode><![CDATA[="hiddenRowItems"]]></valuecode>';
 v_vc(170) := '<forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[<%=iB10%>]]></value><valuecode><![CDATA[="''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>125</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RS</id><nameid>B10RS</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attribut';
 v_vc(171) := 'es><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RS_NAME_RASD]]></value><valuecode><![CDATA[="B10RS_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RS_NAME]]></value><valuecode><![CDATA[="B10RS_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA';
 v_vc(172) := '[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RS_VALUE]]></value><valuecode><![CDATA[="''||B10RS(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>126</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10FORMID</id><nameid>B10FORMID</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderb';
 v_vc(173) := 'y>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10FORMID_NAME_RASD]]></value><valuecode><![CDATA[="B10FORMID_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10FORMID_NAME]]></value><valuecode><![CDATA[="B10FORMID_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10FORMID_VALUE]]></value><valuecode><![CDATA[="''||B10FORMID(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></valu';
 v_vc(174) := 'e><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>127</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10BLOCKID</id><nameid>B10BLOCKID</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10BLOCKID_NAME_RASD]]></value><valuecode><![CDATA[="B10BLOCKID_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10BLOCKID_NAME]]></value><valuecode><![CDATA[="B10BLOCKID_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></texti';
 v_vc(175) := 'd><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10BLOCKID_VALUE]]></value><valuecode><![CDATA[="''||B10BLOCKID(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>128</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</elem';
 v_vc(176) := 'ent><type>D</type><id>B10FIELDID</id><nameid>B10FIELDID</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10FIELDID_NAME_RASD]]></value><valuecode><![CDATA[="B10FIELDID_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10FIELDID_NAME]]></value><valuecode><![CDATA[="B10FIELDID_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode>';
 v_vc(177) := '</textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10FIELDID_VALUE]]></value><valuecode><![CDATA[="''||B10FIELDID(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>129</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF1</id><nameid>B10RF1</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value>';
 v_vc(178) := '</value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF1_NAME_RASD]]></value><valuecode><![CDATA[="B10RF1_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF1_NAME]]></value><valuecode><![CDATA[="B10RF1_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF1_VALUE]]></value><valuecode><![CDATA[="''||B10RF1(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><v';
 v_vc(179) := 'alueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>130</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF2</id><nameid>B10RF2</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF2_NAME_RASD]]></value><valuecode><![CDATA[="B10RF2_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><';
 v_vc(180) := '![CDATA[B10RF2_NAME]]></value><valuecode><![CDATA[="B10RF2_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF2_VALUE]]></value><valuecode><![CDATA[="''||B10RF2(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode>';
 v_vc(181) := '<rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>131</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF3</id><nameid>B10RF3</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF3_NAME_RASD]]></value><valuecode><![CDATA[="B10RF3_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF3_NAME]]></value><valuecode><![CDATA[="B10RF3_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></na';
 v_vc(182) := 'me><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF3_VALUE]]></value><valuecode><![CDATA[="''||B10RF3(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>132</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF4</id><nameid>B10RF4</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rfor';
 v_vc(183) := 'm></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF4_NAME_RASD]]></value><valuecode><![CDATA[="B10RF4_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF4_NAME]]></value><valuecode><![CDATA[="B10RF4_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B1';
 v_vc(184) := '0RF4_VALUE]]></value><valuecode><![CDATA[="''||B10RF4(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>133</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF5</id><nameid>B10RF5</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF5_NAME_RASD]]></value><valuecode><![CDATA[="B10RF5_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform';
 v_vc(185) := '></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF5_NAME]]></value><valuecode><![CDATA[="B10RF5_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF5_VALUE]]></value><valuecode><![CDATA[="''||B10RF5(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![C';
 v_vc(186) := 'DATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>134</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF6</id><nameid>B10RF6</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF6_NAME_RASD]]></value><valuecode><![CDATA[="B10RF6_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF6_NAME]]></value><valuecode><![CDATA[="B10RF6_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform>';
 v_vc(187) := '</rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF6_VALUE]]></value><valuecode><![CDATA[="''||B10RF6(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>135</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF7</id><nameid>B10RF7</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode>';
 v_vc(188) := '<forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF7_NAME_RASD]]></value><valuecode><![CDATA[="B10RF7_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF7_NAME]]></value><valuecode><![CDATA[="B10RF7_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><ri';
 v_vc(189) := 'd></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF7_VALUE]]></value><valuecode><![CDATA[="''||B10RF7(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>136</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF8</id><nameid>B10RF8</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF8_NAME_RASD]]></value><valuecode><![CDATA[="B10RF8_''||iB10||''_RASD"]]></valuecode><';
 v_vc(190) := 'forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF8_NAME]]></value><valuecode><![CDATA[="B10RF8_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF8_VALUE]]></value><valuecode><![CDATA[="''||B10RF8(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><a';
 v_vc(191) := 'ttribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>137</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF9</id><nameid>B10RF9</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF9_NAME_RASD]]></value><valuecode><![CDATA[="B10RF9_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF9_NAME]]></value><valuecode><![CDATA[="B10RF9_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><f';
 v_vc(192) := 'orloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF9_VALUE]]></value><valuecode><![CDATA[="''||B10RF9(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>138</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF10</id><nameid>B10RF10</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attri';
 v_vc(193) := 'bute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF10_NAME_RASD]]></value><valuecode><![CDATA[="B10RF10_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF10_NAME]]></value><valuecode><![CDATA[="B10RF10_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloo';
 v_vc(194) := 'p></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF10_VALUE]]></value><valuecode><![CDATA[="''||B10RF10(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>139</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF11</id><nameid>B10RF11</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</at';
 v_vc(195) := 'tribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF11_NAME_RASD]]></value><valuecode><![CDATA[="B10RF11_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF11_NAME]]></value><valuecode><![CDATA[="B10RF11_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF11_VALUE]]></value><valuecode><![CDATA[="''||B10RF11(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop>';
 v_vc(196) := '<endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>140</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF12</id><nameid>B10RF12</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF12_NAME_RASD]]></value><valuecode><![CDATA[="B10RF12_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF12_NAME]]></value><valuecode><![CDATA[="B10RF12_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></';
 v_vc(197) := 'rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF12_VALUE]]></value><valuecode><![CDATA[="''||B10RF12(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>141</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF13</id><nameid>B10RF13</nameid><endtagele';
 v_vc(198) := 'mentid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF13_NAME_RASD]]></value><valuecode><![CDATA[="B10RF13_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF13_NAME]]></value><valuecode><![CDATA[="B10RF13_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>';
 v_vc(199) := '7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF13_VALUE]]></value><valuecode><![CDATA[="''||B10RF13(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>142</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF14</id><nameid>B10RF14</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hid';
 v_vc(200) := 'denyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF14_NAME_RASD]]></value><valuecode><![CDATA[="B10RF14_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF14_NAME]]></value><valuecode><![CDATA[="B10RF14_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF14_VALUE]]></value><valuecode><![CDATA[="''||B10RF14(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rfo';
 v_vc(201) := 'rm><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>143</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF15</id><nameid>B10RF15</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF15_NAME_RASD]]></value><valuecode><![CDATA[="B10RF15_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF15_NAME]]></value><valuecode><![CDATA[="B10RF15_''||iB10||''"]]></val';
 v_vc(202) := 'uecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF15_VALUE]]></value><valuecode><![CDATA[="''||B10RF15(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></elemen';
 v_vc(203) := 't><element><elementid>144</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF16</id><nameid>B10RF16</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF16_NAME_RASD]]></value><valuecode><![CDATA[="B10RF16_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF16_NAME]]></value><valuecode><![CDATA[="B10RF16_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop><';
 v_vc(204) := '/endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF16_VALUE]]></value><valuecode><![CDATA[="''||B10RF16(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>145</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF17</id><nameid>B10RF17</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><a';
 v_vc(205) := 'ttribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF17_NAME_RASD]]></value><valuecode><![CDATA[="B10RF17_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF17_NAME]]></value><valuecode><![CDATA[="B10RF17_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF17_VALUE]]></value><valuecode><![CDATA[="''||B10RF17(iB10)||';
 v_vc(206) := '''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>146</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF18</id><nameid>B10RF18</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF18_NAME_RASD]]></value><valuecode><![CDATA[="B10RF18_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</o';
 v_vc(207) := 'rderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF18_NAME]]></value><valuecode><![CDATA[="B10RF18_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF18_VALUE]]></value><valuecode><![CDATA[="''||B10RF18(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></';
 v_vc(208) := 'endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>147</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF19</id><nameid>B10RF19</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF19_NAME_RASD]]></value><valuecode><![CDATA[="B10RF19_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF19_NAME]]></value><valuecode><![CDATA[="B10RF19_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><ord';
 v_vc(209) := 'erby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF19_VALUE]]></value><valuecode><![CDATA[="''||B10RF19(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>148</elementid><pelementid>124</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RF99</id><nameid>B10RF99</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><sou';
 v_vc(210) := 'rce></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RF99_NAME_RASD]]></value><valuecode><![CDATA[="B10RF99_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RF99_NAME]]></value><valuecode><![CDATA[="B10RF99_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderb';
 v_vc(211) := 'y>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RF99_VALUE]]></value><valuecode><![CDATA[="''||B10RF99(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>149</elementid><pelementid>43</pelementid><orderby>2024</orderby><element>TX_</element><type>E</type><id></id><nameid>B10BLOK_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10BLOK rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10BLOK rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10BLOK_NAME]]></value><valuecode><![CDATA[="rasdTxB10BLOK_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDA';
 v_vc(212) := 'TA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>150</elementid><pelementid>149</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B10BLOK</id><nameid>B10BLOK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10BLOK_NAME_RASD]]></value><valuecode><![CDATA[="B10BLOK_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute';
 v_vc(213) := '><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B10BLOK_VALUE]]></value><valuecode><![CDATA[''||B10BLOK(iB10)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></value';
 v_vc(214) := 'id><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>151</elementid><pelementid>43</pelementid><orderby>2034</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE0_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE0 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE0 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE0_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE0_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>152</elementid><pelementid>151</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE0</id><nameid>B10PAGE0</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attrib';
 v_vc(215) := 'ute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE0#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE0#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE0#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE0#SET(iB10).custom , instr(upper(B10PAGE0#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE0#SET(iB10).custom),''"'',instr(upper(B10PAGE0#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE0#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE0#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE0#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE0#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE0#SET(iB10).custom , instr(upper(B10PAGE0#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE0#SET(iB10).custom),''"'',instr(upper(B10PAGE0#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE0#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE0_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE0_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE0_NAME]]></value><valuecode><![CDATA[="B10PAGE0_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></';
 v_vc(216) := 'valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE0_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE0_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE0_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE0(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE0#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE0#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop><';
 v_vc(217) := '/forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE0#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE0#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE0#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE0#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE0#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE0#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE0#SET(iB10).error is not null then htp.p('' title="''||B10PAGE0#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE0#SET(iB10).error is not null then htp.p('' title="''||B10PAGE0#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE0#SET(iB10).info is not null t';
 v_vc(218) := 'hen htp.p('' title="''||B10PAGE0#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE0#SET(iB10).info is not null then htp.p('' title="''||B10PAGE0#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE0_VALUE'''' , ''''B10PAGE0_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE0(iB10)||'''''' , ''''B10PAGE0_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE0#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE0#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>153</elementid><pelementid>43</pelementid><orderby>2040</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE1_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE1 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE1 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderb';
 v_vc(219) := 'y><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE1_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE1_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>154</elementid><pelementid>153</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE1</id><nameid>B10PAGE1</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE1#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE1#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE1#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE1#SET(iB10).custom , instr(upper(B10PAGE1#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE1#SET(iB10).custom),''"'',instr(upper(B10PAGE1#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE1#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE1#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE1#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE1#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||s';
 v_vc(220) := 'ubstr(B10PAGE1#SET(iB10).custom , instr(upper(B10PAGE1#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE1#SET(iB10).custom),''"'',instr(upper(B10PAGE1#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE1#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE1_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE1_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE1_NAME]]></value><valuecode><![CDATA[="B10PAGE1_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE1_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE1_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textco';
 v_vc(221) := 'de><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE1_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE1(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE1#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE1#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE1#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE1#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE1#SET(iB10).required then htp.p('' required="required"''); e';
 v_vc(222) := 'nd if; %>]]></value><valuecode><![CDATA['');  if B10PAGE1#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE1#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE1#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE1#SET(iB10).error is not null then htp.p('' title="''||B10PAGE1#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE1#SET(iB10).error is not null then htp.p('' title="''||B10PAGE1#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE1#SET(iB10).info is not null then htp.p('' title="''||B10PAGE1#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE1#SET(iB10).info is not null then htp.p('' title="''||B10PAGE1#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE1_VALUE'''' , ''''B10PAGE1_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE1(iB';
 v_vc(223) := '10)||'''''' , ''''B10PAGE1_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE1#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE1#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>155</elementid><pelementid>43</pelementid><orderby>2044</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE2_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE2 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE2 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE2_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE2_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value';
 v_vc(224) := '><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>156</elementid><pelementid>155</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE2</id><nameid>B10PAGE2</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE2#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE2#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE2#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE2#SET(iB10).custom , instr(upper(B10PAGE2#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE2#SET(iB10).custom),''"'',instr(upper(B10PAGE2#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE2#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE2#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE2#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE2#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE2#SET(iB10).custom , instr(upper(B10PAGE2#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE2#SET(iB10).custom),''"'',instr(upper(B10PAGE2#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE2#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text';
 v_vc(225) := '></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE2_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE2_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE2_NAME]]></value><valuecode><![CDATA[="B10PAGE2_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE2_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE2_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE2_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE2(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><';
 v_vc(226) := 'attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE2#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE2#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE2#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE2#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE2#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE2#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE2#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE2#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><va';
 v_vc(227) := 'lue><![CDATA[<% if B10PAGE2#SET(iB10).error is not null then htp.p('' title="''||B10PAGE2#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE2#SET(iB10).error is not null then htp.p('' title="''||B10PAGE2#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE2#SET(iB10).info is not null then htp.p('' title="''||B10PAGE2#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE2#SET(iB10).info is not null then htp.p('' title="''||B10PAGE2#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE2_VALUE'''' , ''''B10PAGE2_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE2(iB10)||'''''' , ''''B10PAGE2_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE2#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE2#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>157</elementid><pelementid>43</pelementid><orderby>2048</orderby><element>TX_</element><type>E</type><id></id><na';
 v_vc(228) := 'meid>B10PAGE3_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE3 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE3 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE3_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE3_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>158</elementid><pelementid>157</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE3</id><nameid>B10PAGE3</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE3#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE3#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% i';
 v_vc(229) := 'f instr(upper(B10PAGE3#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE3#SET(iB10).custom , instr(upper(B10PAGE3#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE3#SET(iB10).custom),''"'',instr(upper(B10PAGE3#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE3#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE3#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE3#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE3#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE3#SET(iB10).custom , instr(upper(B10PAGE3#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE3#SET(iB10).custom),''"'',instr(upper(B10PAGE3#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE3#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE3_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE3_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE3_NAME]]></value><valuecode><![CDATA[="B10PAGE3_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKB';
 v_vc(230) := 'XDclick(this.value, ''''B10PAGE3_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE3_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE3_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE3(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE3#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE3#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><';
 v_vc(231) := 'text></text><name></name><value><![CDATA[<% if B10PAGE3#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE3#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE3#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE3#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE3#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE3#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE3#SET(iB10).error is not null then htp.p('' title="''||B10PAGE3#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE3#SET(iB10).error is not null then htp.p('' title="''||B10PAGE3#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE3#SET(iB10).info is not null then htp.p('' title="''||B10PAGE3#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE3#SET(iB10).info is not null then htp.p('' title="''||B10PAGE3#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloo';
 v_vc(232) := 'p></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE3_VALUE'''' , ''''B10PAGE3_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE3(iB10)||'''''' , ''''B10PAGE3_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE3#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE3#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>159</elementid><pelementid>43</pelementid><orderby>2054</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE4_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE4 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE4 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE4_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE4_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hidd';
 v_vc(233) := 'enyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>160</elementid><pelementid>159</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE4</id><nameid>B10PAGE4</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE4#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE4#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE4#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE4#SET(iB10).custom , instr(upper(B10PAGE4#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE4#SET(iB10).custom),''"'',instr(upper(B10PAGE4#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE4#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE4#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE4#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE4#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE4#SET(iB10).custom , instr(upper(B10PAGE4#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE4#SET(iB10).custom),''"'',instr(upper(B10PAGE4#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE4#SET(iB10).custom),''CLASS="'')-7) ); end if;
ht';
 v_vc(234) := 'p.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE4_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE4_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE4_NAME]]></value><valuecode><![CDATA[="B10PAGE4_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE4_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE4_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valueco';
 v_vc(235) := 'de><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE4_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE4(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE4#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE4#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE4#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE4#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE4#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE4#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></';
 v_vc(236) := 'textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE4#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE4#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE4#SET(iB10).error is not null then htp.p('' title="''||B10PAGE4#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE4#SET(iB10).error is not null then htp.p('' title="''||B10PAGE4#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE4#SET(iB10).info is not null then htp.p('' title="''||B10PAGE4#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE4#SET(iB10).info is not null then htp.p('' title="''||B10PAGE4#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE4_VALUE'''' , ''''B10PAGE4_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE4(iB10)||'''''' , ''''B10PAGE4_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rfo';
 v_vc(237) := 'rm><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE4#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE4#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>161</elementid><pelementid>43</pelementid><orderby>2060</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE5_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE5 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE5 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE5_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE5_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><el';
 v_vc(238) := 'ement><elementid>162</elementid><pelementid>161</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE5</id><nameid>B10PAGE5</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE5#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE5#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE5#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE5#SET(iB10).custom , instr(upper(B10PAGE5#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE5#SET(iB10).custom),''"'',instr(upper(B10PAGE5#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE5#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE5#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE5#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE5#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE5#SET(iB10).custom , instr(upper(B10PAGE5#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE5#SET(iB10).custom),''"'',instr(upper(B10PAGE5#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE5#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE5_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE5_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></text';
 v_vc(239) := 'id><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE5_NAME]]></value><valuecode><![CDATA[="B10PAGE5_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE5_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE5_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE5_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE5(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid';
 v_vc(240) := '><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE5#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE5#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE5#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE5#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE5#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE5#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE5#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE5#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE5#SET(iB10).error is not null then htp.p('' title="''||B10PAGE5#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE5#SET(iB10).error is not null then htp.p('' title="''||B10PAGE5#SET(iB10).error||''"''); ';
 v_vc(241) := 'end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE5#SET(iB10).info is not null then htp.p('' title="''||B10PAGE5#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE5#SET(iB10).info is not null then htp.p('' title="''||B10PAGE5#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE5_VALUE'''' , ''''B10PAGE5_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE5(iB10)||'''''' , ''''B10PAGE5_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE5#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE5#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>163</elementid><pelementid>43</pelementid><orderby>2064</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE6_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</typ';
 v_vc(242) := 'e><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE6 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE6 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE6_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE6_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>164</elementid><pelementid>163</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE6</id><nameid>B10PAGE6</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE6#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE6#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE6#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE6#SET(iB10).custom , instr(upper(B10PAGE6#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE6#SET(iB10).custom),''"'',instr(upper(B10PAGE6#SET(iB10).custom),''CLASS=';
 v_vc(243) := '"'')+8)-instr(upper(B10PAGE6#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE6#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE6#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE6#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE6#SET(iB10).custom , instr(upper(B10PAGE6#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE6#SET(iB10).custom),''"'',instr(upper(B10PAGE6#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE6#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE6_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE6_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE6_NAME]]></value><valuecode><![CDATA[="B10PAGE6_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE6_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE6_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></';
 v_vc(244) := 'textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE6_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE6(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE6#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE6#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE6#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE6#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valu';
 v_vc(245) := 'ecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE6#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE6#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE6#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE6#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE6#SET(iB10).error is not null then htp.p('' title="''||B10PAGE6#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE6#SET(iB10).error is not null then htp.p('' title="''||B10PAGE6#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE6#SET(iB10).info is not null then htp.p('' title="''||B10PAGE6#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE6#SET(iB10).info is not null then htp.p('' title="''||B10PAGE6#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name';
 v_vc(246) := '></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE6_VALUE'''' , ''''B10PAGE6_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE6(iB10)||'''''' , ''''B10PAGE6_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE6#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE6#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>165</elementid><pelementid>43</pelementid><orderby>2068</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE7_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE7 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE7 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE7_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE7_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![C';
 v_vc(247) := 'DATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>166</elementid><pelementid>165</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE7</id><nameid>B10PAGE7</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE7#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE7#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE7#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE7#SET(iB10).custom , instr(upper(B10PAGE7#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE7#SET(iB10).custom),''"'',instr(upper(B10PAGE7#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE7#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE7#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE7#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE7#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE7#SET(iB10).custom , instr(upper(B10PAGE7#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE7#SET(iB10).custom),''"'',instr(upper(B10PAGE7#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE7#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribut';
 v_vc(248) := 'e>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE7_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE7_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE7_NAME]]></value><valuecode><![CDATA[="B10PAGE7_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE7_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE7_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute';
 v_vc(249) := '><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE7_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE7(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE7#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE7#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE7#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE7#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE7#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE7#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE7#SET(iB10).custom); %>]]></value>';
 v_vc(250) := '<valuecode><![CDATA['');  htp.p('' ''||B10PAGE7#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE7#SET(iB10).error is not null then htp.p('' title="''||B10PAGE7#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE7#SET(iB10).error is not null then htp.p('' title="''||B10PAGE7#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE7#SET(iB10).info is not null then htp.p('' title="''||B10PAGE7#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE7#SET(iB10).info is not null then htp.p('' title="''||B10PAGE7#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE7_VALUE'''' , ''''B10PAGE7_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE7(iB10)||'''''' , ''''B10PAGE7_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE7#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE7#SET(iB10).visible th';
 v_vc(251) := 'en
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>167</elementid><pelementid>43</pelementid><orderby>2072</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE8_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE8 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE8 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE8_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE8_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>168</elementid><pelementid>167</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE8</id><nameid>B10PAGE8</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></';
 v_vc(252) := 'rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE8#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE8#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE8#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE8#SET(iB10).custom , instr(upper(B10PAGE8#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE8#SET(iB10).custom),''"'',instr(upper(B10PAGE8#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE8#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE8#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE8#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE8#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE8#SET(iB10).custom , instr(upper(B10PAGE8#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE8#SET(iB10).custom),''"'',instr(upper(B10PAGE8#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE8#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE8_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE8_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE8_NAME]]></value><valuecode><![CDA';
 v_vc(253) := 'TA[="B10PAGE8_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE8_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE8_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE8_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE8(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE8#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valueco';
 v_vc(254) := 'de><![CDATA['');  if B10PAGE8#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE8#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE8#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE8#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE8#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE8#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE8#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE8#SET(iB10).error is not null then htp.p('' title="''||B10PAGE8#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE8#SET(iB10).error is not null then htp.p('' title="''||B10PAGE8#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orde';
 v_vc(255) := 'rby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE8#SET(iB10).info is not null then htp.p('' title="''||B10PAGE8#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE8#SET(iB10).info is not null then htp.p('' title="''||B10PAGE8#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE8_VALUE'''' , ''''B10PAGE8_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE8(iB10)||'''''' , ''''B10PAGE8_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE8#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE8#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>169</elementid><pelementid>43</pelementid><orderby>2120</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE9_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE9 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE9 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></va';
 v_vc(256) := 'lueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE9_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE9_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>170</elementid><pelementid>169</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE9</id><nameid>B10PAGE9</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE9#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE9#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE9#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE9#SET(iB10).custom , instr(upper(B10PAGE9#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE9#SET(iB10).custom),''"'',instr(upper(B10PAGE9#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE9#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE9#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE9#SET(iB10).info is not nul';
 v_vc(257) := 'l then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE9#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE9#SET(iB10).custom , instr(upper(B10PAGE9#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE9#SET(iB10).custom),''"'',instr(upper(B10PAGE9#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE9#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE9_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE9_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE9_NAME]]></value><valuecode><![CDATA[="B10PAGE9_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE9_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE9_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><fo';
 v_vc(258) := 'rloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE9_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE9(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE9#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE9#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE9#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE9#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute';
 v_vc(259) := '><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE9#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE9#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE9#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE9#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE9#SET(iB10).error is not null then htp.p('' title="''||B10PAGE9#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE9#SET(iB10).error is not null then htp.p('' title="''||B10PAGE9#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE9#SET(iB10).info is not null then htp.p('' title="''||B10PAGE9#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE9#SET(iB10).info is not null then htp.p('' title="''||B10PAGE9#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE9_VALUE'''' , ''''B10PAGE9_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT';
 v_vc(260) := ' LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE9(iB10)||'''''' , ''''B10PAGE9_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE9#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE9#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>171</elementid><pelementid>43</pelementid><orderby>2122</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE10_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE10 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE10 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE10_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE10_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attr';
 v_vc(261) := 'ibute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>172</elementid><pelementid>171</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE10</id><nameid>B10PAGE10</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE10#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE10#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE10#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE10#SET(iB10).custom , instr(upper(B10PAGE10#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE10#SET(iB10).custom),''"'',instr(upper(B10PAGE10#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE10#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE10#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE10#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE10#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE10#SET(iB10).custom , instr(upper(B10PAGE10#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE10#SET(iB10).custom),''"'',instr(upper(B10PAGE10#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE10#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><t';
 v_vc(262) := 'extcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE10_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE10_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE10_NAME]]></value><valuecode><![CDATA[="B10PAGE10_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE10_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE10_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE10_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE10(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn';
 v_vc(263) := '>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE10#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE10#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE10#SET(iB10).error is not null then htp.p('' title="''||B10PAGE10#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE10#SET(iB10).error is not null then htp.p('' title="''||B10PAGE10#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE10#SET(iB10).info is not null then htp.p('' title="''||B10PAGE10#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE10#SET(iB10).info is not null then htp.p('' title="''||B10PAGE10#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE10#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE10#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></';
 v_vc(264) := 'forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE10#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE10#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE10#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE10#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE10_VALUE'''' , ''''B10PAGE10_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE10(iB10)||'''''' , ''''B10PAGE10_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE10#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE10#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rfor';
 v_vc(265) := 'm><rid></rid></attribute></attributes></element><element><elementid>173</elementid><pelementid>43</pelementid><orderby>2124</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE11_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE11 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE11 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE11_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE11_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>174</elementid><pelementid>173</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE11</id><nameid>B10PAGE11</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><va';
 v_vc(266) := 'lue><![CDATA[rasdCheckbox <% if B10PAGE11#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE11#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE11#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE11#SET(iB10).custom , instr(upper(B10PAGE11#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE11#SET(iB10).custom),''"'',instr(upper(B10PAGE11#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE11#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE11#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE11#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE11#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE11#SET(iB10).custom , instr(upper(B10PAGE11#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE11#SET(iB10).custom),''"'',instr(upper(B10PAGE11#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE11#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE11_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE11_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE11_NAME]]></value><valuecode><![CDATA[="B10PAGE11_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcod';
 v_vc(267) := 'e><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE11_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE11_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE11_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE11(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE11#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE11#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></';
 v_vc(268) := 'source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE11#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE11#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE11#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE11#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE11#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE11#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE11#SET(iB10).error is not null then htp.p('' title="''||B10PAGE11#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE11#SET(iB10).error is not null then htp.p('' title="''||B10PAGE11#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE11#SET(iB10).info is not null then htp.p('' title="''||B10PA';
 v_vc(269) := 'GE11#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE11#SET(iB10).info is not null then htp.p('' title="''||B10PAGE11#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE11_VALUE'''' , ''''B10PAGE11_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE11(iB10)||'''''' , ''''B10PAGE11_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE11#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE11#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>175</elementid><pelementid>43</pelementid><orderby>2126</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE12_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE12 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE12 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_';
 v_vc(270) := 'ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE12_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE12_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>176</elementid><pelementid>175</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE12</id><nameid>B10PAGE12</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE12#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE12#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE12#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE12#SET(iB10).custom , instr(upper(B10PAGE12#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE12#SET(iB10).custom),''"'',instr(upper(B10PAGE12#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE12#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE12#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE12#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE12#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||s';
 v_vc(271) := 'ubstr(B10PAGE12#SET(iB10).custom , instr(upper(B10PAGE12#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE12#SET(iB10).custom),''"'',instr(upper(B10PAGE12#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE12#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE12_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE12_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE12_NAME]]></value><valuecode><![CDATA[="B10PAGE12_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE12_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE12_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textco';
 v_vc(272) := 'de></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE12_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE12(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE12#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE12#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE12#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE12#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE12#SET(iB10).required then htp.p('' requir';
 v_vc(273) := 'ed="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE12#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE12#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE12#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE12#SET(iB10).error is not null then htp.p('' title="''||B10PAGE12#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE12#SET(iB10).error is not null then htp.p('' title="''||B10PAGE12#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE12#SET(iB10).info is not null then htp.p('' title="''||B10PAGE12#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE12#SET(iB10).info is not null then htp.p('' title="''||B10PAGE12#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE12_VALUE'''' , ''''B10PAGE12_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Cli';
 v_vc(274) := 'nk$CHKBXDinit( ''''''||B10PAGE12(iB10)||'''''' , ''''B10PAGE12_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE12#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE12#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>177</elementid><pelementid>43</pelementid><orderby>2128</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE13_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE13 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE13 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE13_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE13_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[';
 v_vc(275) := '<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>178</elementid><pelementid>177</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE13</id><nameid>B10PAGE13</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE13#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE13#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE13#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE13#SET(iB10).custom , instr(upper(B10PAGE13#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE13#SET(iB10).custom),''"'',instr(upper(B10PAGE13#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE13#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE13#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE13#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE13#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE13#SET(iB10).custom , instr(upper(B10PAGE13#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE13#SET(iB10).custom),''"'',instr(upper(B10PAGE13#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE13#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby';
 v_vc(276) := '>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE13_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE13_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE13_NAME]]></value><valuecode><![CDATA[="B10PAGE13_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE13_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE13_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE13_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE13(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform';
 v_vc(277) := '></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE13#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE13#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE13#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE13#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE13#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE13#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE13#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE13#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</or';
 v_vc(278) := 'derby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE13#SET(iB10).error is not null then htp.p('' title="''||B10PAGE13#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE13#SET(iB10).error is not null then htp.p('' title="''||B10PAGE13#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE13#SET(iB10).info is not null then htp.p('' title="''||B10PAGE13#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE13#SET(iB10).info is not null then htp.p('' title="''||B10PAGE13#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE13_VALUE'''' , ''''B10PAGE13_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE13(iB10)||'''''' , ''''B10PAGE13_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE13#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE13#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>179</elementid><pelementi';
 v_vc(279) := 'd>43</pelementid><orderby>2130</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE14_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE14 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE14 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE14_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE14_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>180</elementid><pelementid>179</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE14</id><nameid>B10PAGE14</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE14#SET(iB10).error is not null then htp.p('' errorField';
 v_vc(280) := '''); end if; %><% if B10PAGE14#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE14#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE14#SET(iB10).custom , instr(upper(B10PAGE14#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE14#SET(iB10).custom),''"'',instr(upper(B10PAGE14#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE14#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE14#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE14#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE14#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE14#SET(iB10).custom , instr(upper(B10PAGE14#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE14#SET(iB10).custom),''"'',instr(upper(B10PAGE14#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE14#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE14_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE14_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE14_NAME]]></value><valuecode><![CDATA[="B10PAGE14_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attr';
 v_vc(281) := 'ibute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE14_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE14_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE14_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE14(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE14#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE14#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid';
 v_vc(282) := '></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE14#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE14#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE14#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE14#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE14#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE14#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE14#SET(iB10).error is not null then htp.p('' title="''||B10PAGE14#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE14#SET(iB10).error is not null then htp.p('' title="''||B10PAGE14#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE14#SET(iB10).info is not null then htp.p('' title="''||B10PAGE14#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE14#SET(iB1';
 v_vc(283) := '0).info is not null then htp.p('' title="''||B10PAGE14#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE14_VALUE'''' , ''''B10PAGE14_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE14(iB10)||'''''' , ''''B10PAGE14_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE14#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE14#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>181</elementid><pelementid>43</pelementid><orderby>2140</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE15_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE15 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE15 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10';
 v_vc(284) := 'PAGE15_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE15_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>182</elementid><pelementid>181</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE15</id><nameid>B10PAGE15</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE15#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE15#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE15#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE15#SET(iB10).custom , instr(upper(B10PAGE15#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE15#SET(iB10).custom),''"'',instr(upper(B10PAGE15#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE15#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE15#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE15#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE15#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE15#SET(iB10).custom , instr(upper(B10PAGE15#SET(iB10).custom),''CLASS="'')+7 , ins';
 v_vc(285) := 'tr(upper(B10PAGE15#SET(iB10).custom),''"'',instr(upper(B10PAGE15#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE15#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE15_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE15_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE15_NAME]]></value><valuecode><![CDATA[="B10PAGE15_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE15_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE15_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</o';
 v_vc(286) := 'rderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE15_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE15(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE15#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE15#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE15#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE15#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE15#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE15#SET(iB10).require';
 v_vc(287) := 'd then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE15#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE15#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE15#SET(iB10).error is not null then htp.p('' title="''||B10PAGE15#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE15#SET(iB10).error is not null then htp.p('' title="''||B10PAGE15#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE15#SET(iB10).info is not null then htp.p('' title="''||B10PAGE15#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE15#SET(iB10).info is not null then htp.p('' title="''||B10PAGE15#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE15_VALUE'''' , ''''B10PAGE15_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE15(iB10)||'''''' , ''''B10PAGE15_''||iB10||'''''' ); }) </SCRIPT>'');  end i';
 v_vc(288) := 'f;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE15#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE15#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>183</elementid><pelementid>43</pelementid><orderby>3000</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE16_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE16 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE16 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE16_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE16_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forlo';
 v_vc(289) := 'op><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>184</elementid><pelementid>183</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE16</id><nameid>B10PAGE16</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE16#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE16#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE16#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE16#SET(iB10).custom , instr(upper(B10PAGE16#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE16#SET(iB10).custom),''"'',instr(upper(B10PAGE16#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE16#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE16#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE16#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE16#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE16#SET(iB10).custom , instr(upper(B10PAGE16#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE16#SET(iB10).custom),''"'',instr(upper(B10PAGE16#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE16#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name>';
 v_vc(290) := '<value><![CDATA[B10PAGE16_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE16_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE16_NAME]]></value><valuecode><![CDATA[="B10PAGE16_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE16_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE16_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE16_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE16(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><ty';
 v_vc(291) := 'pe>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE16#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE16#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE16#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE16#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE16#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE16#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE16#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE16#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if ';
 v_vc(292) := 'B10PAGE16#SET(iB10).error is not null then htp.p('' title="''||B10PAGE16#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE16#SET(iB10).error is not null then htp.p('' title="''||B10PAGE16#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE16#SET(iB10).info is not null then htp.p('' title="''||B10PAGE16#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE16#SET(iB10).info is not null then htp.p('' title="''||B10PAGE16#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE16_VALUE'''' , ''''B10PAGE16_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE16(iB10)||'''''' , ''''B10PAGE16_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE16#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE16#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>185</elementid><pelementid>43</pelementid><orderby>3076</orderby><element>TX_</element><type>E</type><id></id><nameid>';
 v_vc(293) := 'B10PAGE17_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE17 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE17 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE17_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE17_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>186</elementid><pelementid>185</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE17</id><nameid>B10PAGE17</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE17#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE17#SET(iB10).info is not null then htp.p('' infoField''); end if; %>';
 v_vc(294) := '<% if instr(upper(B10PAGE17#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE17#SET(iB10).custom , instr(upper(B10PAGE17#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE17#SET(iB10).custom),''"'',instr(upper(B10PAGE17#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE17#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE17#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE17#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE17#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE17#SET(iB10).custom , instr(upper(B10PAGE17#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE17#SET(iB10).custom),''"'',instr(upper(B10PAGE17#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE17#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE17_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE17_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE17_NAME]]></value><valuecode><![CDATA[="B10PAGE17_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value>';
 v_vc(295) := '<![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE17_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE17_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE17_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE17(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE17#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE17#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C';
 v_vc(296) := '_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE17#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE17#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE17#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE17#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE17#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE17#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE17#SET(iB10).error is not null then htp.p('' title="''||B10PAGE17#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE17#SET(iB10).error is not null then htp.p('' title="''||B10PAGE17#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE17#SET(iB10).info is not null then htp.p('' title="''||B10PAGE17#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE17#SET(iB10).info is not null then htp.p('' title="''||B10PAGE17#SET(iB10).info||''"''); end if;
htp.prn(';
 v_vc(297) := ''']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE17_VALUE'''' , ''''B10PAGE17_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE17(iB10)||'''''' , ''''B10PAGE17_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE17#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE17#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>187</elementid><pelementid>43</pelementid><orderby>3100</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE18_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE18 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE18 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE18_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE18_''||iB10||''"]]></valuecode><forloo';
 v_vc(298) := 'p></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>188</elementid><pelementid>187</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE18</id><nameid>B10PAGE18</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE18#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE18#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE18#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE18#SET(iB10).custom , instr(upper(B10PAGE18#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE18#SET(iB10).custom),''"'',instr(upper(B10PAGE18#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE18#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE18#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE18#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE18#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE18#SET(iB10).custom , instr(upper(B10PAGE18#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE18#SET(iB10).custom),''"'',instr(upper(B10PAGE18#SET(iB10).custom),''CLASS="'')+8';
 v_vc(299) := ')-instr(upper(B10PAGE18#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE18_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE18_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE18_NAME]]></value><valuecode><![CDATA[="B10PAGE18_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE18_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE18_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><';
 v_vc(300) := 'value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE18_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE18(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE18#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE18#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE18#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE18#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE18#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE18#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><e';
 v_vc(301) := 'ndloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE18#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE18#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE18#SET(iB10).error is not null then htp.p('' title="''||B10PAGE18#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE18#SET(iB10).error is not null then htp.p('' title="''||B10PAGE18#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE18#SET(iB10).info is not null then htp.p('' title="''||B10PAGE18#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE18#SET(iB10).info is not null then htp.p('' title="''||B10PAGE18#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE18_VALUE'''' , ''''B10PAGE18_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE18(iB10)||'''''' , ''''B10PAGE18_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn';
 v_vc(302) := '>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE18#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE18#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>189</elementid><pelementid>43</pelementid><orderby>3120</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE19_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE19 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE19 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE19_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE19_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></tex';
 v_vc(303) := 'tid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>190</elementid><pelementid>189</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE19</id><nameid>B10PAGE19</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE19#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE19#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE19#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE19#SET(iB10).custom , instr(upper(B10PAGE19#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE19#SET(iB10).custom),''"'',instr(upper(B10PAGE19#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE19#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE19#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE19#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE19#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE19#SET(iB10).custom , instr(upper(B10PAGE19#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE19#SET(iB10).custom),''"'',instr(upper(B10PAGE19#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE19#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE19_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE19_''||iB10||''_RASD';
 v_vc(304) := '"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE19_NAME]]></value><valuecode><![CDATA[="B10PAGE19_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE19_NAME'''');]]></value><valuecode><![CDATA[="js_Clink$CHKBXDclick(this.value, ''''B10PAGE19_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE19_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE19(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop';
 v_vc(305) := '><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE19#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE19#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE19#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE19#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE19#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE19#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE19#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE19#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE19#SET(iB10).error is not null then htp.p('' title="''||B10PAGE19#SET(iB10).error||''"'');';
 v_vc(306) := ' end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE19#SET(iB10).error is not null then htp.p('' title="''||B10PAGE19#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE19#SET(iB10).info is not null then htp.p('' title="''||B10PAGE19#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE19#SET(iB10).info is not null then htp.p('' title="''||B10PAGE19#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE19_VALUE'''' , ''''B10PAGE19_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE19(iB10)||'''''' , ''''B10PAGE19_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE19#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE19#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>191</elementid><pelementid>43</pelementid><orderby>3140</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PAGE99_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddeny';
 v_vc(307) := 'n><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PAGE99 rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10PAGE99 rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PAGE99_NAME]]></value><valuecode><![CDATA[="rasdTxB10PAGE99_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>192</elementid><pelementid>191</pelementid><orderby>1</orderby><element>INPUT_CHECKBOX</element><type>D</type><id>B10PAGE99</id><nameid>B10PAGE99</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdCheckbox <% if B10PAGE99#SET(iB10).error is not null then htp.p('' errorField''); end if; %><% if B10PAGE99#SET(iB10).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PAGE99#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE';
 v_vc(308) := '99#SET(iB10).custom , instr(upper(B10PAGE99#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE99#SET(iB10).custom),''"'',instr(upper(B10PAGE99#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE99#SET(iB10).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdCheckbox '');  if B10PAGE99#SET(iB10).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if B10PAGE99#SET(iB10).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(B10PAGE99#SET(iB10).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PAGE99#SET(iB10).custom , instr(upper(B10PAGE99#SET(iB10).custom),''CLASS="'')+7 , instr(upper(B10PAGE99#SET(iB10).custom),''"'',instr(upper(B10PAGE99#SET(iB10).custom),''CLASS="'')+8)-instr(upper(B10PAGE99#SET(iB10).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PAGE99_NAME_RASD]]></value><valuecode><![CDATA[="B10PAGE99_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PAGE99_NAME]]></value><valuecode><![CDATA[="B10PAGE99_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[js_Clink$CHKBXDclick(this.value, ''''B10PAGE99_NAME'''');]]></value><valuecode><![CDATA[';
 v_vc(309) := '="js_Clink$CHKBXDclick(this.value, ''''B10PAGE99_''||iB10||'''''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[checkbox]]></value><valuecode><![CDATA[="checkbox"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10PAGE99_VALUE]]></value><valuecode><![CDATA[="''||B10PAGE99(iB10)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE99#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE99#SET(iB10).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE99#SET(iB10';
 v_vc(310) := ').disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE99#SET(iB10).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE99#SET(iB10).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE99#SET(iB10).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PAGE99#SET(iB10).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PAGE99#SET(iB10).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE99#SET(iB10).error is not null then htp.p('' title="''||B10PAGE99#SET(iB10).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE99#SET(iB10).error is not null then htp.p('' title="''||B10PAGE99#SET(iB10).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PAGE99#SET(iB10).info is not null then htp.p('' title="''||B10PAGE99#SET(iB10).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PAGE99#SET(iB10).info is not null then htp.p('' title="''||B10PAGE99#SET(iB10).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn>';
 v_vc(311) := '<valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''B10PAGE99_VALUE'''' , ''''B10PAGE99_NAME'''' ); }) </SCRIPT><% end if; %>]]></value><valuecode><![CDATA[/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''''DOMContentLoaded'''', function(event) {  js_Clink$CHKBXDinit( ''''''||B10PAGE99(iB10)||'''''' , ''''B10PAGE99_''||iB10||'''''' ); }) </SCRIPT>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PAGE99#SET(iB10).visible then %><input]]></value><valuecode><![CDATA['');  if B10PAGE99#SET(iB10).visible then
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>193</elementid><pelementid>94</pelementid><orderby>20000</orderby><element>TX_</element><type>E</type><id></id><nameid>B30TEXT_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB30TEXT rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB30TEXT rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB30TEXT_NAME]]></value><valuecode><![CDATA[="rasdTxB30TEXT_''||iB30||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></tex';
 v_vc(312) := 'tid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>194</elementid><pelementid>193</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B30TEXT</id><nameid>B30TEXT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30TEXT_NAME_RASD]]></value><valuecode><![CDATA[="B30TEXT_''||iB30||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name>';
 v_vc(313) := '<value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</';
 v_vc(314) := 'attribute><type>V</type><text></text><name></name><value><![CDATA[B30TEXT_VALUE]]></value><valuecode><![CDATA[''||B30TEXT(iB30)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element></elements></form>';
     return v_vc;
  end;
function metadata return clob is
  v_clob clob := '';
  v_vc cctab;
  begin
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       v_clob := v_clob || v_vc(i);
     end loop;
     return v_clob;
  end;
procedure metadata is
  v_clob clob := '';
  v_vc cctab;
  begin
  owa_util.mime_header('text/xml', FALSE);
  HTP.p('Content-Disposition: filename="Export_RASDC2_PAGES_v.1.1.20240228090907.xml"');
  owa_util.http_header_close;
  htp.p('<?xml version="1.0" encoding="UTF-8" ?>');
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       htp.prn(v_vc(i));
     end loop;
  end;
     begin
       null;
  -- initialization part

end RASDC2_PAGES;

/
